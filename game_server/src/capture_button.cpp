#include "capture_button.h"

#include "image.h"
#include "timing.h"

#include "RakNetTypes.h"
using namespace RakNet;

CaptureButton::CaptureButton() :
	GameObject(),
	m_isRedTeam(false),
	m_isActivated(false),
	m_cooldownTimer(0.0f) {

}

CaptureButton::~CaptureButton() {

}

int CaptureButton::init() {
	GameObject::init();

	m_image->m_size = glm::vec2(0.020f, 0.01f);
	m_position = glm::vec2(0.0f, -0.395f);
	m_image->m_color = glm::vec4(1.0f);

	return 0;
}

void CaptureButton::customUpdate() {
	if (m_isActivated) {
		if (m_isRedTeam) m_image->m_color = glm::vec4(0.9f, 0.3f, 0.3f, 1.0f);
		else m_image->m_color = glm::vec4(0.3f, 0.3f, 0.9f, 1.0f);
	}
	else m_image->m_color = glm::vec4(1.0f);

	m_cooldownTimer -= (float)Time::getDeltaTime();
}

int CaptureButton::exit() {
	return GameObject::exit();
}

void CaptureButton::setIsRedTeam(bool a_isRedTeam) {
	m_isRedTeam = a_isRedTeam;
	m_isDirty = true;
}

void CaptureButton::setIsActivated(bool a_isActivated) {
	m_isActivated = a_isActivated;

	if (m_isActivated) {
		m_position = glm::vec2(0.0f, -0.4f);
		m_cooldownTimer = 30.0f;
	}
	else m_position = glm::vec2(0.0f, -0.395f);
	m_isDirty = true;
}

void CaptureButton::setCooldownTimer(float a_cooldownTimer) {
	m_cooldownTimer = a_cooldownTimer;
}