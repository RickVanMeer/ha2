#include "pickup.h"

#include "image.h"
#include "RakNetTypes.h"
using namespace RakNet;

Pickup::Pickup() :
	GameObject(),
	m_pickupType(ETeleportRed) {

}

Pickup::~Pickup() {

}

int Pickup::init() {
	GameObject::init();

	m_image->m_size = glm::vec2(0.05f);

	switch (m_pickupType) {
	case ETeleportRed:
		m_image->m_color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
		break;
	case ETeleportBlue:
		m_image->m_color = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
		break;
	}

	m_velocity = glm::vec2(0.0f, -0.0005f);

	return 0;
}

void Pickup::customUpdate() {
	if (m_position.y < -0.375f) {
		m_position.y = -0.375f;
		m_velocity = glm::vec2(0.0f);
		m_isDirty = true;
	}
}

int Pickup::exit() {
	return GameObject::exit();
}

void Pickup::setPickupType(PickupType a_pickupType) {
	m_pickupType = a_pickupType;

	switch (m_pickupType) {
	case ETeleportRed:
		m_image->m_color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
		break;
	case ETeleportBlue:
		m_image->m_color = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
		break;
	}
}