#include "capture_npc.h"

#include "image.h"
#include "timing.h"

#include "RakNetTypes.h"
using namespace RakNet;

CaptureNPC::CaptureNPC() :
	GameObject(),
	m_isRedTeam(false),
	m_isActivated(false),
	m_health(100.0f),
	m_shootCooldown(0.0f),
	m_owner(nullptr) {

}

CaptureNPC::~CaptureNPC() {

}

int CaptureNPC::init() {
	GameObject::init();

	m_image->m_size = glm::vec2(0.1f);
	m_position = glm::vec2(0.0f, -0.1f);
	m_image->m_color = glm::vec4(1.0f);

	return 0;
}

void CaptureNPC::customUpdate() {
	if (m_isActivated) {
		if (m_isRedTeam) m_image->m_color = glm::vec4(0.9f, 0.3f, 0.3f, 1.0f);
		else m_image->m_color = glm::vec4(0.3f, 0.3f, 0.9f, 1.0f);

		m_shootCooldown -= (float)Time::getDeltaTime();
		m_isDirty = true;
	}
	else {
		m_image->m_color = glm::vec4(1.0f);
		m_image->m_rotation = (float)Time::getRunningTime() / 5.0f;
	}
}

int CaptureNPC::exit() {
	return GameObject::exit();
}

void CaptureNPC::setIsRedTeam(bool a_isRedTeam) {
	m_isRedTeam = a_isRedTeam;
}

void CaptureNPC::setIsActivated(bool a_isActivated) {
	m_isActivated = a_isActivated;
	m_isDirty = true;
}

void CaptureNPC::setHealth(float a_health) {
	m_health = a_health;

	if (m_health <= 0.0f) setIsActivated(false);
}

void CaptureNPC::setShootCooldown(float a_shootCooldown) {
	m_shootCooldown = a_shootCooldown;
}

void CaptureNPC::setOwner(RakNet::RakNetGUID* a_owner) {
	m_owner = a_owner;
}