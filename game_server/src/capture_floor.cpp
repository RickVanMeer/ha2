#include "capture_floor.h"

#include "image.h"
#include "RakNetTypes.h"
using namespace RakNet;

CaptureFloor::CaptureFloor() :
	GameObject(),
	m_isRedTeam(false),
	m_isActivated(false) {

}

CaptureFloor::~CaptureFloor() {

}

int CaptureFloor::init() {
	GameObject::init();

	m_image->m_size = glm::vec2(2.0f, 0.1f);
	m_position = glm::vec2(0.0f, -0.45f);
	m_image->m_correctForAspectRatio = false;
	m_image->m_color = glm::vec4(1.0f);

	return 0;
}

void CaptureFloor::customUpdate() {
	if (m_isActivated) {
		if (m_isRedTeam) m_image->m_color = glm::vec4(0.85f, 0.35f, 0.35f, 1.0f);
		else m_image->m_color = glm::vec4(0.35f, 0.35f, 0.85f, 1.0f);
	}
	else m_image->m_color = glm::vec4(1.0f);
}

int CaptureFloor::exit() {
	return GameObject::exit();
}

void CaptureFloor::setIsRedTeam(bool a_isRedTeam) {
	m_isRedTeam = a_isRedTeam;
}

void CaptureFloor::setIsActivated(bool a_isActivated) {
	m_isActivated = a_isActivated;
	m_isDirty = true;
}