#include "game_mode.h"

#include "network_manager.h"
#include "render_manager.h"
#include "input_manager.h"
#include "game_object.h"
#include "player.h"
#include "player_data.h"
#include "flag.h"
#include "bullet.h"
#include "pickup.h"
#include "capture_button.h"
#include "capture_npc.h"
#include "capture_floor.h"
#include "image.h"
#include "debug.h"
#include "camera.h"
#include "timing.h"

#include "RakPeerInterface.h"
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "RakNetTypes.h"
using namespace RakNet;

GameMode::GameMode():
	m_lobbyServerGUID(nullptr),
	m_numGameClients(0),
	m_currentID(0),
	m_updateTimer(0.0f),
	m_gameObjects(),
	m_captureButton(nullptr),
	m_captureNPC(nullptr),
	m_captureFloor(nullptr),
	m_redTeamScore(0),
	m_blueTeamScore(0),
	m_stopGameTimer(30.0f),
	m_stopGame(false),
	m_pickup(nullptr),
	m_pickupParachute(nullptr),
	m_pickupTimer(10.0f),
	m_pickupType(0),
	m_pickupSpawn(-0.5f) {

}

GameMode::~GameMode() {

}

int GameMode::init(RakNet::RakNetGUID* a_lobbyServerGUID, RakNet::RakNetGUID** a_gameClientGUIDS, unsigned int a_numGameClients) {
	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		m_gameClientGUIDs[i] = nullptr;
		m_players[i] = nullptr;
		m_playerData[i] = nullptr;
	}
	m_currentID = 0;

	m_lobbyServerGUID = new RakNet::RakNetGUID(*a_lobbyServerGUID);
	for (unsigned int i = 0; i < a_numGameClients; ++i) {
		m_gameClientGUIDs[i] = new RakNet::RakNetGUID(*a_gameClientGUIDS[i]);
	}
	m_numGameClients = a_numGameClients;

	m_floors[0] = m_floors[1] = nullptr;
	m_walls[0] = m_walls[1] = nullptr;
	m_flags[0] = m_flags[1] = nullptr;

	std::function<void(RakNet::Packet*, RakNet::BitStream*)> disconnectFunc;
	disconnectFunc = std::bind(&GameMode::onDisconnected, this, std::placeholders::_1, std::placeholders::_2);
	Network.setDisconnectCallback(disconnectFunc);

	std::function<void(RakNet::Packet*, RakNet::BitStream*)> newClientFunc;
	newClientFunc = std::bind(&GameMode::onNewGameClient, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EServerNewGameClient, newClientFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> discClientFunc;
	discClientFunc = std::bind(&GameMode::onDisconnectGameClient, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EServerDisconnectGameClient, discClientFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> updatePlayerFunc;
	updatePlayerFunc = std::bind(&GameMode::onUpdatePlayer, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EServerUpdatePlayer, updatePlayerFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> spawnBulletFunc;
	spawnBulletFunc = std::bind(&GameMode::onSpawnBullet, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EServerSpawnBullet, spawnBulletFunc);

	startGame();
	
	return 0;
}

int GameMode::update() {
	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_players[i]) continue;
		
		m_players[i]->update();
	}

	unsigned int numGameObjects = m_gameObjects.size();
	for (unsigned int i = 0; i < numGameObjects; ++i) {
		m_gameObjects[i]->update();
	}

	glm::vec3 cameraTranslate = glm::vec3(0.0f);
	if (Input.getDown(SDLK_a)) cameraTranslate += glm::vec3(-1.0f, 0.0f, 0.0f) * (float)Time::getDeltaTime();
	if (Input.getDown(SDLK_d)) cameraTranslate += glm::vec3(1.0f, 0.0f, 0.0f) * (float)Time::getDeltaTime();
	if (Input.getDown(SDLK_w)) cameraTranslate += glm::vec3(0.0f, 1.0f, 0.0f) * (float)Time::getDeltaTime();
	if (Input.getDown(SDLK_s)) cameraTranslate += glm::vec3(0.0f, -1.0f, 0.0f) * (float)Time::getDeltaTime();
	Renderer.getCamera()->translate(cameraTranslate);
	if (Input.getReleased(SDLK_r)) Renderer.getCamera()->reset();

	m_updateTimer -= (float)Time::getDeltaTime();
	if (m_updateTimer < 0.0f) {
		sendUpdatePlayer();
		sendUpdateGameObject();
		m_updateTimer = 1.0f / GAME_SERVER_TICK_RATE;
	}

	
	checkFlags();
	if (!m_stopGame) checkArea();
	checkBullets();
	checkPlayers();
	checkPickup();

	if (m_stopGame) m_stopGameTimer -= (float)Time::getDeltaTime();

	return 0;
}

int GameMode::exit() {
	stopGame();

	if (m_lobbyServerGUID) delete m_lobbyServerGUID;
	m_lobbyServerGUID = nullptr;

	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_gameClientGUIDs[i]) delete m_gameClientGUIDs[i];
		m_gameClientGUIDs[i] = nullptr;
	}

	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_players[i]) delete m_players[i];
		m_players[i] = nullptr;	
		if (m_playerData[i]) delete m_playerData[i];
		m_playerData[i] = nullptr;
	}

	unsigned int numGameObjects = m_gameObjects.size();
	for (unsigned int i = 0; i < numGameObjects; ++i) {
		delete m_gameObjects[i];
	}
	m_gameObjects.clear();

	Network.setDisconnectCallback(nullptr);

	Network.setNetworkCallback(EServerNewGameClient, nullptr);
	Network.setNetworkCallback(EServerDisconnectGameClient, nullptr);
	Network.setNetworkCallback(EServerUpdatePlayer, nullptr);
	Network.setNetworkCallback(EServerSpawnBullet, nullptr);

	return 0;
}

void GameMode::onDisconnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	if (RakNet::RakNetGUID::ToUint32(*m_lobbyServerGUID) == RakNet::RakNetGUID::ToUint32(a_packet->guid)) {
		// @TODO: stop server
		Debug.logMessage("Lost connection to LobbyServer");
	}
	else {
		onDisconnectGameClient(a_packet, a_bitstream);
	}
}

void GameMode::onNewGameClient(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	Debug.logMessage("GameClient connected.");

	if (m_numGameClients >= GAME_SERVER_MAX_CLIENTS) {
		Debug.logWarning("Too many gameclients connected, closing connection.");
		Network.closeConnection(a_packet->guid);
		return;
	}

	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_gameClientGUIDs[i] == nullptr) {
			sendUpdatedClientList();
			sendUpdateGameInfo();
		}
	}
}

void GameMode::onDisconnectGameClient(Packet *a_packet, RakNet::BitStream* a_bitStream) {
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;
		if (RakNet::RakNetGUID::ToUint32(*m_gameClientGUIDs[i]) != RakNet::RakNetGUID::ToUint32(a_packet->guid)) continue;

		Debug.logMessage("GameClient disconnected.");
		removePlayer(i);
		return;
	}
}

void GameMode::onUpdatePlayer(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	int networkID;
	a_bitStream->Read(networkID);

	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_players[i]) continue;
		if (m_players[i]->getNetworkID() != networkID) continue;
		
		float posX, posY, velX, velY;
		a_bitStream->Read(posX);
		a_bitStream->Read(posY);
		a_bitStream->Read(velX);
		a_bitStream->Read(velY);

		m_players[i]->setPosition(glm::vec2(posX, posY));
		m_players[i]->setVelocity(glm::vec2(velX, velY));
		return;
	}
}

void GameMode::onSpawnBullet(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	int ownerIndex = -1;
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_gameClientGUIDs[i] && *m_gameClientGUIDs[i] == a_packet->guid) {
			ownerIndex = (int)i;
			break;
		}
	}

	float rotation, velX, velY;
	a_bitStream->Read(rotation);
	a_bitStream->Read(velX);
	a_bitStream->Read(velY);

	unsigned int index = m_gameObjects.size();
	Bullet* newBullet = (Bullet*)addGameObject(new Bullet());
	if (ownerIndex != -1) {
		newBullet->setOwner(m_gameClientGUIDs[ownerIndex]);
		newBullet->setPosition(m_players[ownerIndex]->getPosition() + glm::normalize(glm::vec2(velX, velY)) * 0.05f);
	}
	newBullet->getImage()->m_rotation = rotation;
	newBullet->setVelocity(glm::vec2(velX, velY));
	m_bullets.push_back(newBullet);
	sendSpawnGameObject(index);
	
	m_playerData[ownerIndex]->bulletsFired++;
}

void GameMode::startGame() {
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)EClientStartGame);
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;
		
		Network.sendMessage(&bitStreamOut, *m_gameClientGUIDs[i]);
	}

	bool isRedTeam = true;
	float spawnX = -2.6f;
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;

		m_players[i] = new Player();
		m_playerData[i] = new PlayerData();
		m_players[i]->init();
		m_players[i]->setPosition(glm::vec2(spawnX, -0.375f));
		m_players[i]->setIsGhost(true);
		m_players[i]->setIsRedTeam(isRedTeam);
		if (isRedTeam) m_players[i]->getImage()->m_color = glm::vec4(0.7f + i * 0.025f, 0.3f, 0.3f, 1.0f);
		else m_players[i]->getImage()->m_color = glm::vec4(0.3f, 0.3f, 0.7f + i * 0.025f, 1.0f);

		sendSpawnPlayer(i);

		isRedTeam = !isRedTeam;
		spawnX *= -1.0f;
		if (isRedTeam) spawnX -= 0.1f;
		else spawnX += 0.1f;
	}

	createLevel();

	sendUpdateGameInfo();
	sendUpdatedClientList();
}

void GameMode::stopGame() {
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)EClientStopGame);

	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;

		Network.sendMessage(&bitStreamOut, *m_gameClientGUIDs[i]);
	}
}

void GameMode::createLevel() {
	unsigned int index = m_gameObjects.size();

	// Spawn floor+walls
	for (unsigned int i = 0; i < 2; ++i) {
		m_floors[i] = addGameObject(new GameObject());
		m_floors[i]->getImage()->m_size = glm::vec2(2.0f, 0.1f);
		m_floors[i]->setPosition(glm::vec2(-2.0f + 4.0f * i, -0.45f));
		m_floors[i]->getImage()->m_correctForAspectRatio = false;
	}
	m_floors[0]->getImage()->m_color = glm::vec4(0.85f, 0.35f, 0.35f, 1.0f);
	m_floors[1]->getImage()->m_color = glm::vec4(0.35f, 0.35f, 0.85f, 1.0f);

	for (unsigned int i = 0; i < 2; ++i) {
		m_walls[i] = addGameObject(new GameObject());
		m_walls[i]->getImage()->m_size = glm::vec2(5.0f, 5.0f);
		m_walls[i]->setPosition(glm::vec2(-5.5f + i * 11.0f, 2.0f));
		m_walls[i]->getImage()->m_correctForAspectRatio = false;
	}
	m_walls[0]->getImage()->m_color = glm::vec4(0.85f, 0.35f, 0.35f, 1.0f);
	m_walls[1]->getImage()->m_color = glm::vec4(0.35f, 0.35f, 0.85f, 1.0f);
	

	// Spawn flags
	m_flags[0] = (Flag*)addGameObject(new Flag());
	m_flags[0]->setPosition(glm::vec2(-2.5f, -0.35f));
	m_flags[0]->getImage()->m_color = glm::vec4(0.7f, 0.3f, 0.3f, 1.0f);
	m_flags[0]->setIsRedTeam(true);
	m_flags[1] = (Flag*)addGameObject(new Flag());
	m_flags[1]->setPosition(glm::vec2(2.5f, -0.35f));
	m_flags[1]->getImage()->m_color = glm::vec4(0.3f, 0.3f, 0.7f, 1.0f);
	m_flags[1]->setIsRedTeam(false);

	m_captureButton = (CaptureButton*)addGameObject(new CaptureButton());
	m_captureNPC = (CaptureNPC*)addGameObject(new CaptureNPC());
	m_captureFloor = (CaptureFloor*)addGameObject(new CaptureFloor());

unsigned int index2 = m_gameObjects.size();
for (unsigned int i = index; i < index2; ++i) {
	sendSpawnGameObject(i);
}
}

void GameMode::checkFlags() {
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_players[i]) continue;

		glm::vec2 difFlagRed = m_flags[0]->getPosition() - m_players[i]->getPosition();
		glm::vec2 difFlagBlue = m_flags[1]->getPosition() - m_players[i]->getPosition();
		bool flagRedOnPlace = m_flags[0]->getPosition() == glm::vec2(-2.5f, -0.35f);
		bool flagBlueOnPlace = m_flags[1]->getPosition() == glm::vec2(2.5f, -0.35f);
		bool isRedTeam = m_players[i]->getIsRedTeam();

		//if (m_flags[0]->getOwner() != m_players[i] && m_flags[1]->getOwner() != m_players[i]) {
			if (glm::length2(difFlagRed) < 0.05f * 0.05f) {
				if (flagRedOnPlace) {
					if (!isRedTeam) {
						// Steal red flag
						if (m_flags[1]->getOwner() == m_players[i]) m_flags[1]->setOwner(nullptr);
						m_flags[0]->setOwner(m_players[i]);
						m_playerData[i]->flagsStolen++;
					}
				}
				else {
					// Pickup flag, no matter which team
					if (m_flags[1]->getOwner() == m_players[i]) m_flags[1]->setOwner(nullptr);
					m_flags[0]->setOwner(m_players[i]);
				}
			}

			if (glm::length2(difFlagBlue) < 0.05f * 0.05f) {
				if (flagBlueOnPlace) {
					if (isRedTeam) {
						// Steal red flag
						if (m_flags[0]->getOwner() == m_players[i]) m_flags[0]->setOwner(nullptr);
						m_flags[1]->setOwner(m_players[i]);
						m_playerData[i]->flagsStolen++;
					}
				}
				else {
					// Pickup flag, no matter which team
					if (m_flags[0]->getOwner() == m_players[i]) m_flags[0]->setOwner(nullptr);
					m_flags[1]->setOwner(m_players[i]);
				}
			}
		//}

		if (m_flags[0]->getOwner() == m_players[i]) {
			// Secure flag
			if (isRedTeam && glm::length2(glm::vec2(-2.5f, -0.35f) - m_players[i]->getPosition()) < 0.05f * 0.05f) {
				m_flags[0]->setOwner(nullptr);
				m_flags[0]->setPosition(glm::vec2(-2.5f, -0.35f));
				m_playerData[i]->flagsRetrieved++;
			}
			// Capture flag
			if (!isRedTeam && glm::length2(glm::vec2(2.5f, -0.35f) - m_players[i]->getPosition()) < 0.05f * 0.05f && flagBlueOnPlace) {
				m_flags[0]->setOwner(nullptr);
				m_flags[0]->setPosition(glm::vec2(-2.5f, -0.35f));
				m_playerData[i]->flagsCaptured++;
				m_blueTeamScore++;
				sendUpdateGameInfo();
			}
		}

		if (m_flags[1]->getOwner() == m_players[i]) {
			// Secure flag
			if (!isRedTeam && glm::length2(glm::vec2(2.5f, -0.35f) - m_players[i]->getPosition()) < 0.05f * 0.05f) {
				m_flags[1]->setOwner(nullptr);
				m_flags[1]->setPosition(glm::vec2(2.5f, -0.35f));
				m_playerData[i]->flagsRetrieved++;
			}
			// Capture flag
			if (isRedTeam && glm::length2(glm::vec2(-2.5f, -0.35f) - m_players[i]->getPosition()) < 0.05f * 0.05f && flagRedOnPlace) {
				m_flags[1]->setOwner(nullptr);
				m_flags[1]->setPosition(glm::vec2(2.5f, -0.35f));
				m_playerData[i]->flagsCaptured++;
				m_redTeamScore++;
				sendUpdateGameInfo();
			}
		}
	}

	if (!m_flags[0]->getOwner()) {
		m_flags[0]->setPosition(glm::vec2(m_flags[0]->getPosition().x, -0.35f));
		m_flags[0]->setVelocity(glm::vec2(0.0f));
		m_flags[0]->setIsDirty(true);
	}
	if (!m_flags[1]->getOwner()) {
		m_flags[1]->setPosition(glm::vec2(m_flags[1]->getPosition().x, -0.35f));
		m_flags[1]->setVelocity(glm::vec2(0.0f));
		m_flags[1]->setIsDirty(true);
	}

	if (m_blueTeamScore >= 2 || m_redTeamScore >= 2) {
		if (m_stopGame) return;
		m_stopGame = true;

		unsigned int numGameObjects = m_gameObjects.size();
		for (unsigned int i = 0; i < numGameObjects; ++i) {
			sendDespawnGameObject(i);
		}

		sendUpdateGameInfo();
	}
}

void GameMode::checkArea() {
	// Check area capture
	if (m_captureButton && !m_captureButton->getIsActivated()) {
		for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
			if (!m_players[i]) continue;

			glm::vec2 dif = m_captureButton->getPosition() - m_players[i]->getPosition();
			if (glm::length2(dif) <= 0.025f * 0.025f) {
				m_captureButton->setIsActivated(true);
				m_captureButton->setIsRedTeam(m_players[i]->getIsRedTeam());
				m_captureNPC->setIsActivated(true);
				m_captureNPC->setIsRedTeam(m_players[i]->getIsRedTeam());
				m_captureNPC->setHealth(100.0f);
				m_captureNPC->setOwner(m_gameClientGUIDs[i]);
				m_captureFloor->setIsActivated(true);
				m_captureFloor->setIsRedTeam(m_players[i]->getIsRedTeam());
			}
		}
	}
	else {
		// Check for NPC shooting
		if (m_captureNPC && m_captureNPC->getIsActivated() && m_captureNPC->getShootCooldown() <= 0.0f) {
			bool isRedTeam = m_captureNPC->getIsRedTeam();
			int closestPlayerIndex = -1;
			float closestPlayerDistance = 1e32f;
			for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
				if (!m_players[i]) continue;
				if (!m_players[i]->getIsSpawned() || m_players[i]->getIsRedTeam() == isRedTeam) continue;

				float dif = glm::length2(m_captureNPC->getPosition() - m_players[i]->getPosition());
				if (dif < closestPlayerDistance) {
					closestPlayerDistance = dif;
					closestPlayerIndex = i;
				}
			}

			if (closestPlayerIndex != -1) {
				glm::vec2 velocity = glm::normalize(m_players[closestPlayerIndex]->getPosition() - m_captureNPC->getPosition()) * 0.01f;
				float rotation = glm::atan(velocity.y / velocity.x);
				m_captureNPC->getImage()->m_rotation = rotation;
				m_captureNPC->setShootCooldown(0.5f);

				// Spawn bullet
				unsigned int index = m_gameObjects.size();
				Bullet* newBullet = (Bullet*)addGameObject(new Bullet());
				newBullet->setOwner(m_captureNPC->getOwner());
				newBullet->setPosition(m_captureNPC->getPosition() + glm::normalize(velocity) * 0.1f);
				newBullet->getImage()->m_rotation = rotation;
				newBullet->setVelocity(velocity);
				m_bullets.push_back(newBullet);
				sendSpawnGameObject(index);
			}
		}

		// Check for deactivation
		if ((m_captureButton && m_captureButton->getCooldownTimer() <= 0.0f) ||
			(m_captureNPC && !m_captureNPC->getIsActivated())) {
			m_captureButton->setIsActivated(false);
			m_captureNPC->setIsActivated(false);
			m_captureFloor->setIsActivated(false);
			m_captureNPC->setOwner(nullptr);
		}
	}
}

void GameMode::checkBullets() {
	int numBullets = (int)m_bullets.size();
	unsigned int numGameObjects = m_gameObjects.size();
	for (int i = numBullets - 1; i >= 0; --i) {
		glm::vec2 bulletPos = m_bullets[i]->getPosition();

		// Check boundaries
		if (bulletPos.y > 2.0f || bulletPos.y < -0.4f || bulletPos.x < -3.0f || bulletPos.x > 3.0f) {
			// Despawn bullet
			for (unsigned int j = 0; j < numGameObjects; ++j) {
				if (m_gameObjects[j]->getNetworkID() == m_bullets[i]->getNetworkID()) {
					m_bullets.erase(m_bullets.begin() + i);
					removeGameObject(j);
					break;
				}
			}
			continue;
		}

		// Check NPC collisions
		glm::vec2 npcPos = m_captureNPC->getPosition();
		glm::vec2 npcSizeHalf = m_captureNPC->getImage()->m_size * 0.5f;
		if (bulletPos.x > npcPos.x - npcSizeHalf.x && bulletPos.x < npcPos.x + npcSizeHalf.x &&
			bulletPos.y > npcPos.y - npcSizeHalf.y && bulletPos.y < npcPos.y + npcSizeHalf.y) {
			// Damage player
			m_captureNPC->setHealth(m_captureNPC->getHealth() - 10.0f);

			// Despawn bullet
			for (unsigned int k = 0; k < numGameObjects; ++k) {
				if (m_gameObjects[k]->getNetworkID() == m_bullets[i]->getNetworkID()) {
					m_bullets.erase(m_bullets.begin() + i);
					removeGameObject(k);
					break;
				}
			}
			continue;;
		}

		// Check player collisions
		bool isRedTeam = false;
		for (unsigned int j = 0; j < GAME_SERVER_MAX_CLIENTS; ++j) {
			if (!m_players[j]) continue;
			
			if (m_gameClientGUIDs[j] == m_bullets[i]->getOwner()) {
				isRedTeam = m_players[j]->getIsRedTeam();
			}
		}

		for (int j = GAME_SERVER_MAX_CLIENTS - 1; j >= 0; --j) {
			if (!m_players[j]) continue;
			if (!m_players[j]->getIsSpawned()) continue;
			if (m_players[j]->getIsRedTeam() == isRedTeam) continue;

			glm::vec2 playerPos = m_players[j]->getPosition();
			glm::vec2 playerSizeHalf = m_players[j]->getImage()->m_size * 0.5f;
			if (bulletPos.x > playerPos.x - playerSizeHalf.x && bulletPos.x < playerPos.x + playerSizeHalf.x &&
				bulletPos.y > playerPos.y - playerSizeHalf.y && bulletPos.y < playerPos.y + playerSizeHalf.y) {
				// Damage player
				float oldHealth = m_players[j]->getHealth();
				m_players[j]->setHealth(oldHealth - 25.0f);

				for (unsigned int k = 0; k < GAME_SERVER_MAX_CLIENTS; ++k) {
					if (!m_players[k]) continue;

					if (m_bullets[i]->getOwner() == m_gameClientGUIDs[k]) {						
						m_playerData[k]->bulletsHit++;
						m_players[j]->addAssistIndex(k);

						if (oldHealth > 0.0f && m_players[j]->getHealth() <= 0.0f) {
							m_playerData[k]->kills++;

							// Check assists
							for (unsigned int l = 0; l < 3; ++l) {
								unsigned int assistIndex = m_players[j]->getAssistIndex(l);
								if (assistIndex == -1 || assistIndex == k || !m_players[assistIndex]) continue;
								m_playerData[assistIndex]->assists++;
							}
							m_players[j]->clearAssistIndices();
						}
						break;
					}
				}

				// Despawn bullet
				for (unsigned int k = 0; k < numGameObjects; ++k) {
					if (m_gameObjects[k]->getNetworkID() == m_bullets[i]->getNetworkID()) {
						m_bullets.erase(m_bullets.begin() + i);
						removeGameObject(k);
						break;
					}
				}
				break;
			}
		}
	}
}

void GameMode::checkPlayers() {
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_players[i]) continue;
			
		// Check death
		if (m_players[i]->getHealth() <= 0.0f) {
			sendDespawnPlayer(i);
			if (m_players[i]->getIsRedTeam()) m_players[i]->setPosition(glm::vec2(-2.6f, -0.375f));
			else m_players[i]->setPosition(glm::vec2(2.6f, -0.375f));

			m_players[i]->setHealth(100);
			m_players[i]->setSpawnCooldown(5.0f);
			m_players[i]->setVelocity(glm::vec2(0.0f));

			// Check if it was carrying a flag
			if (m_flags[0]->getOwner() == m_players[i]) m_flags[0]->setOwner(nullptr);
			if (m_flags[1]->getOwner() == m_players[i]) m_flags[1]->setOwner(nullptr);

			m_playerData[i]->deaths++;
		}

		// Check respawn
		if (!m_players[i]->getIsSpawned() && m_players[i]->getSpawnCooldown() <= 0.0f) {
			if (m_players[i]->getIsRedTeam()) m_players[i]->setPosition(glm::vec2(-2.6f, -0.375f));
			else m_players[i]->setPosition(glm::vec2(2.6f, -0.375f));
			m_players[i]->setHealth(100);
			m_players[i]->setVelocity(glm::vec2(0.0f));

			sendSpawnPlayer(i);
		}
	}
}

void GameMode::checkPickup() {
	m_pickupTimer -= (float)Time::getDeltaTime();

	if (!m_pickup) {
		if (m_pickupTimer <= 0.0f) {
			unsigned int index = m_gameObjects.size();
			m_pickup = (Pickup*)addGameObject(new Pickup());
			m_pickup->setPickupType((PickupType)((m_pickupType++) % ENumPickupTypes));
			m_pickup->setPosition(glm::vec2(m_pickupSpawn, 0.0f));
			m_pickupParachute = addGameObject(new GameObject());
			m_pickupParachute->getImage()->m_rotation = glm::pi<float>() / 4.0f;
			m_pickupParachute->getImage()->m_size = glm::vec2(0.2f);
			m_pickupParachute->setPosition(m_pickup->getPosition() + glm::vec2(0.0f, 0.18f));
			m_pickupParachute->setVelocity(m_pickup->getVelocity());
			m_pickup->setIsDirty(true);
			m_pickupParachute->setIsDirty(true);
			m_pickupSpawn *= -1.0f;
			sendSpawnGameObject(index);
			sendSpawnGameObject(index + 1);
		}
	}
	else {
		if (m_pickupParachute && m_pickup->getPosition().y <= -0.375f) {
			unsigned int numGameObjects = m_gameObjects.size();
			for (unsigned int j = 0; j < numGameObjects; ++j) {
				if (m_gameObjects[j]->getNetworkID() == m_pickupParachute->getNetworkID()) {
					removeGameObject(j);
					break;
				}
			}
			m_pickupParachute = nullptr;
		}

		for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
			if (!m_players[i]) continue;

			glm::vec2 dif = m_pickup->getPosition() - m_players[i]->getPosition();
			if (glm::length2(dif) <= 0.025f * 0.025f) {
				switch (m_pickup->getPickupType()) {
				case ETeleportRed:
					sendDespawnPlayer(i);
					m_players[i]->setPosition(glm::vec2(-2.0f, 0.0f));
					m_players[i]->setVelocity(glm::vec2(0.0f));
					sendSpawnPlayer(i);
					break;
				case ETeleportBlue:
					sendDespawnPlayer(i);
					m_players[i]->setPosition(glm::vec2(2.0f, 0.0f));
					m_players[i]->setVelocity(glm::vec2(0.0f));
					sendSpawnPlayer(i);
					break;
				}
				
				unsigned int numGameObjects = m_gameObjects.size();
				for (unsigned int j = 0; j < numGameObjects; ++j) {
					if (m_gameObjects[j]->getNetworkID() == m_pickup->getNetworkID()) {
						if (m_pickupParachute) removeGameObject(j + 1);
						removeGameObject(j);
						break;
					}
				}
				m_pickup = nullptr;
				m_pickupParachute = nullptr;
				m_pickupTimer = 30.0f;
				
				break;
			}
		}
	}
}

void GameMode::removePlayer(unsigned int a_index) {
	if (!m_gameClientGUIDs[a_index]) return;

	// Disconnect (unnecessary most of the time)
	Network.closeConnection(*m_gameClientGUIDs[a_index]);

	sendDespawnPlayer(a_index);

	// Remove bullets of this player
	int numBullets = m_bullets.size();
	unsigned int numGameObjects = m_gameObjects.size();
	for (int i = numBullets - 1; i >= 0; --i) {
		if (m_bullets[i]->getOwner() == m_gameClientGUIDs[a_index]) {
			for (unsigned int j = 0; j < numGameObjects; ++j) {
				if (m_gameObjects[j]->getNetworkID() == m_bullets[i]->getNetworkID()) {
					m_bullets.erase(m_bullets.begin() + i);
					removeGameObject(j);
					break;
				}
			}
		}
	}

	// Check if it was carrying a flag
	if (m_flags[0]->getOwner() == m_players[a_index]) m_flags[0]->setOwner(nullptr);
	if (m_flags[1]->getOwner() == m_players[a_index]) m_flags[1]->setOwner(nullptr);

	// Check if it captured the area
	if (m_captureNPC && m_captureNPC->getOwner() && m_captureNPC->getOwner() == m_gameClientGUIDs[a_index]) {
		m_captureButton->setIsActivated(false);
		m_captureNPC->setIsActivated(false);
		m_captureFloor->setIsActivated(false);
		m_captureNPC->setOwner(nullptr);
	}

	delete m_players[a_index];
	m_players[a_index] = nullptr;
	delete m_playerData[a_index];
	m_playerData[a_index] = nullptr;
	delete m_gameClientGUIDs[a_index];
	m_gameClientGUIDs[a_index] = nullptr;
	m_numGameClients--;

	if (m_numGameClients < 2) {
		m_stopGameTimer = -1.0f;
	}

	sendUpdatedClientList();
}

void GameMode::removeGameObject(unsigned int a_index) {
	sendDespawnGameObject(a_index);

	if (m_gameObjects[a_index]) delete m_gameObjects[a_index];
	m_gameObjects.erase(m_gameObjects.begin() + a_index);
}

GameObject* GameMode::addGameObject(GameObject* a_gameObject) {
	unsigned int index = m_gameObjects.size();
	m_gameObjects.push_back(a_gameObject);
	a_gameObject->init();

	return a_gameObject;
}

void GameMode::sendUpdateGameInfo() {
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)EClientUpdateGameInfo);
	bitStreamOut.Write(m_redTeamScore);
	bitStreamOut.Write(m_blueTeamScore);

	// If the game ended send stat info
	bitStreamOut.Write(m_stopGame);
	if (m_stopGame) {
		bitStreamOut.Write(m_numGameClients);
		for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
			if (!m_players[i]) continue;

			bitStreamOut.Write(m_players[i]->getImage()->m_color.r);
			bitStreamOut.Write(m_players[i]->getImage()->m_color.g);
			bitStreamOut.Write(m_players[i]->getImage()->m_color.b);
			bitStreamOut.Write(m_players[i]->getImage()->m_color.a);
			bitStreamOut.Write(m_playerData[i]->kills);
			bitStreamOut.Write(m_playerData[i]->deaths);
			bitStreamOut.Write(m_playerData[i]->assists);
			bitStreamOut.Write(m_playerData[i]->flagsStolen);
			bitStreamOut.Write(m_playerData[i]->flagsRetrieved);
			bitStreamOut.Write(m_playerData[i]->flagsCaptured);
			float accuracy = ((float)m_playerData[i]->bulletsHit) / ((float)m_playerData[i]->bulletsFired);
			if ((float)m_playerData[i]->bulletsFired == 0) accuracy = 0.0f;
			bitStreamOut.Write(accuracy * 100);
		}
	}
	
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;

		Network.sendMessage(&bitStreamOut, *m_gameClientGUIDs[i]);
	}
}

void GameMode::sendUpdatedClientList() {
	// Notify the LobbyServer
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)ELobbyUpdateGameServer);
	bitStreamOut.Write(m_numGameClients);
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;

		bitStreamOut.Write(m_gameClientGUIDs[i]->g);
	}
	bitStreamOut.Write(true);
	Network.sendMessage(&bitStreamOut, *m_lobbyServerGUID);
}

void GameMode::sendSpawnPlayer(unsigned int a_index) {
	// Broadcast message for spawning player
	m_players[a_index]->setNetworkID(m_currentID++);
	m_players[a_index]->setIsSpawned(true);
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;

		BitStream bitStreamSpawn;
		bitStreamSpawn.Write((MessageID)EClientSpawnPlayer);
		bitStreamSpawn.Write(m_players[a_index]->getNetworkID());
		bitStreamSpawn.Write(a_index != i);
		bitStreamSpawn.Write(m_players[a_index]->getPosition().x);
		bitStreamSpawn.Write(m_players[a_index]->getPosition().y);
		bitStreamSpawn.Write(m_players[a_index]->getImage()->m_color.r);
		bitStreamSpawn.Write(m_players[a_index]->getImage()->m_color.g);
		bitStreamSpawn.Write(m_players[a_index]->getImage()->m_color.b);
		bitStreamSpawn.Write(m_players[a_index]->getImage()->m_color.a);
		Network.sendMessage(&bitStreamSpawn, *m_gameClientGUIDs[i]);
	}
}

void GameMode::sendSpawnGameObject(unsigned int a_index) {
	// Broadcast message for spawning player
	BitStream bitStreamSpawn;
	bitStreamSpawn.Write((MessageID)EClientSpawnGameObject);
	m_gameObjects[a_index]->setNetworkID(m_currentID++);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getNetworkID());
	bitStreamSpawn.Write(true);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getPosition().x);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getPosition().y);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getVelocity().x);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getVelocity().y);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getImage()->m_color.r);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getImage()->m_color.g);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getImage()->m_color.b);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getImage()->m_color.a);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getImage()->m_size.x);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getImage()->m_size.y);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getImage()->m_rotation);
	bitStreamSpawn.Write(m_gameObjects[a_index]->getImage()->m_correctForAspectRatio);
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;

		Network.sendMessage(&bitStreamSpawn, *m_gameClientGUIDs[i]);
	}
}

void GameMode::sendDespawnPlayer(unsigned int a_index) {
	m_players[a_index]->setIsSpawned(false);

	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)EClientDespawnPlayer);
	bitStreamOut.Write(m_players[a_index]->getNetworkID());
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;

		Network.sendMessage(&bitStreamOut, *m_gameClientGUIDs[i]);
	}
}

void GameMode::sendDespawnGameObject(unsigned int a_index) {
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)EClientDespawnGameObject);
	bitStreamOut.Write(m_gameObjects[a_index]->getNetworkID());
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;

		Network.sendMessage(&bitStreamOut, *m_gameClientGUIDs[i]);
	}
}

void GameMode::sendUpdatePlayer() {
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)EClientUpdatePlayers);

	unsigned int numPlayers = 0;
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_players[i]) numPlayers++;
	}
	bitStreamOut.Write(numPlayers);

	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_players[i]) continue;
		
		bitStreamOut.Write(m_players[i]->getNetworkID());
		bitStreamOut.Write(m_players[i]->getPosition().x + m_players[i]->getVelocity().x * 1.0f / float(GAME_SERVER_TICK_RATE));
		bitStreamOut.Write(m_players[i]->getPosition().y + m_players[i]->getVelocity().x * 1.0f / float(GAME_SERVER_TICK_RATE));
		bitStreamOut.Write(m_players[i]->getVelocity().x);
		bitStreamOut.Write(m_players[i]->getVelocity().y);
	}

	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;

		Network.sendMessage(&bitStreamOut, *m_gameClientGUIDs[i]);
	}
}

void GameMode::sendUpdateGameObject() {
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)EClientUpdateGameObjects);

	unsigned int numGameObjects = m_gameObjects.size();
	unsigned int numDirtyGameObjects = 0;
	for (unsigned int i = 0; i < numGameObjects; ++i) {
		if (m_gameObjects[i]->getIsDirty()) numDirtyGameObjects++;
	}
	bitStreamOut.Write(numDirtyGameObjects);

	for (unsigned int i = 0; i < numGameObjects; ++i) {
		if (!m_gameObjects[i]->getIsDirty()) continue;
		m_gameObjects[i]->setIsDirty(false);

		bitStreamOut.Write(m_gameObjects[i]->getNetworkID());
		bitStreamOut.Write(m_gameObjects[i]->getPosition().x + m_gameObjects[i]->getVelocity().x * 1.0f / float(GAME_SERVER_TICK_RATE));
		bitStreamOut.Write(m_gameObjects[i]->getPosition().y + m_gameObjects[i]->getVelocity().x * 1.0f / float(GAME_SERVER_TICK_RATE));
		bitStreamOut.Write(m_gameObjects[i]->getVelocity().x);
		bitStreamOut.Write(m_gameObjects[i]->getVelocity().y);
		bitStreamOut.Write(m_gameObjects[i]->getImage()->m_color.r);
		bitStreamOut.Write(m_gameObjects[i]->getImage()->m_color.g);
		bitStreamOut.Write(m_gameObjects[i]->getImage()->m_color.b);
		bitStreamOut.Write(m_gameObjects[i]->getImage()->m_color.a);
		bitStreamOut.Write(m_gameObjects[i]->getImage()->m_size.x);
		bitStreamOut.Write(m_gameObjects[i]->getImage()->m_size.y);
		bitStreamOut.Write(m_gameObjects[i]->getImage()->m_rotation);
	}

	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_gameClientGUIDs[i]) continue;

		Network.sendMessage(&bitStreamOut, *m_gameClientGUIDs[i]);
	}
}