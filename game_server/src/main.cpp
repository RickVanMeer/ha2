#include "engine.h"
#include "server_manager.h"

#include <functional>
using std::function;
using std::bind;

int main(int argc, char* argv[]) {
	ServerManager server;
	function<int(int, char**)> initFunc = bind(&ServerManager::init, &server, std::placeholders::_1, std::placeholders::_2);
	function<int()> updateFunc = bind(&ServerManager::update, &server);
	function<int()> exitFunc = bind(&ServerManager::exit, &server);

	Engine engine(initFunc, updateFunc, exitFunc);
	engine.startMainLoop(argc, argv);

	return 0;
}