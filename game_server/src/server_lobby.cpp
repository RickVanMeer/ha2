#include "server_lobby.h"

#include "network_manager.h"
#include "debug.h"
#include "timing.h"

#include "RakPeerInterface.h"
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "RakNetTypes.h"
using namespace RakNet;

const float TIME_TILL_GAME_START = 1.0f;
const int MIN_NUM_CLIENTS = 2;

ServerLobby::ServerLobby():
	m_lobbyServerGUID(nullptr),
	m_numGameClients(0),
	m_numGameClientsReady(0),
	m_readyTimer(-1.0f),
	m_updateTimer(0.5f) {

}

ServerLobby::~ServerLobby() {

}

int ServerLobby::init(RakNet::RakNetGUID* a_lobbyServerGUID, RakNet::RakNetGUID** a_gameClientGUIDS, unsigned int a_numGameClients) {
	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		m_gameClientGUIDs[i] = nullptr;
		m_gameClientsReady[i] = false;
	}

	if (a_lobbyServerGUID) m_lobbyServerGUID = new RakNet::RakNetGUID(*a_lobbyServerGUID);
	if (a_gameClientGUIDS) {
		for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
			if (a_gameClientGUIDS[i]) m_gameClientGUIDs[i] = new RakNet::RakNetGUID(*a_gameClientGUIDS[i]);
		}
	}
	m_numGameClients = a_numGameClients;

	std::function<void(RakNet::Packet*, RakNet::BitStream*)> connectFunc;
	connectFunc = std::bind(&ServerLobby::onConnected, this, std::placeholders::_1, std::placeholders::_2);
	Network.setConnectCallback(connectFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> disconnectFunc;
	disconnectFunc = std::bind(&ServerLobby::onDisconnected, this, std::placeholders::_1, std::placeholders::_2);
	Network.setDisconnectCallback(disconnectFunc);

	std::function<void(RakNet::Packet*, RakNet::BitStream*)> newClientFunc;
	newClientFunc = std::bind(&ServerLobby::onNewGameClient, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EServerNewGameClient, newClientFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> discClientFunc;
	discClientFunc = std::bind(&ServerLobby::onDisconnectGameClient, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EServerDisconnectGameClient, discClientFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> readyClientFunc;
	readyClientFunc = std::bind(&ServerLobby::onGameClientReady, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EServerGameClientReady, readyClientFunc);

	sendGameServerInfo();

	return 0;
}

int ServerLobby::update() {
	if (m_numGameClients >= MIN_NUM_CLIENTS && m_numGameClientsReady == m_numGameClients) {
		if (m_readyTimer == -1.0f) m_readyTimer = TIME_TILL_GAME_START;
		m_readyTimer -= (float)Time::getDeltaTime();
		if (m_readyTimer <= 0.0f) {
			m_startGame = true;
		}
	}

	m_updateTimer -= (float)Time::getDeltaTime();
	if (m_updateTimer <= 0.0f) {
		m_updateTimer = 1.0f / (float)GAME_CLIENT_TICK_RATE;
		if (m_numGameClients >= MIN_NUM_CLIENTS && m_numGameClientsReady == m_numGameClients) {
			sendGameServerInfo();
		}
	}

	return 0;
}

int ServerLobby::exit() {
	if (m_lobbyServerGUID) delete m_lobbyServerGUID;
	m_lobbyServerGUID = nullptr;

	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_gameClientGUIDs[i]) delete m_gameClientGUIDs[i];
		m_gameClientGUIDs[i] = nullptr;
	}

	Network.setConnectCallback(nullptr);
	Network.setDisconnectCallback(nullptr);

	Network.setNetworkCallback(EServerNewGameClient, nullptr);
	Network.setNetworkCallback(EServerDisconnectGameClient, nullptr);
	Network.setNetworkCallback(EServerGameClientReady, nullptr);

	return 0;
}

void ServerLobby::onConnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	Debug.logMessage("Connected to the LobbyServer.");
	if (!m_lobbyServerGUID) m_lobbyServerGUID = new RakNet::RakNetGUID(a_packet->guid);

	// Send a message to confirm that this is a new gameserver
	BitStream bitStreamOut;
	bitStreamOut.Write((RakNet::MessageID) ELobbyNewGameServer);
	Network.sendMessage(&bitStreamOut, *m_lobbyServerGUID);

	sendUpdatedClientList();
}

void ServerLobby::onDisconnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	onDisconnectGameClient(a_packet, a_bitstream);
}

void ServerLobby::onNewGameClient(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	Debug.logMessage("GameClient connected.");

	if (m_numGameClients >= GAME_SERVER_MAX_CLIENTS) {
		Debug.logWarning("Too many gameclients connected, closing connection.");
		Network.closeConnection(a_packet->guid);
		return;
	}

	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_gameClientGUIDs[i] == nullptr) {
			m_gameClientGUIDs[i] = new RakNetGUID(a_packet->guid);
			m_gameClientsReady[i] = false;

			m_numGameClients++;
			break;
		}
	}

	sendUpdatedClientList();
	sendGameServerInfo();
}

void ServerLobby::onDisconnectGameClient(Packet *a_packet, RakNet::BitStream* a_bitStream) {
	unsigned int gameClientIndex = -1;
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_gameClientGUIDs[i] && RakNet::RakNetGUID::ToUint32(*m_gameClientGUIDs[i]) == RakNet::RakNetGUID::ToUint32(a_packet->guid)) {
			gameClientIndex = i;
			break;
		}
	}
	if (gameClientIndex != -1) {
		Debug.logMessage("GameClient disconnected.");

		delete m_gameClientGUIDs[gameClientIndex];
		m_gameClientGUIDs[gameClientIndex] = nullptr;
		if (m_gameClientsReady[gameClientIndex]) m_numGameClientsReady--;
		m_gameClientsReady[gameClientIndex] = false;
		for (unsigned int i = gameClientIndex + 1; i < GAME_SERVER_MAX_CLIENTS; ++i) {
			m_gameClientGUIDs[i - 1] = m_gameClientGUIDs[i];
			m_gameClientsReady[i - 1] = m_gameClientsReady[i];
		}
		m_numGameClients--;
	}

	Network.closeConnection(a_packet->guid);

	sendUpdatedClientList();
	sendGameServerInfo();
}

void ServerLobby::onGameClientReady(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	int gameClientIndex = -1;;
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_gameClientGUIDs[i] && a_packet->guid == *m_gameClientGUIDs[i]) {
			gameClientIndex = i;
			break;
		}
	}
	if (gameClientIndex == -1) return;

	bool clientReady;
	a_bitStream->Read(clientReady);
	if (clientReady) {
		m_numGameClientsReady++;
		m_gameClientsReady[gameClientIndex] = true;
	}
	else {
		m_numGameClientsReady--;
		m_gameClientsReady[gameClientIndex] = false;
	}

	sendGameServerInfo();
}

void ServerLobby::sendUpdatedClientList() {
	// Notify the LobbyServer
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)ELobbyUpdateGameServer);
	bitStreamOut.Write(m_numGameClients);
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_gameClientGUIDs[i]) bitStreamOut.Write(m_gameClientGUIDs[i]->g);
	}
	bitStreamOut.Write(false);
	Network.sendMessage(&bitStreamOut, *m_lobbyServerGUID);
}

void ServerLobby::sendGameServerInfo() {
	// Send game server info to gameclients
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)EClientUpdateGameServerInfo);
	bitStreamOut.Write(m_numGameClients);
	bitStreamOut.Write(GAME_SERVER_MAX_CLIENTS);
	bitStreamOut.Write(m_numGameClientsReady);
	bitStreamOut.Write((int)m_readyTimer);

	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_gameClientGUIDs[i]) Network.sendMessage(&bitStreamOut, *m_gameClientGUIDs[i]);
	}
}