#include "network_manager.h"

#include "input_manager.h"
#include "debug.h"
#include "timing.h"
#include "game_object.h"
#include "player.h"
#include "image.h"

#include "RakPeerInterface.h"
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "RakNetTypes.h"
using namespace RakNet;

NetworkManager Network = NetworkManager();

const float TIME_TILL_GAME_START = 1.0f;

RakNet::RakNetGUID lobbyServerGUID;
RakNet::RakNetGUID connectedGUID = RakNet::RakNetGUID();

NetworkManager::NetworkManager() {
}

NetworkManager::~NetworkManager() {
}

int NetworkManager::init() {
	Debug.logMessage("Initializing GameServer...");

	for (int i = ID_USER_PACKET_ENUM; i < ENumNetworkMessages - 1; ++i) {
		m_networkCallbacks[i - ID_USER_PACKET_ENUM] = nullptr;
	}
	m_connectCallback = nullptr;
	m_disconnectCallback = nullptr;

	startServer();

	Debug.logMessage("GameServer initialized!");
	return 0;
}

int NetworkManager::update() {
	if (Input.getReleased(SDLK_ESCAPE)) return 1;

	if (handleMessages() != 0) return 1;

	return 0;
}

int NetworkManager::exit() {
	Debug.logMessage("Exiting GameServer...");

	stopServer();

	Debug.logMessage("GameServer Exited!");
	return 0;
}

void NetworkManager::connect(const char* a_ip, unsigned short a_port) {
	if (m_peer) {
		if (connectedGUID != UNASSIGNED_RAKNET_GUID) {
			closeConnections();
		}
		m_peer->Connect(a_ip, a_port, 0, 0);
		Debug.logMessage("Connecting...");
	}
}

void NetworkManager::closeConnection(const RakNet::RakNetGUID& a_GUID) {
	m_peer->CloseConnection(a_GUID, true);
}

void NetworkManager::sendMessage(RakNet::BitStream* a_bitStream, const RakNet::RakNetGUID& a_GUID) {
	m_peer->Send(a_bitStream, HIGH_PRIORITY, RELIABLE_ORDERED, 0, a_GUID, false);
}

int NetworkManager::getPing(RakNet::RakNetGUID* a_GUID) {
	return m_peer->GetLastPing(*a_GUID);
}

void NetworkManager::setNetworkCallback(NetworkMessageID a_messageID, std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback) {
	if (a_messageID < ID_USER_PACKET_ENUM || a_messageID >= ENumNetworkMessages) return;

	m_networkCallbacks[a_messageID - ID_USER_PACKET_ENUM] = a_callback;
}

void NetworkManager::setConnectCallback(std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback) {
	m_connectCallback = a_callback;
}

void NetworkManager::setDisconnectCallback(std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback) {
	m_disconnectCallback = a_callback;
}

int NetworkManager::startServer() {
	if (m_peer) {
		stopServer();
	}

	m_peer = RakPeerInterface::GetInstance();
	if (!m_peer) return 1;
	SocketDescriptor socketDescriptor;
	m_peer->Startup(GAME_SERVER_MAX_CLIENTS + 1, &socketDescriptor, 1);
	m_peer->SetMaximumIncomingConnections(GAME_SERVER_MAX_CLIENTS);

	// Connect to the lobby
	m_peer->Connect(LOBBY_SERVER_ADDRESS, LOBBY_SERVER_PORT, 0, 0);

	Debug.logMessage("Started the server.");
	return 0;
}

void NetworkManager::stopServer() {
	if (!m_peer) return;

	closeConnections();
	m_peer->Shutdown(1000);
	RakPeerInterface::DestroyInstance(m_peer);
	m_peer = nullptr;

	Debug.logMessage("Stopped the server.");
}

int NetworkManager::restartServer() {
	if (m_peer) {
		stopServer();
	}
	if (startServer() != 0) return 2;

	return 0;
}

int NetworkManager::handleMessages() {
	if (!m_peer) return 0;

	// Handle all incoming messages
	Packet *messagePacket;
	for (messagePacket = m_peer->Receive(); messagePacket; m_peer->DeallocatePacket(messagePacket), messagePacket = m_peer->Receive()) {
		RakNet::BitStream bitStreamIn(messagePacket->data, messagePacket->length, false);
		RakNet::MessageID messageID;
		bitStreamIn.Read(messageID);

		switch (messageID) {
		case ID_NEW_INCOMING_CONNECTION:
			Debug.logMessage("New connection incoming.");
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			if (m_connectCallback) m_connectCallback(messagePacket, &bitStreamIn);
			break;
		case ID_INVALID_PASSWORD:
		case ID_CONNECTION_ATTEMPT_FAILED: 
		case ID_NO_FREE_INCOMING_CONNECTIONS: 
			onConnectionFailed(messageID);
			break;
		case ID_DISCONNECTION_NOTIFICATION:
		case ID_CONNECTION_LOST: 
			if (m_disconnectCallback) m_disconnectCallback(messagePacket, &bitStreamIn);
			break;

		// Ignore:
		case EClientGameServerlist:
		case EClientCreateGameServer:
		case ELobbyDisconnectGameClient:
		case ELobbyNewGameClient:
		case EServerNewGameClient:
		case EServerDisconnectGameClient:
		case EServerGameClientReady:
		case EServerUpdatePlayer:
		case EServerSpawnBullet:
			break;

		default: 
			Debug.logWarning("Received message with messageID %d; unknown messageID.", messageID);
			break;
		}

		if (messageID >= ID_USER_PACKET_ENUM && messageID < ENumNetworkMessages && m_networkCallbacks[messageID - ID_USER_PACKET_ENUM]) m_networkCallbacks[messageID - ID_USER_PACKET_ENUM](messagePacket, &bitStreamIn);
	}

	return 0;
}

void NetworkManager::onConnectionFailed(unsigned int a_failureType) {
	switch (a_failureType) {
		case ID_CONNECTION_ATTEMPT_FAILED: 
			Debug.logWarning("Couldn't connect to the LobbyServer: timeout");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS: 
			Debug.logWarning("Couldn't connect to the LobbyServer: full.");	
			break;
		case ID_INVALID_PASSWORD: 
			Debug.logWarning("Couldn't connect to the LobbyServer: wrong password.");		
			break;
		default:
			Debug.logWarning("Couldn't connect to the LobbyServer");
	}
}

void NetworkManager::closeConnections() {
	//// Close connections with all clients first
	//for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
	//	if (m_gameClientGUIDs[i] != nullptr) {
	//		m_peer->CloseConnection(*m_gameClientGUIDs[i], true);
	//		delete m_gameClientGUIDs[i];
	//		m_gameClientGUIDs[i] = nullptr;
	//	}
	//}

	// Close connection with lobby
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)ELobbyDisconnectGameServer);
	m_peer->Send(&bitStreamOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, lobbyServerGUID, false);
}