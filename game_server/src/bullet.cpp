#include "bullet.h"

#include "image.h"
#include "RakNetTypes.h"
using namespace RakNet;

Bullet::Bullet() :
	GameObject(),
	m_owner(nullptr) {

}

Bullet::~Bullet() {

}

int Bullet::init() {
	GameObject::init();

	m_image->m_size = glm::vec2(0.020f, 0.005f);

	return 0;
}

void Bullet::customUpdate() {

}

int Bullet::exit() {
	return GameObject::exit();
}

void Bullet::setOwner(RakNet::RakNetGUID* a_owner) {
	m_owner = a_owner;
}