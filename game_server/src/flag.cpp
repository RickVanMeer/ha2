#include "flag.h"

#include "player.h"
#include "image.h"
#include "timing.h"

Flag::Flag() :
	GameObject(),
	m_owner(nullptr) {

}

Flag::~Flag() {

}

int Flag::init() {
	GameObject::init();

	m_image->m_size = glm::vec2(0.025f, 0.025f);

	return 0;
}

void Flag::customUpdate() {
	if (m_owner) {
		m_position = m_owner->getPosition() + glm::vec2(0.0f, 0.1f);
	}
	m_velocity = glm::vec2(0.0f);
	m_image->m_rotation = (float)Time::getRunningTime();
	m_image->m_size = glm::vec2(0.02f + 0.01f * glm::sin((float)Time::getRunningTime()));

	if (m_owner) {
		m_velocity = m_owner->getVelocity();
	}
}

int Flag::exit() {

	return GameObject::exit();
}

void Flag::setOwner(Player* a_owner) {
	m_owner = a_owner;
}


void Flag::setIsRedTeam(bool a_isRedTeam) {
	m_isRedTeam = a_isRedTeam;
}