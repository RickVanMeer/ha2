#include "server_manager.h"

#include "input_manager.h"
#include "network_manager.h"
#include "render_manager.h"
#include "game_mode.h"
#include "server_lobby.h"
#include "timing.h"

#include "RakPeerInterface.h"
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "RakNetTypes.h"
using namespace RakNet;

ServerManager::ServerManager() :
	m_serverLobby(nullptr),
	m_gameMode(nullptr) {

}

ServerManager::~ServerManager() {

}

int ServerManager::init(int argc, char* argv[]) {
	Network.init();

	m_serverLobby = new ServerLobby();
	m_serverLobby->init(nullptr, nullptr, 0);

	return 0;
}

int ServerManager::update() {
	if (Input.getPressed(SDLK_ESCAPE)) return 1;

	Network.update();
	if (m_serverLobby) {
		if (m_serverLobby->update() != 0) return 1;

		if (m_serverLobby->getStartGame()) {
			// Retrieve data from ServerLobby before deleting it
			RakNet::RakNetGUID* lobbyServerGUID = new RakNet::RakNetGUID(*m_serverLobby->getLobbyServerGUID());
			unsigned int numGameClients = m_serverLobby->getNumGameClients();
			RakNet::RakNetGUID* gameClientGUIDs[GAME_SERVER_MAX_CLIENTS];
			for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
				if (m_serverLobby->getGameClientGUIDs()[i]) gameClientGUIDs[i] = new RakNet::RakNetGUID(*m_serverLobby->getGameClientGUIDs()[i]);
				else gameClientGUIDs[i] = nullptr;
			}

			// Delete ServerLobby, create GameMode
			m_serverLobby->exit();
			delete m_serverLobby;
			m_serverLobby = nullptr;
			m_gameMode = new GameMode();
			m_gameMode->init(lobbyServerGUID, gameClientGUIDs, numGameClients);

			// Delete data again
			if (lobbyServerGUID) delete lobbyServerGUID;
			for (unsigned int i = 0; i < numGameClients; ++i) {
				if (gameClientGUIDs[i]) delete gameClientGUIDs[i];
			}
		}
	}
	if (m_gameMode) {
		if (m_gameMode->update() != 0) return 2;

		if (m_gameMode->getStopGame()) {
			// Retrieve data from ServerLobby before deleting it
			RakNet::RakNetGUID* lobbyServerGUID = new RakNet::RakNetGUID(*m_gameMode->getLobbyServerGUID());
			unsigned int numGameClients = m_gameMode->getNumGameClients();
			RakNet::RakNetGUID* gameClientGUIDs[GAME_SERVER_MAX_CLIENTS];
			for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
				if (m_gameMode->getGameClientGUIDs()[i]) gameClientGUIDs[i] = new RakNet::RakNetGUID(*m_gameMode->getGameClientGUIDs()[i]);
				else gameClientGUIDs[i] = nullptr;
			}

			// Delete ServerLobby, create GameMode
			m_gameMode->exit();
			delete m_gameMode;
			m_gameMode = nullptr;
			m_serverLobby = new ServerLobby();
			m_serverLobby->init(lobbyServerGUID, gameClientGUIDs, numGameClients);

			// Delete data again
			if (lobbyServerGUID) delete lobbyServerGUID;
			for (unsigned int i = 0; i < numGameClients; ++i) {
				if (gameClientGUIDs[i]) delete gameClientGUIDs[i];
			}
		}
	}

	return 0;
}

int ServerManager::exit() {
	if (m_serverLobby) {
		m_serverLobby->exit();
		delete m_serverLobby;
		m_serverLobby = nullptr;
	}
	if (m_gameMode) {
		m_gameMode->exit();
		delete m_gameMode;
		m_gameMode = nullptr;
	}

	Network.exit();

	return 0;
}