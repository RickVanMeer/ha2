#pragma once

#include "game_object.h"
#include "raknet_fwd.h"

class Bullet : public GameObject {
public:
	Bullet();
	~Bullet();

	virtual int init() override;
	virtual void customUpdate() override;
	virtual int exit() override;

	// Getters/Setters //
	inline RakNet::RakNetGUID*	getOwner();
	void						setOwner(RakNet::RakNetGUID* a_owner);

private:
	// Data members //
	RakNet::RakNetGUID*	m_owner;
};

RakNet::RakNetGUID* Bullet::getOwner() {
	return m_owner;
}