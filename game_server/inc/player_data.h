#pragma once

struct PlayerData {
	int		kills = 0;
	int		deaths = 0;
	int		assists = 0;
	int		flagsStolen = 0;
	int		flagsRetrieved = 0;
	int		flagsCaptured = 0;
	int		bulletsFired = 0;
	int		bulletsHit = 0;
};