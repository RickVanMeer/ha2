#pragma once

#include "game_object.h"
#include "raknet_fwd.h"

class CaptureNPC : public GameObject {
public:
	CaptureNPC();
	~CaptureNPC();

	virtual int init() override;
	virtual void customUpdate() override;
	virtual int exit() override;

	// Getters/Setters //
	inline bool					getIsRedTeam();
	void						setIsRedTeam(bool a_isRedTeam);
	inline bool					getIsActivated();
	void						setIsActivated(bool a_isActivated);
	inline float				getHealth();
	void						setHealth(float a_health);
	inline float				getShootCooldown();
	void						setShootCooldown(float a_shootCooldown);
	inline RakNet::RakNetGUID*	getOwner();
	void						setOwner(RakNet::RakNetGUID* a_owner);

private:
	// Data members //
	bool				m_isRedTeam;
	bool				m_isActivated;
	float				m_health;
	float				m_shootCooldown;
	RakNet::RakNetGUID*	m_owner;
};

bool CaptureNPC::getIsRedTeam() {
	return m_isRedTeam;
}

bool CaptureNPC::getIsActivated() {
	return m_isActivated;
}

float CaptureNPC::getHealth() {
	return m_health;
}

float CaptureNPC::getShootCooldown() {
	return m_shootCooldown;
}

RakNet::RakNetGUID* CaptureNPC::getOwner() {
	return m_owner;
}