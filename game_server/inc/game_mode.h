#pragma once

#include "network.h"
#include "raknet_fwd.h"

class GameObject;

class GameMode {
public:
	GameMode();
	~GameMode();

	// Methods //
	int init(RakNet::RakNetGUID* a_lobbyServerGUID, RakNet::RakNetGUID** a_gameClientGUIDS, unsigned int a_numGameClients);
	int update();
	int exit();

	void onDisconnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);
	void onNewGameClient(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);
	void onDisconnectGameClient(RakNet::Packet *a_packet, RakNet::BitStream* a_bitStream);
	void onUpdatePlayer(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);
	void onSpawnBullet(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);

	// Getters/Setters //
	inline bool					getStopGame();
	inline RakNet::RakNetGUID*	getLobbyServerGUID();
	inline RakNet::RakNetGUID**	getGameClientGUIDs();
	inline unsigned int			getNumGameClients();

private:
	// Methods //
	void		startGame();
	void		stopGame();
	void		createLevel();
	void		checkFlags();
	void		checkArea();
	void		checkBullets();
	void		checkPlayers();
	void		checkPickup();

	void		removePlayer(unsigned int a_index);
	void		removeGameObject(unsigned int a_index);
	GameObject*	addGameObject(GameObject* a_gameObject);

	void sendUpdateGameInfo();
	void sendUpdatedClientList();
	void sendSpawnPlayer(unsigned int a_index);
	void sendSpawnGameObject(unsigned int a_index);
	void sendDespawnPlayer(unsigned int a_index);
	void sendDespawnGameObject(unsigned int a_index);
	void sendUpdatePlayer();
	void sendUpdateGameObject();

	// Data members //
	RakNet::RakNetGUID*			m_lobbyServerGUID;
	RakNet::RakNetGUID*			m_gameClientGUIDs[GAME_SERVER_MAX_CLIENTS];
	unsigned int				m_numGameClients;

	unsigned int				m_currentID;
	float						m_updateTimer;

	class Player*				m_players[GAME_SERVER_MAX_CLIENTS];
	struct PlayerData*			m_playerData[GAME_SERVER_MAX_CLIENTS];
	std::vector<GameObject*>	m_gameObjects;
	std::vector<class Bullet*>	m_bullets;
	GameObject*					m_floors[2];
	GameObject*					m_walls[2];
	class Flag*					m_flags[2];
	class CaptureButton*		m_captureButton;
	class CaptureNPC*			m_captureNPC;
	class CaptureFloor*			m_captureFloor;
	
	int							m_redTeamScore;
	int							m_blueTeamScore;
	float						m_stopGameTimer;
	bool						m_stopGame;

	class Pickup*				m_pickup;
	GameObject*					m_pickupParachute;
	float						m_pickupTimer;
	int							m_pickupType;
	float						m_pickupSpawn;
};

bool GameMode::getStopGame() {
	return m_stopGameTimer < 0.0f;
}

RakNet::RakNetGUID* GameMode::getLobbyServerGUID() {
	return m_lobbyServerGUID;
}

RakNet::RakNetGUID** GameMode::getGameClientGUIDs() {
	return m_gameClientGUIDs;
}

unsigned int GameMode::getNumGameClients() {
	return m_numGameClients;
}