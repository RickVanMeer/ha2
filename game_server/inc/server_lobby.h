#pragma once

#include "network.h"
#include "raknet_fwd.h"

class ServerLobby {
public:
	ServerLobby();
	~ServerLobby();

	// Methods //
	int init(RakNet::RakNetGUID* a_lobbyServerGUID, RakNet::RakNetGUID** a_gameClientGUIDS, unsigned int a_numGameClients);
	int update();
	int exit();

	// Methods //
	void onConnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);
	void onDisconnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);
	void onNewGameClient(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);
	void onDisconnectGameClient(RakNet::Packet *a_packet, RakNet::BitStream* a_bitStream);
	void onGameClientReady(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);

	// Getters/Setters //
	inline bool	getStartGame();
	inline RakNet::RakNetGUID*	getLobbyServerGUID();
	inline RakNet::RakNetGUID**	getGameClientGUIDs();
	inline unsigned int			getNumGameClients();

private:
	// Methods //
	void sendGameServerInfo();
	void sendUpdatedClientList();

	// Data members //
	RakNet::RakNetGUID*	m_lobbyServerGUID;
	RakNet::RakNetGUID*	m_gameClientGUIDs[GAME_SERVER_MAX_CLIENTS];
	unsigned int		m_numGameClients;
	bool				m_gameClientsReady[GAME_SERVER_MAX_CLIENTS];
	unsigned int		m_numGameClientsReady;
	float				m_readyTimer;
	float				m_updateTimer;
	bool				m_startGame;
};

bool ServerLobby::getStartGame() {
	return m_startGame;
}

RakNet::RakNetGUID* ServerLobby::getLobbyServerGUID() {
	return m_lobbyServerGUID;
}

RakNet::RakNetGUID** ServerLobby::getGameClientGUIDs() {
	return m_gameClientGUIDs;
}

unsigned int ServerLobby::getNumGameClients() {
	return m_numGameClients;
}