#pragma once

#include "raknet_fwd.h"

class NetworkManager;
class ServerLobby;
class GameMode;

class ServerManager {
public:
	ServerManager();
	~ServerManager();

	// Methods //
	int init(int argc, char* argv[]);
	int update();
	int exit();

private:
	// Data members //
	ServerLobby*	m_serverLobby;
	GameMode*		m_gameMode;
};