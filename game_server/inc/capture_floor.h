#pragma once

#include "game_object.h"
#include "raknet_fwd.h"

class CaptureFloor : public GameObject {
public:
	CaptureFloor();
	~CaptureFloor();

	virtual int init() override;
	virtual void customUpdate() override;
	virtual int exit() override;

	// Getters/Setters //
	inline bool					getIsRedTeam();
	void						setIsRedTeam(bool a_isRedTeam);
	inline bool					getIsActivated();
	void						setIsActivated(bool a_isActivated);

private:
	// Data members //
	bool	m_isRedTeam;
	bool	m_isActivated;
};

bool CaptureFloor::getIsRedTeam() {
	return m_isRedTeam;
}

bool CaptureFloor::getIsActivated() {
	return m_isActivated;
}