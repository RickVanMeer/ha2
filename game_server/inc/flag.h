#pragma once

#include "game_object.h"

class Player;

class Flag : public GameObject {
public:
	Flag();
	~Flag();

	virtual int init() override;
	virtual void customUpdate() override;
	virtual int exit() override;

	// Getters/Setters //
	inline Player*	getOwner();
	void			setOwner(Player* a_owner);
	inline bool		getIsRedTeam();
	void			setIsRedTeam(bool a_isRedTeam);

private:
	// Data members //
	Player*		m_owner;
	bool		m_isRedTeam;
};

Player* Flag::getOwner() {
	return m_owner;
}

bool Flag::getIsRedTeam() {
	return m_isRedTeam;
}