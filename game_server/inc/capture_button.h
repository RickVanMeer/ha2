#pragma once

#include "game_object.h"
#include "raknet_fwd.h"

class CaptureButton : public GameObject {
public:
	CaptureButton();
	~CaptureButton();

	virtual int init() override;
	virtual void customUpdate() override;
	virtual int exit() override;

	// Getters/Setters //
	inline bool					getIsRedTeam();
	void						setIsRedTeam(bool a_isRedTeam);
	inline bool					getIsActivated();
	void						setIsActivated(bool a_isActivated);
	inline float				getCooldownTimer();
	void						setCooldownTimer(float a_cooldownTimer);

private:
	// Data members //
	bool	m_isRedTeam;
	bool	m_isActivated;
	float	m_cooldownTimer;
};

bool CaptureButton::getIsRedTeam() {
	return m_isRedTeam;
}

bool CaptureButton::getIsActivated() {
	return m_isActivated;
}

float CaptureButton::getCooldownTimer() {
	return m_cooldownTimer;
}