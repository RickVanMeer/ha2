#pragma once

#include "game_object.h"
#include "raknet_fwd.h"

enum PickupType {
	ETeleportRed = 0,
	ETeleportBlue,
	ENumPickupTypes
};

class Pickup : public GameObject {
public:
	Pickup();
	~Pickup();

	virtual int init() override;
	virtual void customUpdate() override;
	virtual int exit() override;

	// Getters/Setters //
	inline PickupType	getPickupType();
	void				setPickupType(PickupType a_pickupType);

private:
	// Data members //
	PickupType	m_pickupType;
};

PickupType Pickup::getPickupType() {
	return m_pickupType;
}