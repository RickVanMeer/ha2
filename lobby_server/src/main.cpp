#include "engine.h"
#include "network_manager.h"


#include <functional>
using std::function;
using std::bind;

int main(int argc, char* argv[]) {
	NetworkManager server;
	function<int(int, char**)> initFunc = bind(&NetworkManager::init, &server, std::placeholders::_1, std::placeholders::_2);
	function<int()> updateFunc = bind(&NetworkManager::update, &server);
	function<int()> exitFunc = bind(&NetworkManager::exit, &server);

	Engine engine(initFunc, updateFunc, exitFunc);
	engine.startMainLoop(argc, argv);

	return 0;
}