#include "network_manager.h"

#include "input_manager.h"
#include "debug.h"

#include "RakPeerInterface.h"
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "RakNetTypes.h"
using namespace RakNet;

#include "render_manager.h"
#include "text.h"
#include "button.h"
#include "image.h"

NetworkManager::NetworkManager():
	m_peer(nullptr) {
}

NetworkManager::~NetworkManager() {
}

int NetworkManager::init(int argc, char* argv[]) {
	Debug.logMessage("Initializing LobbyServer...");

	for (int i = ID_USER_PACKET_ENUM; i < ENumNetworkMessages - 1; ++i) {
		m_networkCallbacks[i - ID_USER_PACKET_ENUM] = nullptr;
	}

	if (startServer() != 0) return 1;

	// Create button
	m_stopButton = new Button(glm::vec2(0.0f, -0.35f), glm::vec2(0.1f), 0, "Stop server");
	m_stopButton->setTextColor(glm::vec4(1.0f));
	m_stopButton->setBackgroundColor(glm::vec4(0.4f));
	std::function<void(int)> stopCallback = std::bind(&NetworkManager::stopServer, this, std::placeholders::_1);
	m_stopButton->setReleasedCallback(stopCallback);

	// Create texts
	for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		m_serverNames[i] = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_serverIPs[i] = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_serverPlayers[i] = Renderer.createText("../../res/fonts/cour.ttf", 500);

		m_serverNames[i]->setTextAndColor("-", 0xffffffff);
		m_serverIPs[i]->setTextAndColor("-", 0xffffffff);
		m_serverPlayers[i]->setTextAndColor("-/-", 0xffffffff);

		m_serverNames[i]->getImage()->m_position = glm::vec2(-0.25f, 0.235f - i * 0.07f);
		m_serverNames[i]->getImage()->m_size = glm::vec2(0.3f, 0.07f);
		m_serverIPs[i]->getImage()->m_position = glm::vec2(0.1f, 0.235f - i * 0.07f);
		m_serverIPs[i]->getImage()->m_size = glm::vec2(0.3f, 0.07f);
		m_serverPlayers[i]->getImage()->m_position = glm::vec2(0.35f, 0.235f - i * 0.07f);
		m_serverPlayers[i]->getImage()->m_size = glm::vec2(0.1f, 0.07f);

		m_serverClosed[i] = true;
	}
	m_serverList = nullptr;

	for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		m_gameServers[i] = nullptr;
	}

	for (int i = 0; i < LOBBY_SERVER_MAX_CLIENTS; ++i) {
		m_gameClients[i] = nullptr;
	}
	
	Debug.logMessage("LobbyServer initialized!");
	return 0;
}

int NetworkManager::update() {
	m_stopButton->update();
	
	if (!m_peer) return 1;

	if (handleMessages() != 0) return 2;

	return 0;
}

int NetworkManager::exit() {
	Debug.logMessage("Exiting LobbyServer...");

	if (m_peer) {
		stopServer();
	}

	Debug.logMessage("LobbyServer Exited.");
	return 0;
}

void NetworkManager::setNetworkCallback(NetworkMessageID a_messageID, std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback) {
	if (a_messageID < ID_USER_PACKET_ENUM || a_messageID >= ENumNetworkMessages) return;

	m_networkCallbacks[a_messageID - ID_USER_PACKET_ENUM] = a_callback;
}

int NetworkManager::startServer() {
	if (m_peer) {
		stopServer();
	}
	
	m_peer = RakPeerInterface::GetInstance();
	if (!m_peer) return 1;
	SocketDescriptor socketDescriptor(LOBBY_SERVER_PORT, LOBBY_SERVER_ADDRESS);
	m_peer->Startup(LOBBY_SERVER_MAX_CLIENTS + LOBBY_SERVER_MAX_SERVERS, &socketDescriptor, 1);
	m_peer->SetMaximumIncomingConnections(LOBBY_SERVER_MAX_CLIENTS + LOBBY_SERVER_MAX_SERVERS);

	Debug.logMessage("Started the server.");
	return 0;
}

void NetworkManager::stopServer(int a_id) {
	if (!m_peer) return;

	m_peer->Shutdown(1000);
	RakPeerInterface::DestroyInstance(m_peer);
	m_peer = nullptr;

	Debug.logMessage("Stopped the server.");
}

int NetworkManager::restartServer() {
	if (m_peer) {
		stopServer();
	}
	if (startServer() != 0) return 2;

	return 0;
}

int NetworkManager::handleMessages() {
	Packet* messagePacket = m_peer->Receive();

	// Receive message, get messageID, call appropriate function, repeat
	while (messagePacket) {
		BitStream bitStreamIn(messagePacket->data, messagePacket->length, false);
		MessageID messageID;
		bitStreamIn.Read(messageID);

		switch(messageID) {
		case ID_NEW_INCOMING_CONNECTION:
			Debug.logMessage("New connection incoming.");
			break;
		case ID_CONNECTION_LOST:
		case ID_DISCONNECTION_NOTIFICATION:
			// In case this is an unexpected disconnection
			onDisconnectGameClient(messagePacket);
			onDisconnectGameServer(messagePacket);
			break;
		case ID_INVALID_PASSWORD:
			Debug.logMessage("Failed connection attempt: wrong password.");
			break;

		// A couple of messages we don't have to handle
		case ELobbyJoinGameServer:
		case ELobbyQuitGameServer:
			break;

		case ELobbyNewGameServer:
			onNewGameServer(messagePacket, &bitStreamIn);
			break;
		case ELobbyNewGameClient:
			onNewGameClient(messagePacket, &bitStreamIn);
			break;
		case ELobbyDisconnectGameServer:
			onDisconnectGameServer(messagePacket);
			break;
		case ELobbyDisconnectGameClient:
			onDisconnectGameClient(messagePacket);
			break;
		case ELobbyUpdateGameServer:
			onUpdateGameServer(messagePacket, &bitStreamIn);
			break;
		case ELobbyRequestGameServer:
			onRequestGameServer(messagePacket);
			break;

		// Ignore
		case EServerNewGameClient:
		case EServerDisconnectGameClient:
			break;

		default: 
			Debug.logWarning("Received message with messageID %d; unknown messageID.", messageID);
			break;
		}

		if (messageID >= ID_USER_PACKET_ENUM && messageID < ENumNetworkMessages && m_networkCallbacks[messageID - ID_USER_PACKET_ENUM]) {
			m_networkCallbacks[messageID - ID_USER_PACKET_ENUM](messagePacket, &bitStreamIn);
		}

		m_peer->DeallocatePacket(messagePacket);
		messagePacket = m_peer->Receive();
	}
	
	return 0;
}

void NetworkManager::onNewGameServer(Packet *a_packet, BitStream *a_bitStream) {
	Debug.logMessage("GameServer connected.");

	// No spot reserved, searching for a spot
	for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		if (!m_gameServers[i]) {
			char serverAddress[256];
			a_packet->systemAddress.ToString(false, serverAddress);
			unsigned short serverPort = a_packet->systemAddress.GetPort();
			m_gameServers[i] = new GameServerData(RakNet::RakNetGUID::ToUint32(a_packet->guid), serverPort, std::string(serverAddress));
			m_gameServers[i]->name = "Open";

			refreshServerList(true);
			return;
		}
	}

	Debug.logWarning("No empty GameServer spots left, closing connection.");
	m_peer->CloseConnection(a_packet->guid, true);
}

void NetworkManager::onNewGameClient(Packet *a_packet, BitStream *a_bitStream) {
	Debug.logMessage("GameClient connected.");

	for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		if (!m_gameClients[i] || m_gameClients[i]->GUID == 0) {
			m_gameClients[i] = new GameClientData(RakNet::RakNetGUID::ToUint32(a_packet->guid), 0);
			if (m_serverList) m_peer->Send(m_serverList, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::RakNetGUID(m_gameClients[i]->GUID), true);
			return;
		}
	}

	Debug.logWarning("No empty GameClient spots left, closing connection.");
	m_peer->CloseConnection(a_packet->guid, true);
}

void NetworkManager::onDisconnectGameServer(Packet *a_packet) {
	unsigned int gameServerIndex = -1;
	for (unsigned int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		if (m_gameServers[i] && m_gameServers[i]->GUID == RakNet::RakNetGUID::ToUint32(a_packet->guid)) {
			gameServerIndex = i;
			Debug.logMessage("GameServer disconnected.");
			break;
		}
	}
	if (gameServerIndex != -1) {
		delete m_gameServers[gameServerIndex];
		m_gameServers[gameServerIndex] = nullptr;
		for (unsigned int i = gameServerIndex + 1; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
			m_gameServers[i - 1] = m_gameServers[i];
		}
	}

	m_peer->CloseConnection(a_packet->guid, true);

	refreshServerList(true);
}

void NetworkManager::onDisconnectGameClient(Packet *a_packet) {
	unsigned int gameClientIndex = -1;
	for (unsigned int i = 0; i < LOBBY_SERVER_MAX_CLIENTS; ++i) {
		if (m_gameClients[i] && m_gameClients[i]->GUID == RakNet::RakNetGUID::ToUint32(a_packet->guid)) {
			gameClientIndex = i;
			Debug.logMessage("GameClient disconnected.");
			break;
		}
	}
	if (gameClientIndex != -1) {
		delete m_gameClients[gameClientIndex];
		m_gameClients[gameClientIndex] = nullptr;
		for (unsigned int i = gameClientIndex + 1; i < LOBBY_SERVER_MAX_CLIENTS; ++i) {
			m_gameClients[i - 1] = m_gameClients[i];
		}
	}

	m_peer->CloseConnection(a_packet->guid, true);
}

void NetworkManager::onUpdateGameServer(RakNet::Packet* a_packet, RakNet::BitStream *a_bitStream) {
	unsigned int numGameClients;
	a_bitStream->Read(numGameClients);
	for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		if (m_gameServers[i] && m_gameServers[i]->GUID == RakNet::RakNetGUID::ToUint32(a_packet->guid)) {
			m_gameServers[i]->gameClients.clear();
			for (int j = 0; j < GAME_SERVER_MAX_CLIENTS; ++j) {
				if (j >= (int)numGameClients) continue;
				uint64_t gameClientGUID;
				a_bitStream->Read(gameClientGUID);
				m_gameServers[i]->gameClients.push_back(new GameClientData((unsigned long)gameClientGUID, m_gameServers[i]->GUID));
			}
			a_bitStream->Read(m_serverClosed[i]);

			if (m_gameServers[i]->gameClients.size() == GAME_SERVER_MAX_CLIENTS) m_gameServers[i]->name = "Full";
			else if (m_serverClosed[i]) m_gameServers[i]->name = "Closed";
			else m_gameServers[i]->name = "Open";
		}
	}

	refreshServerList(true);
}

void NetworkManager::onRequestGameServer(RakNet::Packet* a_packet) {
	bool emptySpotFound = false;
	for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		if (!m_gameServers[i]) {
			emptySpotFound = true;
			break;
		}
	}
	if (!emptySpotFound) return;

	Debug.logMessage("Accepted request to create GameServer.");
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)EClientCreateGameServer);
	m_peer->Send(&bitStreamOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, a_packet->guid, false);
}

void NetworkManager::refreshServerList(bool a_broadcastServerList) {
	for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		if (m_gameServers[i]) {
			char buffer[256];
			m_serverNames[i]->setText(m_gameServers[i]->name);
			sprintf_s(buffer, "%s|%hu", m_gameServers[i]->ipAddress.c_str(), m_gameServers[i]->port);
			m_serverIPs[i]->setText(buffer);
			sprintf_s(buffer, "%d/%d", m_gameServers[i]->gameClients.size(), GAME_SERVER_MAX_CLIENTS);
			m_serverPlayers[i]->setText(buffer);
		}
		else {
			m_serverNames[i]->setText("-");
			m_serverIPs[i]->setText("-");
			m_serverPlayers[i]->setText("-/-");
		}
	}

	if (m_serverList) delete m_serverList;
	m_serverList = new BitStream();

	for (int i = 0; i < LOBBY_SERVER_MAX_CLIENTS; ++i) {
		m_serverList->Write((MessageID)EClientGameServerlist);
		unsigned int numGameServers = 0;
		for (unsigned int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) if (m_gameServers[i]) numGameServers++;
		m_serverList->Write(numGameServers);

		for (unsigned int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
			if (m_gameServers[i]) {
				m_serverList->Write(m_gameServers[i]->name.c_str());
				m_serverList->Write(m_gameServers[i]->ipAddress.c_str());
				m_serverList->Write(m_gameServers[i]->port);
				m_serverList->Write((unsigned int)m_gameServers[i]->gameClients.size());
				m_serverList->Write(GAME_SERVER_MAX_CLIENTS);
				m_serverList->Write(m_serverClosed[i]);
			}
		}
	}

	if (a_broadcastServerList) {
		for (int i = 0; i < LOBBY_SERVER_MAX_CLIENTS; ++i) {
			if (m_gameClients[i]) m_peer->Send(m_serverList, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::RakNetGUID(m_gameClients[i]->GUID), true);
		}
	}
}