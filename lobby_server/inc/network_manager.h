#pragma once

#include <functional>
#include "network.h"
#include "raknet_fwd.h"

struct	GameClientData;
struct	GameServerData;
class	Button;
class	Text;

class NetworkManager {
public:
	NetworkManager();
	~NetworkManager();

	// Methods //
	int init(int argc, char* argv[]);
	int update();
	int exit();

	void setNetworkCallback(NetworkMessageID a_messageID, std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback);

private:
	NetworkManager(const NetworkManager& a_rhs);
	NetworkManager& operator=(const NetworkManager& a_rhs);

	// Methods //
	int		startServer();
	void	stopServer(int a_id = 0);
	int		restartServer();

	int		handleMessages();
	void	onNewGameServer(RakNet::Packet *a_packet, RakNet::BitStream* a_bitStream);
	void	onNewGameClient(RakNet::Packet *a_packet, RakNet::BitStream* a_bitStream);
	void	onDisconnectGameClient(RakNet::Packet* a_packet);
	void	onDisconnectGameServer(RakNet::Packet* a_packet);
	void	onUpdateGameServer(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);
	void	onRequestGameServer(RakNet::Packet* a_packet);
	void	refreshServerList(bool a_broadcastServerList);

	// Data members //
	RakNet::RakPeerInterface*	m_peer;
	GameClientData*				m_gameClients[LOBBY_SERVER_MAX_CLIENTS];
	GameServerData*				m_gameServers[LOBBY_SERVER_MAX_SERVERS];

	Button*						m_stopButton;
	Text*						m_serverNames[LOBBY_SERVER_MAX_SERVERS];
	Text*						m_serverIPs[LOBBY_SERVER_MAX_SERVERS];
	Text*						m_serverPlayers[LOBBY_SERVER_MAX_SERVERS];
	bool						m_serverClosed[LOBBY_SERVER_MAX_SERVERS];
	RakNet::BitStream*			m_serverList;

	std::function<void(RakNet::Packet*, RakNet::BitStream*)>	m_networkCallbacks[ENumNetworkMessages - ID_USER_PACKET_ENUM];
};