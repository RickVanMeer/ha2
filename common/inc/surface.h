#pragma once

class Surface {
public:
	// Constructors/Destructors //
	Surface(const unsigned int a_width, const unsigned int a_height);
	~Surface();

	// Methods //
	void plotPixel(const unsigned int a_x, const unsigned int a_y, const Uint32 a_color);
	void clear(const Uint32 a_color);

	// Getters/Setters //
	inline unsigned int	getWidth() const;
	inline unsigned int	getHeight() const;

private:
	friend class RenderManager;
	friend struct Image;

	// Data members //
	Uint32*			m_buffer;
	unsigned int	m_width;
	unsigned int	m_height;
	float			m_aspectRatio;
};

unsigned int Surface::getWidth() const {
	return m_width;
}

unsigned int Surface::getHeight() const {
	return m_height;
}