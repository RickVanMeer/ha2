#pragma once

namespace RakNet {
	class	BitStream;
	struct	Packet;
	class	RakPeerInterface;
	struct	RakNetGUID; 
};