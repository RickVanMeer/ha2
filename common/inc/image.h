#pragma once

class Surface;

struct Image {
public:
	Image();
	~Image();

	// Getters/Setters //
	unsigned int			getWidth() const;
	unsigned int			getHeight() const;
	void					setTexture(unsigned int a_texture);
	void					setTexture(const char* a_filePath);

	// Data members //
	glm::vec2		m_position;
	glm::vec2		m_size;
	glm::vec4		m_color;
	float			m_rotation;
	unsigned int	m_texture;
	glm::vec4		m_backgroundColor;
	bool			m_correctForAspectRatio;
	bool			m_isStatic;

private:
	friend class RenderManager;

	// Methods //
	void initBuffers();
	void reset();
	
	// Data members //
	unsigned int	m_vertexArrayObject;
	unsigned int	m_vertexBufferObject;
	unsigned int	m_indexBufferObject;
	unsigned int	m_shaderProgram;
};