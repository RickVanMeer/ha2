#pragma once

/* Class: Timer
 * Functions:
 * - start: start the timer
 * - stop: stop the timer, does not reset
 * - reset: resets the timer to 0.0, does not stop the timer
 * - getElapsedTime: gets elapsed time in seconds
 */

class Timer {
public:
	// Constructors/Destructors //
	Timer(bool a_start = false);
	~Timer();

	// Methods //
	void	start();
	void	reset();
	void	stop();

	// Getters/Setters //
	double	getElapsedTime();

private:
	friend class Time;
	// Data members //
	bool		m_isTiming;
	__int64		m_startTime;
	double		m_endTime;
	double		m_timeStep;
};