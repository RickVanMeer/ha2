#pragma once

struct Image;

class GameObject {
public:
	GameObject();
	~GameObject();

	// Methods //
	virtual int init();
	int update();
	virtual int exit();

	virtual void customUpdate();

	// Getters/Setters //
	inline Image*			getImage();
	inline const glm::vec2&	getPosition();
	void					setPosition(const glm::vec2& a_position);
	inline const glm::vec2&	getVelocity();
	void					setVelocity(const glm::vec2& a_velocity);
	inline bool				getIsGhost();
	void					setIsGhost(bool a_isGhost);
	inline int				getNetworkID();
	void					setNetworkID(int a_networkID);
	inline bool				getIsDirty();
	void					setIsDirty(bool a_isDirty);

protected:
	// Data members //
	Image*		m_image;
	glm::vec2	m_position;
	glm::vec2	m_velocity;
	bool		m_isGhost;
	int			m_networkID;
	bool		m_isDirty;
};

Image* GameObject::getImage() {
	return m_image;
}

const glm::vec2& GameObject::getPosition() {
	return m_position;
}

const glm::vec2& GameObject::getVelocity() {
	return m_velocity;
}

bool GameObject::getIsGhost() {
	return m_isGhost;
}

int GameObject::getNetworkID() {
	return m_networkID;
}

bool GameObject::getIsDirty() {
	return m_isDirty;
}