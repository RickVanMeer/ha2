#pragma once

const unsigned int SCR_WIDTH	= 1080;
const unsigned int SCR_HEIGHT	= 720;

#define MEMBER_OFFSET(a_s, a_member) ((char*)NULL + (offsetof(a_s, a_member)))

struct Image;
class Text;
class Camera;

class RenderManager {
public:
	// Constructors/Destructors //
	RenderManager();
	~RenderManager();

	// Methods //
	void	clearScreen(unsigned int a_color);
	Image*	createImage(const char* a_filePath);
	Text*	createText(const char* a_filePath, const int a_size);
	void	deleteImage(Image* a_image);
	void	deleteText(Text* a_text);

	// Getters/Setters //
	inline Camera*		getCamera() const;
	inline SDL_Surface* getScreenSurface() const;
	const glm::vec2		getScreenSize() const;
	void				setClearColor(const glm::vec4& a_clearColor);

private:
	friend class Engine;

	// Methods //
	int init();
	int update();
	int exit();

	int render();

	int initSDLAndOpenGL();
	int initGLEW();

	// Data members //
	Camera*				m_camera;
	SDL_Window*			m_window;
	SDL_Surface*		m_screen;
	SDL_GLContext		m_glContext;
	std::vector<Image>	m_images;
	std::vector<bool>	m_imagesActive;
	unsigned int		m_activeImages;
};

extern RenderManager Renderer;

Camera* RenderManager::getCamera() const {
	return m_camera;
}

SDL_Surface* RenderManager::getScreenSurface() const {
	return m_screen;
}