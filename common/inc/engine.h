#pragma once

#include <functional>

class Engine { 
public:
	Engine(std::function<int (int, char**)> a_initFunc, std::function<int ()> a_updateFunc, std::function<int ()> a_exitFunc);
	~Engine();

	// Methods //
	void startMainLoop(int argc, char* argv[]);

private:
	Engine(const Engine& a_rhs);
	Engine& operator=(const Engine& a_rhs);

	// Methods //
	int init(int argc, char* argv[]);
	int update();
	int exit();

	// Data members //
	std::function<int (int, char**)>	m_initFunc;
	std::function<int ()>				m_updateFunc;
	std::function<int ()>				m_exitFunc;
};