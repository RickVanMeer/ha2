#pragma once

/* Class: InputManager
 * Functions:
 * - getPressed(): returns true if a_key was down this frame, but not the previous
 * - getReleased(): returns true if a_key was up this frame, but not the previous
 * - getDown(): returns true if a_key is down
 * - getUp(): returns true if a_key is up
 * - getMousePressed(): see getPressed()
 * - getMouseReleased(): see getReleased()
 * - getMouseDown(): see getDown()
 * - getMouseUp(): see getUp()
 * - getMouseMotionX(): returns deltaX of the mouse
 * - getMouseMotionY(): returns deltaY of the mouse
 * - getMouseX(): returns x position of the mouse
 * - getMouseY(): returns y position of the mouse
 */

class InputManager {
public:
	// Methods //
	int		init();
	int		update();

	// Getters/Setters //
	bool	getPressed(unsigned int a_key) const;
	bool	getReleased(unsigned int a_key) const;
	bool	getDown(unsigned int a_key) const;
	bool	getUp(unsigned int a_key) const;
	bool	getMousePressed(unsigned int a_mouse) const;
	bool	getMouseReleased(unsigned int a_mouse) const;
	bool	getMouseDown(unsigned int a_mouse) const;
	bool	getMouseUp(unsigned int a_mouse) const;
	int		getMouseMotionX() const;
	int		getMouseMotionY() const;
	int		getMouseX() const;
	int		getMouseY() const;

private:
	// Methods //
	void	setKey(unsigned int a_key, bool a_state);
	void	setMouse(unsigned int a_mouse, bool a_state);
	void	setMousePosition(int a_x, int a_y, int a_xRelative, int a_yRelative);
};

extern InputManager Input;