#pragma once

//#define LOG_IN_RELEASE

enum DEBUG_FLAGS {
	DEBUGLOG_NONE = 0,
	DEBUGLOG_MESSAGE = 1,
	DEBUGLOG_DEBUG = 2,
	DEBUGLOG_WARNING = 4,
	DEBUGLOG_ERROR = 8,
	DEBUGLOG_ALL = (1 << 8) - 1
};

struct DebugMembers;

class DebugLogger 
{
public:
	// Constructors/Destructors //
	DebugLogger();
	~DebugLogger();

	// Methods //
	bool init(char *a_logPath, bool a_breakAtError, unsigned int a_debugFlags = DEBUGLOG_ALL);
	void exit();

	void logMessage(const char *a_message, ...);
	void logDebug(const char *a_debugInfo, ...);
	void logWarning(const char *a_warning, ...);
	void logError(const char *a_error, ...);
	void clearLogFile();

	// Getters/Setters //
	unsigned int getDebugFlags() const;
	void setDebugFlags(unsigned int a_debugFlags);

private:
	// Methods //
	void log(const char *a_log);

	// Data members //
	DebugMembers *m_pImpl;
};

extern DebugLogger Debug;