#pragma once

GLuint	loadShader(GLenum a_shaderType, const std::string& a_filePath);
GLuint	createShaderProgram(std::vector<GLuint> a_shaders);