#pragma once

class Camera {
public:
	// Constructors/Destructors //
	Camera();
	Camera(int a_screenWidth, int a_screenHeight);
	~Camera();

	// Methods //
	void		init(const glm::vec3& a_startPosition, const glm::quat& a_startRotation);
	void		applyViewMatrix();
	void		translate(const glm::vec3& a_translation, bool a_translateLocal = true);
	void		rotate(const glm::quat& a_rotation);
	void		reset();

	// Getters/Setters //
	void				setViewport(int a_x, int a_y, int a_width, int a_height);
	inline glm::vec4	getViewport() const { return m_viewport; }
	void				setPosition(const glm::vec3& a_position);
	inline glm::vec3	getPosition() const { return m_position; }
	void				setRotation(const glm::quat& a_rotation);
	inline glm::quat	getRotation() const { return m_rotation; }
	void				setScale(const glm::vec3& a_scale);
	inline glm::vec3	getScale() const { return m_scale; }
	void				setEulerAngles(const glm::vec3& a_eulerAngles);
	glm::vec3			getEulerAngles() const;
	void				setProjectionOrthographicRH(float a_left, float a_right, float a_bottom, float a_top);
	void				setProjectionPerspectiveRH(float a_fov, float a_aspectRatio, float a_zNear, float a_zFar);
	inline glm::mat4	getProjectionMatrix() { return m_projectionMatrix; }
	glm::mat4			getViewMatrix();
	glm::vec3			getForward();
	inline float		getNearPlane() const;
	inline float		getFarPlane() const;

private:
	// Methods //
	void		updateViewMatrix();

	// Data members //
	glm::vec4	m_viewport;
	glm::vec3	m_startPosition;
	glm::vec3	m_position;
	glm::quat	m_startRotation;
	glm::quat	m_rotation;
	glm::vec3	m_scale;
	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;
	bool		m_updateViewMatrix;

	float		m_nearPlane;
	float		m_farPlane;
};

float Camera::getNearPlane() const {
	return m_nearPlane;
}

float Camera::getFarPlane() const {
	return m_farPlane;
}