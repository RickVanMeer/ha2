#pragma once

class ResourceManager {
public:
	// Constructors/Destructors //
	ResourceManager();
	~ResourceManager();

	unsigned int	loadTexture(const std::string& a_filePath);

private:
	friend class Engine;
	friend class RenderManager;

	// Methods //
	int exit();

	// Data members //
	std::map<std::string, GLuint>	m_textures;
};

extern ResourceManager Resources;