#pragma once

/* Class: Time
 * Functions:
 * - getRunningTime():			returns the time elapsed since the start of the application
 * - getRunningTimeSeconds():	returns the seconds elapsed since the start of the application
 * - getRunningTimeMinutes():	returns the minutes elapsed since the start of the application
 * - getRunningTimeHours():		returns the hours elapsed since the start of the application
 * - getRunningTimeFormatted():	returns the time elapsed since the start of the application, formatted
 * - getDeltaTime():			returns the deltatime of the application
 * - getSmoothedDeltaTime():	returns a smoothed deltatime of the applications, this limits spikes in deltatime
 * - getFrameCount():			returns the amount of frames since the start of the application
 * - getCPUTimeStep():			returns the timestep of the CPU
 * - getCPUFrequency():			returns the frequency of the CPU
 */

class Time {
public:
	static double		getRunningTime();
	static unsigned int	getRunningTimeSeconds();
	static unsigned int	getRunningTimeMinutes();
	static unsigned int	getRunningTimeHours();
	static void			getRunningTimeFormatted(char *a_destination, unsigned int a_byteSize);
	static double		getDeltaTime();
	static double		getSmoothedDeltaTime();
	static unsigned int getFrameCount();
	static double		getCPUTimeStep();
	static double		getCPUFrequency();

private:
	friend class Engine;
	static void			update();
};