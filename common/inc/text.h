#pragma once

struct Image;

class Text {
public:
	// Constructors/Destructors //
	Text(Image* a_image, const char* a_filePath, const int a_size);
	~Text();

	// Getters/Setters //
	void			setText(std::string a_text);
	void			setColor(unsigned int a_color);
	void			setTextAndColor(std::string a_text, unsigned int a_color);
	unsigned int	getWidth();
	unsigned int	getHeight();
	inline Image*	getImage();

private:
	// Methods //
	void renderText();

	// Data members //
	TTF_Font*		m_font;
	Image*			m_textImage;
	SDL_Color		m_color;
	std::string		m_text;
};

Image* Text::getImage() {
	return m_textImage;
}