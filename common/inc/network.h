#pragma once

#include <string>
#include <vector>

#include "MessageIdentifiers.h"

const char* const	LOBBY_SERVER_ADDRESS = "127.0.0.1";
const int			LOBBY_SERVER_PORT = 8233;
const int			LOBBY_SERVER_MAX_CLIENTS = 16;
const int			LOBBY_SERVER_MAX_SERVERS = 8;
const int			GAME_SERVER_MAX_CLIENTS = 6;
const int			GAME_SERVER_TICK_RATE = 30;
const int			GAME_CLIENT_TICK_RATE = 30;

struct GameClientData {
	GameClientData(unsigned long a_GUID, unsigned long a_serverGUID);

	unsigned long GUID;
	unsigned long serverGUID;
};

struct GameServerData {
	GameServerData(unsigned long a_GUID, unsigned short a_port, std::string a_ipAddress, std::string a_name = "Default");

	void addGameClient(GameClientData* a_gameClient);
	void removeGameClient(GameClientData* a_gameClient);

	unsigned long					GUID;
	unsigned short					port;
	std::string						ipAddress;
	std::string						name;

	std::vector<GameClientData*>	gameClients;
};

enum NetworkMessageID {
	ELobbyNewGameServer = ID_USER_PACKET_ENUM,
	ELobbyNewGameClient,
	ELobbyDisconnectGameServer,
	ELobbyDisconnectGameClient,
	ELobbyJoinGameServer,
	ELobbyQuitGameServer,
	ELobbyRequestGameServer,
	ELobbyRequestGameServerlist,
	ELobbyUpdateGameServer,

	EClientGameServerlist,
	EClientCreateGameServer,
	EClientUpdateGameServerInfo,
	EClientUpdateGameInfo,
	EClientStartGame,
	EClientStopGame,
	EClientSpawnPlayer,
	EClientDespawnPlayer,
	EClientUpdatePlayers,
	EClientSpawnGameObject,
	EClientDespawnGameObject,
	EClientUpdateGameObjects,

	EServerNewGameClient,
	EServerDisconnectGameClient,
	EServerGameClientReady,
	EServerUpdatePlayer,
	EServerSpawnBullet,

	ENumNetworkMessages
};