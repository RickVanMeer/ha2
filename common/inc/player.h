#pragma once

#include "game_object.h"

class Player : public GameObject {
public:
	Player();
	~Player();

	// Methods //
	virtual int		init() override;
	virtual void	customUpdate() override;
	virtual int		exit() override;

	void			addAssistIndex(int a_index);
	void			clearAssistIndices();

	// Getters/Setters //
	inline bool		getIsRedTeam();
	void			setIsRedTeam(bool a_isRedTeam);
	inline float	getHealth();
	void			setHealth(float a_health);
	inline float	getShootCooldown();
	void			setShootCooldown(float a_shootCooldown);
	inline float	getSpawnCooldown();
	void			setSpawnCooldown(float a_spawnCooldown);
	inline bool		getIsSpawned();
	void			setIsSpawned(bool a_isSpawned);
	inline int		getAssistIndex(unsigned int a_index);

private:
	// Data members //
	bool	m_isGrounded;
	bool	m_usedDoubleJump;
	bool	m_isRedTeam;
	float	m_health;
	float	m_shootCooldown;
	float	m_spawnCooldown;
	bool	m_isSpawned;
	int		m_assistIndices[3];
};

bool Player::getIsRedTeam() {
	return m_isRedTeam;
}

float Player::getHealth() {
	return m_health;
}

float Player::getShootCooldown() {
	return m_shootCooldown;
}

float Player::getSpawnCooldown() {
	return m_spawnCooldown;
}

bool Player::getIsSpawned() {
	return m_isSpawned;
}

int Player::getAssistIndex(unsigned int a_index) {
	return m_assistIndices[a_index];
}