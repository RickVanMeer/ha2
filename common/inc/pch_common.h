#pragma once

#include <vector>
#include <string>
#include <map>

#define GLEW_STATIC
#include <gl/glew.h>

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <SDL_opengl.h>

#include <glm.hpp>
#include <gtc/type_ptr.hpp>
#include <gtx/transform.hpp>
#include <gtx/quaternion.hpp>