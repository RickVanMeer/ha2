#pragma once

#include <functional>

class Text;

class Button {
public:
	// Constructors/Destructors //
	Button(glm::vec2 a_position, glm::vec2 a_size, int a_id = 0, const char* a_text = nullptr);
	~Button();

	void update();

	// Getters/Setters //
	void			setPressedCallback(std::function<void(int)> a_callback);
	void			setDownCallback(std::function<void(int)> a_callback);
	void			setUpCallback(std::function<void(int)> a_callback);
	void			setReleasedCallback(std::function<void(int)> a_callback);
	inline Text*	getText();
	void			setTextColor(const glm::vec4& a_color);
	void			setBackgroundColor(const glm::vec4& a_color);

private:
	// Data members //
	Text*	m_text;
	int		m_id;
	bool	m_pressed;
	bool	m_previousPressed;

	std::function<void(int)>	m_onPressedFunc;
	std::function<void(int)>	m_onDownFunc;
	std::function<void(int)>	m_onUpFunc;
	std::function<void(int)>	m_onReleasedFunc;
};

Text* Button::getText() {
	return m_text;
}