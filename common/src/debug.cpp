#include "debug.h"
#include "timing.h"

#include <fstream>
using std::fstream;
#include <stdarg.h>
#include <string>
#include <windows.h>

DebugLogger Debug = DebugLogger();
HANDLE hConsole;

#ifdef LOG_IN_RELEASE
	#define ENABLE_DEBUGLOG
#else
	#ifdef _DEBUG
		#define ENABLE_DEBUGLOG
	#endif
#endif

#ifdef _MSC_VER
	#define DEBUG_BREAK __debugbreak()
#else
#include <signal.h>
	#define DEBUG_BREAK int a;
#endif

struct DebugMembers 
{
	char		logFilePath[512];
	fstream		logFileStream;

	bool		breakAtError;
	int			debugFlags;
};

DebugLogger::DebugLogger() 
{
	static unsigned int numDebugLoggers = 0;
	if (numDebugLoggers) Debug.logError("Multiple DebugLoggers are not allowed.");
	numDebugLoggers++;

	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
}

DebugLogger::~DebugLogger() 
{
	CloseHandle(hConsole);

	if (m_pImpl) delete m_pImpl;
}

unsigned int DebugLogger::getDebugFlags() const 
{
	return m_pImpl->debugFlags;
}

void DebugLogger::setDebugFlags(unsigned int a_debugFlags) 
{
	m_pImpl->debugFlags = a_debugFlags;
}

bool DebugLogger::init(char *a_logPath, bool a_breakAtError, unsigned int a_debugFlags) 
{
	// Initialize data members
	m_pImpl = new DebugMembers();
	m_pImpl->breakAtError = a_breakAtError;
	m_pImpl->debugFlags = a_debugFlags;

	// Open the logfile
	strcpy_s(m_pImpl->logFilePath, a_logPath);
	strcat_s(m_pImpl->logFilePath, "debug_log.txt");

	m_pImpl->logFileStream.open(m_pImpl->logFilePath, fstream::out | fstream::app);

	// Log some information for this session
	logMessage("DebugLogger initialized, backing up logmessages in:\n%s", m_pImpl->logFilePath);

	return true;
}

void DebugLogger::exit() 
{
	logMessage("DebugLogger exited.");

	// Close the logfile
	m_pImpl->logFileStream.close();
	delete m_pImpl;
	m_pImpl = 0;
}

void DebugLogger::logMessage(const char *a_message, ...) 
{
#if defined(_WIN32) &&  defined(ENABLE_DEBUGLOG)
	// Return if it shouldn't log messages
	if (!(m_pImpl->debugFlags & DEBUGLOG_MESSAGE)) return;

	// Use the formatting to get the correct log and copy it to the messageLog
	va_list arguments;
	char message[512];

	va_start(arguments, a_message);
	vsprintf_s(message, a_message, arguments);
	va_end(arguments);

	char messageLog[512];
	char timeLog[256];
	Time::getRunningTimeFormatted(timeLog, 256);
	sprintf_s(messageLog, sizeof(messageLog), "%s | %s", timeLog, message);

	// Change console text color
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN);

	// Log it
	log(messageLog);
#endif
}

void DebugLogger::logDebug(const char *a_debugInfo, ...) 
{
#if defined(_WIN32) &&  defined(ENABLE_DEBUGLOG)
	// See logMessage()
	if (!(m_pImpl->debugFlags & DEBUGLOG_DEBUG)) return;

	va_list arguments;
	char debug[512];

	va_start(arguments, a_debugInfo);
	vsprintf_s(debug, a_debugInfo, arguments);
	va_end(arguments);

	char debugLog[512];
	char timeLog[256];
	Time::getRunningTimeFormatted(timeLog, 256);
	sprintf_s(debugLog, sizeof(debugLog), "%s | %s", timeLog, debug);

	SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN | FOREGROUND_INTENSITY);

	log(debugLog);
#endif
}

void DebugLogger::logWarning(const char *a_warning, ...) 
{
#if defined(_WIN32) &&  defined(ENABLE_DEBUGLOG)
	// See logMessage()
	if (!(m_pImpl->debugFlags & DEBUGLOG_WARNING)) return;

	va_list arguments;
	char warning[512];

	va_start(arguments, a_warning);
	vsprintf_s(warning, a_warning, arguments);
	va_end(arguments);

	char warningLog[512];
	char timeLog[256];
	Time::getRunningTimeFormatted(timeLog, 256);
	sprintf_s(warningLog, sizeof(warningLog), "%s | %s", timeLog, warning);

	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);

	log(warningLog);
#endif
}

void DebugLogger::logError(const char *a_error, ...) 
{
#if defined(_WIN32) &&  defined(ENABLE_DEBUGLOG)
	// See logMessage()
	if (!(m_pImpl->debugFlags & DEBUGLOG_ERROR)) return;

	va_list arguments;
	char error[512];

	va_start(arguments, a_error);
	vsprintf_s(error, a_error, arguments);
	va_end(arguments);

	char errorLog[512];
	char timeLog[256];
	Time::getRunningTimeFormatted(timeLog, 256);
	sprintf_s(errorLog, sizeof(errorLog), "%s | %s", timeLog, error);

	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_INTENSITY | BACKGROUND_RED);

	log(errorLog);

	// Break if needed
	if (m_pImpl->breakAtError) DEBUG_BREAK;
#endif
}

void DebugLogger::clearLogFile() 
{
	// Clear the debug logging file TODO: Find a cleaner way of doing this
	m_pImpl->logFileStream.close();
	m_pImpl->logFileStream.open(m_pImpl->logFilePath, fstream::out | fstream::trunc);
	m_pImpl->logFileStream.close();
	m_pImpl->logFileStream.open(m_pImpl->logFilePath, fstream::out | fstream::app);
}

void DebugLogger::log(const char *a_log) 
{
	// Print log to console
	printf("%s\n", a_log);

	// Write log to debug logging file
	m_pImpl->logFileStream << a_log << std::endl;
}