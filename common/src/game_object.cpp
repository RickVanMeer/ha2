#include "game_object.h"

#include "render_manager.h"
#include "image.h"
#include "debug.h"

GameObject::GameObject() :
	m_image(nullptr),
	m_position(0.0f),
	m_velocity(0.0f),
	m_isGhost(false),
	m_networkID(-1) {
}

GameObject::~GameObject() {
	exit();
}

int GameObject::init() {
	m_image = Renderer.createImage("../../res/textures/square.png");
	m_image->m_backgroundColor = glm::vec4(1.0f);
	m_image->m_correctForAspectRatio = true;

	return 0;
}

int GameObject::update() {
	glm::vec2 previousPosition = m_position;
	glm::vec2 previousVelocity = m_velocity;
	float previousRotation = m_image->m_rotation;
	glm::vec4 previousColor = m_image->m_color;
	glm::vec2 previousSize = m_image->m_size;

	customUpdate();

	m_position += m_velocity;
	m_image->m_position = glm::vec2(m_position);

	if ((glm::length2(previousPosition - m_position) > 0.005f && glm::length2(previousVelocity - m_velocity) > 0.005f) ||
		(glm::length2(previousPosition - m_position) < 0.005f && glm::length2(previousVelocity - m_velocity) > 0.005f) ||
		(previousRotation != m_image->m_rotation) ||
		(previousColor != m_image->m_color) ||
		(previousSize != m_image->m_size)) m_isDirty = true;

	return 0;
}

int GameObject::exit() {
	Renderer.deleteImage(m_image);

	return 0;
}

void GameObject::customUpdate() {

}

void GameObject::setPosition(const glm::vec2& a_position) {
	m_position = a_position;
}

void GameObject::setVelocity(const glm::vec2& a_velocity) {
	m_velocity = a_velocity;
}

void GameObject::setIsGhost(bool a_isGhost) {
	m_isGhost = a_isGhost;
}

void GameObject::setNetworkID(int a_networkID) {
	m_networkID = a_networkID;
}

void GameObject::setIsDirty(bool a_isDirty) {
	m_isDirty = a_isDirty;
}