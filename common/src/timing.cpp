#include "timing.h"
#include "timer.h"

#include <string>
using std::string;

const int NUM_SMOOTH_SAMPLES = 25;

Timer		globalTimer(true);
double		deltaTime			= 0.0;
double		smoothedDeltaTime	= 0.0;
unsigned	int frameCount		= 0;

// Return the elapsed time since the start of the application
double Time::getRunningTime() {
	return globalTimer.getElapsedTime();
}

// Return the elapsed seconds since the start of the application
unsigned int Time::getRunningTimeSeconds() {
	return ((unsigned int) getRunningTime());
}

// Return the elapsed minutes since the start of the application
unsigned int Time::getRunningTimeMinutes() {
	return (((unsigned int) getRunningTime) / 60);
}

// Return the elapsed hours since the start of the application
unsigned int Time::getRunningTimeHours() {
	return (((unsigned int) getRunningTime) / 3600);
}

// Copies the formatted running time to a_destination
void Time::getRunningTimeFormatted(char *a_destination, unsigned int a_byteSize) {
	unsigned int runningTime = (unsigned int)getRunningTime();
	unsigned int hours, minutes, seconds;
	hours = runningTime / 3600;
	runningTime -= hours * 3600;
	minutes = runningTime / 60;
	runningTime -= minutes * 60;
	seconds = runningTime;

	sprintf_s(a_destination, a_byteSize, "%02i:%02i:%02i", hours, minutes, seconds);
}

// Return deltatime
double Time::getDeltaTime() {
	return deltaTime;
}

// Return the smoothed deltatime
double Time::getSmoothedDeltaTime() {
	return smoothedDeltaTime;
}

// Return the timestep of the CPU; the reciprocal of the cpu frequency
double Time::getCPUTimeStep() {
	return globalTimer.m_timeStep;
}

// Return the frequency of the CPU
double Time::getCPUFrequency() {
	return (1.0 / globalTimer.m_timeStep);
}

// Return the amount of frames since the start of the application
unsigned int Time::getFrameCount() {
	return frameCount;
}

void Time::update() {
	// Update deltaTime and smoothed deltatime
	static double lastTime = getRunningTime();
	double newTime = getRunningTime();
	deltaTime = newTime - lastTime;
	smoothedDeltaTime = ((NUM_SMOOTH_SAMPLES - 1) * smoothedDeltaTime + deltaTime) / NUM_SMOOTH_SAMPLES;
	lastTime = newTime;

	// Increase the framecount
	frameCount++;
}