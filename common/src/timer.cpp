#include "timer.h"

#include <windows.h>

Timer::Timer(bool a_start) :
	m_endTime(0.0),
	m_isTiming(false),
	m_startTime(0) {

	// Get the timestep (does not change)
	LARGE_INTEGER largeInteger;
	QueryPerformanceFrequency(&largeInteger);
	m_timeStep = 1 / (double(largeInteger.QuadPart));

	if (a_start) start();
}

Timer::~Timer() {
}

void Timer::start() {
	m_isTiming = true;
	m_endTime = 0.0;

	if (m_startTime != 0) return;

	// Get the starting time
	LARGE_INTEGER largeInteger;
	QueryPerformanceCounter(&largeInteger);
	m_startTime = largeInteger.QuadPart;
}

void Timer::reset() {
	// Reset the starting time
	LARGE_INTEGER largeInteger;
	QueryPerformanceCounter(&largeInteger);
	m_startTime = largeInteger.QuadPart;

	m_endTime = 0.0;
}

void Timer::stop() {
	// Set current running time as the endtime
	LARGE_INTEGER largeInteger;
	QueryPerformanceCounter(&largeInteger);
	m_endTime = double(largeInteger.QuadPart - m_startTime) * m_timeStep * m_isTiming;

	m_isTiming = false;
}

double Timer::getElapsedTime() {
	// Return endtime if stopped
	if (m_endTime != 0.0) return m_endTime;

	// Return elapsed time if timing
	LARGE_INTEGER largeInteger;
	QueryPerformanceCounter(&largeInteger);
	return double(largeInteger.QuadPart - m_startTime) * m_timeStep * m_isTiming;
}