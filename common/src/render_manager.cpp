#include "render_manager.h"

#include <algorithm>
using std::remove;

#include "debug.h"
#include "image.h"
#include "text.h"
#include "shader.h"
#include "camera.h"

const GLfloat imageCorners[] = {
	0.5f, 0.5f, 0.0f,
	-0.5f, 0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	-0.5f, -0.5f, 0.0f
};

const unsigned short imageIndices[] = { 0, 1, 2, 3 };

const unsigned int MAX_NUM_IMAGES = 256;

RenderManager Renderer = RenderManager();

RenderManager::RenderManager():
	m_camera(nullptr),
	m_window(nullptr),
	m_screen(nullptr),
	m_images(),
	m_imagesActive(),
	m_activeImages(0) {

	m_images.resize(MAX_NUM_IMAGES, Image());
	m_imagesActive.resize(MAX_NUM_IMAGES, false);
}

RenderManager::~RenderManager() {
}

void RenderManager::clearScreen(unsigned int a_color) {
	SDL_FillRect(m_screen, 0, a_color);
}

Image* RenderManager::createImage(const char* a_filePath) {
	for (unsigned int i = 0; i < MAX_NUM_IMAGES; ++i) {
		if (m_imagesActive[i]) continue;

		m_images[i].setTexture(a_filePath);
		m_imagesActive[i] = true;
		m_activeImages++;
		return &m_images[i];
	}

	return nullptr;
}

Text* RenderManager::createText(const char* a_filePath, const int a_size) {
	for (unsigned int i = 0; i < MAX_NUM_IMAGES; ++i) {
		if (m_imagesActive[i]) continue;

		Text* newText = new Text(&m_images[i], a_filePath, a_size);
		m_imagesActive[i] = true;
		m_activeImages++;
		return newText;
	}

	return nullptr;
}

void RenderManager::deleteImage(Image* a_image) {
	if (!a_image) return;

	for (unsigned int i = 0; i < MAX_NUM_IMAGES; ++i) {
		if (!m_imagesActive[i]) continue;
		if (&m_images[i] != a_image) continue;

		m_images[i].reset();
		m_imagesActive[i] = false;
		m_activeImages--;
		return;
	}
}

void RenderManager::deleteText(Text* a_text) {
	if (!a_text) return;

	Image* textImage = a_text->getImage();

	for (unsigned int i = 0; i < MAX_NUM_IMAGES; ++i) {
		if (!m_imagesActive[i]) continue;
		if (&m_images[i] != textImage) continue;

		m_images[i].reset();
		m_imagesActive[i] = false;
		m_activeImages--;
		break;
	}
	delete a_text;
}

const glm::vec2 RenderManager::getScreenSize() const {
	return glm::vec2(m_screen->w, m_screen->h);
}

void RenderManager::setClearColor(const glm::vec4& a_clearColor) {
	glClearColor(a_clearColor.r, a_clearColor.g, a_clearColor.b, a_clearColor.a);
}

int RenderManager::init() {
	Debug.logMessage("Initializing RenderManager...");

	if (initSDLAndOpenGL() != 0) return 1;
	if (initGLEW() != 0) return 2;

	m_camera = new Camera(m_screen->w, m_screen->h);
	m_camera->init(glm::vec3(0.0f), glm::quat());
	m_camera->setProjectionOrthographicRH(-0.5f, 0.5f, -0.5f, 0.5f);

	unsigned int numImages = m_images.size();
	for (unsigned int i = 0; i < numImages; ++i) m_images[i].initBuffers();

	Debug.logMessage("RenderManager initialized!");
	return 0;
}

int RenderManager::update() {
	glClear(GL_COLOR_BUFFER_BIT);

	render();

	SDL_GL_SwapWindow(m_window);

	return 0;
}

int RenderManager::exit() {
	Debug.logMessage("Exiting RenderManager...");

	m_images.clear();

	SDL_DestroyWindow(m_window);

	TTF_Quit();
	SDL_Quit();

	delete m_camera;

	Debug.logMessage("RenderManager exited!");
	return 0;
}

int RenderManager::render() {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	unsigned int numImages = m_images.size();
	unsigned int activeImages = 0;
	for (unsigned int i = 0; i < numImages && activeImages < m_activeImages; ++i) {
		if (!m_imagesActive[i]) continue;
		activeImages++;
		
		glBindVertexArray(m_images[i].m_vertexArrayObject);
		
		GLuint shaderProgram = m_images[i].m_shaderProgram;
		glUseProgram(shaderProgram);

		// Set the uniforms
		glm::mat4 mvp = m_camera->getProjectionMatrix() * m_camera->getViewMatrix();
		glm::mat4 viewMatrix = m_camera->getViewMatrix();
		glm::vec3 cameraRight = glm::vec3(m_camera->getViewMatrix()[0]);
		glm::vec3 cameraUp = glm::vec3(m_camera->getViewMatrix()[1]);
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "modelViewProjectionMatrix"), 1, GL_FALSE, glm::value_ptr(mvp));
		glUniform3f(glGetUniformLocation(shaderProgram, "cameraRight"), viewMatrix[0][0], viewMatrix[1][0], viewMatrix[2][0]);
		glUniform3f(glGetUniformLocation(shaderProgram, "cameraUp"), viewMatrix[0][1], viewMatrix[1][1], viewMatrix[2][1]);
		glUniform2fv(glGetUniformLocation(shaderProgram, "position"), 1, glm::value_ptr(m_images[i].m_position));
		glUniform2fv(glGetUniformLocation(shaderProgram, "size"), 1, glm::value_ptr(m_images[i].m_size));
		glUniform4fv(glGetUniformLocation(shaderProgram, "color"), 1, glm::value_ptr(m_images[i].m_color));
		glUniform1f(glGetUniformLocation(shaderProgram, "rotation"), m_images[i].m_rotation);
		glUniform4fv(glGetUniformLocation(shaderProgram, "backgroundColor"), 1, glm::value_ptr(m_images[i].m_backgroundColor));
		glUniform1i(glGetUniformLocation(shaderProgram, "correctAspect"), m_images[i].m_correctForAspectRatio);
		glUniform1i(glGetUniformLocation(shaderProgram, "isStatic"), m_images[i].m_isStatic);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_images[i].m_texture);
		glUniform1i(glGetUniformLocation(shaderProgram, "texture0"), 0);

		glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_SHORT, 0);
	}


	glUseProgram(0);
	glBindVertexArray(0);

	glDisable(GL_BLEND);

	return 0;
}

int RenderManager::initSDLAndOpenGL() {
	// Init SDL and TTF
	if (SDL_Init(SDL_INIT_VIDEO) < 0) { 
		Debug.logError("Failed to initialize RenderManager, error occured in SDL initialization: %s", SDL_GetError());
		return 1;
	}
	if (TTF_Init() != 0) {
		Debug.logError("Failed to initialize RenderManager, error occured in TTF initialization: %s", SDL_GetError());
		return 2;
	}

	//SDL_SetRelativeMouseMode(SDL_TRUE);

	// Create an OpenGL 3.3 core forward compatible context
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	//SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	// AA
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);

	m_window = SDL_CreateWindow("SDL Template", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCR_WIDTH, SCR_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if (!m_window) { 
		Debug.logError("Failed to initialize RenderManager, error occured in window initialization: %s", SDL_GetError());
		return 3;
	}
	m_screen = SDL_GetWindowSurface(m_window);

	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
	m_glContext = SDL_GL_CreateContext(m_window);

	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);

	return 0;
}

int RenderManager::initGLEW() {
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		Debug.logError("Failed to initialize GLEW! Exiting...");
		return 1;
	}

	if (!GLEW_VERSION_3_3) {
		Debug.logError("OpenGL 3.3 required version support not present. Exiting...");
		return 2;
	}

	return 0;
}