#include "surface.h"

Surface::Surface(const unsigned int a_width, const unsigned int a_height):
	m_buffer(new Uint32[a_width * a_height]),
	m_width(a_width),
	m_height(a_height),
	m_aspectRatio((float)m_width/(float)m_height) {
	
	clear(0x00ff00ff);
}

Surface::~Surface() {
	delete[] m_buffer;
}

void Surface::plotPixel(const unsigned int a_x, const unsigned int a_y, const Uint32 a_color) {
	if (a_x >= m_width || a_y >= m_height) return;

	m_buffer[a_y * m_width + a_x] = a_color;
}

void Surface::clear(const Uint32 a_color) {
	unsigned int bufferSize = m_width * m_height;
	for (unsigned int i = 0; i < bufferSize; ++i) m_buffer[i] = a_color;
}