///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Modified version from camera class found at:                                                                                                                                  //
// Van Oosten, J. 2014. Introduction to the OpenGL Shading Language (GLSL) | 3D Game Engine Programming. [online] Available at: http://3dgep.com/?p=5303 [Accessed: 11 Mar 2014].//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <Camera.h>

Camera::Camera() :
m_viewport(0),
m_startPosition(0),
m_position(0),
m_startRotation(),
m_rotation(),
m_scale(1),
m_projectionMatrix(1),
m_viewMatrix(1),
m_updateViewMatrix(false) {
}

Camera::Camera(int a_screenWidth, int a_screenHeight) :
m_viewport(0, 0, a_screenWidth, a_screenHeight),
m_position(0),
m_rotation(),
m_scale(1),
m_projectionMatrix(1),
m_viewMatrix(1),
m_updateViewMatrix(false) {
}

Camera::~Camera() {
}

void Camera::init(const glm::vec3& a_startPosition, const glm::quat& a_startRotation) {
	m_startPosition = a_startPosition;
	m_startRotation = a_startRotation;
	setPosition(m_startPosition);
	setRotation(m_startRotation);
}

void Camera::applyViewMatrix() {
	updateViewMatrix();
}

void Camera::translate(const glm::vec3& a_translation, bool a_translateLocal) {
	if (a_translateLocal) m_position += m_rotation * a_translation;
	else m_position += a_translation;
	m_updateViewMatrix = true;
}


void Camera::rotate(const glm::quat& a_rotation) {
	m_rotation = m_rotation * a_rotation;
	m_updateViewMatrix = true;
}

void Camera::reset() {
	setPosition(m_startPosition);
	setRotation(m_startRotation);
}


void Camera::setViewport(int a_x, int a_y, int a_width, int a_height) {
	m_viewport = glm::vec4(a_x, a_y, a_width, a_height);
}

void Camera::setProjectionOrthographicRH(float a_left, float a_right, float a_bottom, float a_top) {
	m_projectionMatrix = glm::ortho(a_left, a_right, a_bottom, a_top);
}

void Camera::setProjectionPerspectiveRH(float a_fov, float a_aspectRatio, float a_zNear, float a_zFar) {
	m_projectionMatrix = glm::perspective(glm::radians(a_fov), a_aspectRatio, a_zNear, a_zFar);
	m_nearPlane = a_zNear;
	m_farPlane = a_zFar;
}

void Camera::setPosition(const glm::vec3& a_position) {
	m_position = a_position;
	m_updateViewMatrix = true;
}

void Camera::setRotation(const glm::quat& a_rotation) {
	m_rotation = a_rotation;
	m_updateViewMatrix = true;
}

void Camera::setScale(const glm::vec3& a_scale) {
	m_scale = a_scale;
	m_updateViewMatrix = true;
}

void Camera::setEulerAngles(const glm::vec3 & a_eulerAngles) {
	m_rotation = glm::quat(glm::radians(a_eulerAngles));
}

glm::vec3 Camera::getEulerAngles() const {
	return glm::degrees(glm::eulerAngles(m_rotation));
}

glm::mat4 Camera::getViewMatrix() {
	updateViewMatrix();
	return m_viewMatrix;
}

glm::vec3 Camera::getForward() {
	return m_rotation * glm::vec3(1.0f, 0.0f, 0.0f);
}

void Camera::updateViewMatrix() {
	if (m_updateViewMatrix) {
		glm::mat4 translate = glm::translate(-m_position);
		glm::mat4 rotate = glm::transpose(glm::toMat4(m_rotation));
		glm::mat4 scale = glm::scale(m_scale);
		m_viewMatrix = scale * rotate * translate;
		m_updateViewMatrix = false;
	}
}
