#include "text.h"

#include "debug.h"
#include "render_manager.h"
#include "image.h"

using std::string;

Text::Text(Image* a_image, const char* a_filePath, const int a_size) :
	m_font(nullptr),
	m_textImage(a_image),
	m_color(),
	m_text("default") {

	m_font = TTF_OpenFont(a_filePath, a_size);
	if (!m_font) Debug.logError("Failed to load font: %s", a_filePath);

	m_color.r = 0xff;
	m_color.g = 0x00;
	m_color.b = 0xff;

	renderText();
}

Text::~Text() {
	if (m_font) TTF_CloseFont(m_font);
}

void Text::setText(string a_text) {
	m_text = a_text;

	renderText();
}

void Text::setColor(unsigned int a_color) {
	m_color.r = (a_color & 0xff000000) >> 24;
	m_color.g = (a_color & 0x00ff0000) >> 16;
	m_color.b = (a_color & 0x0000ff00) >> 8;
	m_color.a = (a_color & 0x000000ff);

	renderText();
}

void Text::setTextAndColor(string a_text, unsigned int a_color) {
	m_text = a_text;
	m_color.r = (a_color & 0xff000000) >> 24;
	m_color.g = (a_color & 0x00ff0000) >> 16;
	m_color.b = (a_color & 0x0000ff00) >> 8;
	m_color.a = (a_color & 0x000000ff);

	renderText();
}

void Text::renderText() {
	SDL_Surface* tempSurface = TTF_RenderText_Blended(m_font, m_text.c_str(), m_color);
	if (!tempSurface) {
		Debug.logError("Unable to render text.");
		SDL_FreeSurface(tempSurface);
		return;
	}

	SDL_PixelFormat* newFormat = new SDL_PixelFormat(*tempSurface->format);
	newFormat->Rmask = 0xff;
	newFormat->Gmask = 0xff << 8;
	newFormat->Bmask = 0xff << 16;
	newFormat->Amask = 0xff << 24;
	newFormat->Rshift = 0;
	newFormat->Gshift = 8;
	newFormat->Bshift = 16;
	newFormat->Ashift = 24;
	SDL_Surface* tempSurface2 = SDL_ConvertSurface(tempSurface, newFormat, 0);
	SDL_FreeSurface(tempSurface);
	delete newFormat;
	
	if (tempSurface2) {
		GLuint texture = m_textImage->m_texture;
		if (texture) glDeleteTextures(1, &texture);
		glGenTextures(1, &texture);
		
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tempSurface2->w, tempSurface2->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tempSurface2->pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);

		m_textImage->setTexture(texture);
	}
	
	SDL_FreeSurface(tempSurface2);
}

unsigned int Text::getWidth() {
	if (m_textImage) return m_textImage->getWidth();
	else return 0;
}

unsigned int Text::getHeight() {
	if (m_textImage) return m_textImage->getHeight();
	else return 0;
}