#include "player.h"

#include "render_manager.h"
#include "input_manager.h"
#include "timing.h"
#include "camera.h"
#include "image.h"

Player::Player() :
	GameObject(),
	m_isGrounded(false),
	m_usedDoubleJump(false),
	m_isRedTeam(false),
	m_health(100),
	m_shootCooldown(0.0f),
	m_spawnCooldown(0.0f),
	m_isSpawned(false) {

}

Player::~Player() {

}

int Player::init() {
	GameObject::init();

	m_image->m_size = glm::vec2(0.05f);
	
	clearAssistIndices();

	return 0;
}

void Player::customUpdate() {
	if (m_position.y <= -0.375f) {
		m_velocity.y *= -0.5f;
		if (glm::abs(m_velocity.y) < 0.005f) {
			m_velocity.y = 0.0f;
			m_isGrounded = true;
			m_usedDoubleJump = false;
		}
	}

	if (!m_isGhost) {
		if (Input.getDown(SDLK_a)) m_velocity += glm::vec2(-0.05f, 0.0f) * (float)Time::getDeltaTime();
		if (Input.getDown(SDLK_d)) m_velocity += glm::vec2(0.05f, 0.0f) * (float)Time::getDeltaTime();
		if (Input.getPressed(SDLK_SPACE) && (m_isGrounded || (!m_isGrounded && !m_usedDoubleJump))) {
			m_velocity += glm::vec2(0.0f, 0.8f) * (float)Time::getDeltaTime();
			if (!m_isGrounded) m_usedDoubleJump = true;
			m_isGrounded = false;
		}
	}

	// Slowdown
	m_velocity.x += 0.01f;
	m_velocity.x = glm::min(glm::max(m_velocity.x, 0.0f), 0.02f);
	m_velocity.x -= 0.01f;
	m_velocity.x *= (1.0f - (float)Time::getDeltaTime() * 5.0f);

	// Gravity
	if (!m_isGrounded) m_velocity += glm::vec2(0.0f, -0.02f) * (float)Time::getDeltaTime();

	if (m_position.y <= -0.375f) {
		m_position.y = -0.375f;
	}

	// Wall collision
	if (m_position.x - m_image->m_size.x * 0.5f <= -3.0f) {
		m_position.x = -3.0f + m_image->m_size.x * 0.5f;
		m_velocity.x *= -1.0f;
	}
	if (m_position.x + m_image->m_size.x * 0.5f >= 3.0f) {
		m_position.x = 3.0f - m_image->m_size.x * 0.5f;
		m_velocity.x *= -0.5f;
	}

	m_shootCooldown -= (float)Time::getDeltaTime();
	m_spawnCooldown -= (float)Time::getDeltaTime();
}

int Player::exit() {
	return GameObject::exit();
}

void Player::addAssistIndex(int a_index) {
	for (unsigned int i = 0; i < 3; ++i) {
		if (m_assistIndices[i] == a_index) return;
		if (m_assistIndices[i] == -1) {
			m_assistIndices[i] = a_index;
			return;
		}
	}
}

void Player::clearAssistIndices() {
	m_assistIndices[0] = -1;
	m_assistIndices[1] = -1;
	m_assistIndices[2] = -1;
}

void Player::setIsRedTeam(bool a_isRedTeam) {
	m_isRedTeam = a_isRedTeam;
}

void Player::setHealth(float a_health) {
	m_health = a_health;
}

void Player::setShootCooldown(float a_shootCooldown) {
	m_shootCooldown = a_shootCooldown;
}

void Player::setSpawnCooldown(float a_spawnCooldown) {
	m_spawnCooldown = a_spawnCooldown;
}

void Player::setIsSpawned(bool a_isSpawned) {
	m_isSpawned = a_isSpawned;
}