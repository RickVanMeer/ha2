#include "shader.h"

#include <fstream>

#include "debug.h"

using std::vector;
using std::string;

GLuint loadShader(GLenum a_shaderType, const string& a_filePath) {
	std::ifstream stream;
	stream.open(a_filePath);

	// Copy the source code
	if (!stream) {
		Debug.logError("Can not open shader file: \"%s\"", a_filePath.c_str());
		return 0;
	}

	string source(std::istreambuf_iterator<char>(stream), (std::istreambuf_iterator<char>()));
	stream.close();

	// Create the shader object
	GLuint shader = glCreateShader(a_shaderType);
	const GLchar *sources[] = { source.c_str() };
	glShaderSource(shader, 1, sources, NULL);
	glCompileShader(shader);

	// Check for errors
	GLint compileStatus;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
	if (compileStatus != GL_TRUE) {
		GLint logLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
		GLchar *infoLog = new GLchar[logLength];
		glGetShaderInfoLog(shader, logLength, NULL, infoLog);
		Debug.logError("Shader error: %s", infoLog);
		delete infoLog;

		return 0;
	}

	return shader;
}

GLuint createShaderProgram(vector<GLuint> a_shaders) {
	// Create a shader program
	GLuint shaderProgram = glCreateProgram();

	// Attach the shaders and link it
	for (GLuint shader : a_shaders)	glAttachShader(shaderProgram, shader);
	glLinkProgram(shaderProgram);

	// Check for errors
	GLint linkStatus;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linkStatus);
	if (linkStatus != GL_TRUE) {
		GLint logLength;
		glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &logLength);
		GLchar *infoLog = new GLchar[logLength];
		glGetProgramInfoLog(shaderProgram, logLength, NULL, infoLog);
		Debug.logError("Shaderprogram error: %s", infoLog);
		delete infoLog;

		return 0;
	}
	
	return shaderProgram;
}