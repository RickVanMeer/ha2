#include "button.h"

#include "render_manager.h"
#include "input_manager.h"
#include "text.h"
#include "image.h"

Button::Button(glm::vec2 a_position, glm::vec2 a_size, int a_id, const char* a_text) :
	m_text(Renderer.createText("../../res/fonts/cour.ttf", 500)),
	m_id(a_id),
	m_pressed(false),
	m_previousPressed(false),
	m_onPressedFunc(nullptr),
	m_onDownFunc(nullptr),
	m_onUpFunc(nullptr),
	m_onReleasedFunc(nullptr) {
	if (a_text) m_text->setText(a_text);
	m_text->getImage()->m_position = a_position;
	m_text->getImage()->m_size = a_size;
}

Button::~Button() {
	Renderer.deleteText(m_text);
}

void Button::update() {
	glm::vec2 currentPos = (Renderer.getScreenSize() / 2.0f) + (Renderer.getScreenSize()) * (m_text->getImage()->m_position - m_text->getImage()->m_size * 0.5f);
	glm::vec2 currentSize = Renderer.getScreenSize() * m_text->getImage()->m_size;
	glm::vec2 mousePos = glm::vec2(Input.getMouseX(), Input.getMouseY());
	mousePos.y = Renderer.getScreenSize().y - mousePos.y;

	m_previousPressed = m_pressed;
	if (Input.getMouseDown(SDL_BUTTON_LEFT)) {
		if (mousePos.x > currentPos.x &&
			mousePos.x < currentPos.x + currentSize.x &&
			mousePos.y > currentPos.y &&
			mousePos.y < currentPos.y + currentSize.y) {
		m_pressed = true;
}
	}
	else m_pressed = false;

	if (m_pressed) {
		if (m_previousPressed) {
			if (m_onDownFunc) m_onDownFunc(m_id);
		}
		else {
			if (m_onPressedFunc) m_onPressedFunc(m_id);
		}
	}
	else {
		if (m_previousPressed) {
			if (m_onReleasedFunc) m_onReleasedFunc(m_id);
		}
		else {
			if (m_onUpFunc) m_onUpFunc(m_id);
		}
	}
}

void Button::setPressedCallback(std::function<void(int)> a_callback) {
	m_onPressedFunc = a_callback;
}

void Button::setDownCallback(std::function<void(int)> a_callback) {
	m_onDownFunc = a_callback;
}

void Button::setUpCallback(std::function<void(int)> a_callback) {
	m_onUpFunc = a_callback;
}

void Button::setReleasedCallback(std::function<void(int)> a_callback) {
	m_onReleasedFunc = a_callback;
}

void Button::setTextColor(const glm::vec4& a_color) {
	m_text->setColor((unsigned int)a_color.r * 255 + (((unsigned int)a_color.g * 255) << 8) + (((unsigned int)a_color.b * 255) << 16) + (((unsigned int)a_color.a * 255) << 24));
}

void Button::setBackgroundColor(const glm::vec4& a_color) {
	m_text->getImage()->m_backgroundColor = a_color;
}