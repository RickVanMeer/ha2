#include "network.h"


#include "debug.h"
#include <algorithm>
using std::remove;

GameClientData::GameClientData(unsigned long a_GUID, unsigned long a_serverGUID):
	GUID(a_GUID),
	serverGUID(a_serverGUID) {
}

GameServerData::GameServerData(unsigned long a_GUID, unsigned short a_port, std::string a_ipAddress, std::string a_name):
	GUID(a_GUID),
	port(a_port),
	ipAddress(a_ipAddress),
	name(a_name),
	gameClients() {
}

void GameServerData::addGameClient(GameClientData* a_gameClient) {
	if (gameClients.size() >= GAME_SERVER_MAX_CLIENTS) {
		Debug.logWarning("Server is full");
		return;
	}

	gameClients.push_back(a_gameClient);
}

void GameServerData::removeGameClient(GameClientData* a_gameClient) {
	gameClients.erase(remove(gameClients.begin(), gameClients.end(), a_gameClient), gameClients.end());
}