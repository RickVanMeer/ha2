#include "resource_manager.h"

#include "debug.h"

ResourceManager Resources = ResourceManager();

ResourceManager::ResourceManager():
	m_textures() {

}

ResourceManager::~ResourceManager() {

}

int ResourceManager::exit() {
	for (auto texture : m_textures) {
		glDeleteTextures(1, &texture.second);
	}

	return 0;
}

unsigned int ResourceManager::loadTexture(const std::string& a_filePath) {
	if (m_textures.count(a_filePath)) return m_textures[a_filePath];

	SDL_Surface* surface = IMG_Load(a_filePath.c_str());
	if (!surface) {
		Debug.logError("Failed to load texture %s", a_filePath.c_str());
	}

	GLenum textureFormat;
	GLint numColors = surface->format->BytesPerPixel;
	switch (numColors) {
	case 3:
		if (surface->format->Rmask == 0x000000ff) textureFormat = GL_RGB;
		else textureFormat = GL_BGR;
		break;
	case 4:
		if (surface->format->Rmask == 0x000000ff) textureFormat = GL_RGBA;
		else textureFormat = GL_BGRA;
		break;
	default:
		Debug.logError("Failed to load texture %s, texture is not truecolor", a_filePath.c_str());
		break;
	}

	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, surface->w, surface->h, 0, textureFormat, GL_UNSIGNED_BYTE, surface->pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	SDL_FreeSurface(surface);

	m_textures[a_filePath] = texture;
	return texture;
}