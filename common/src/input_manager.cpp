#include "input_manager.h"

#include <map>
using std::map;

InputManager Input = InputManager();


// Data members //
map<unsigned int, bool>	keys;
map<unsigned int, bool> oldKeys;
map<unsigned int, bool> mouse;
map<unsigned int, bool> oldMouse;
unsigned int			mouseX;
unsigned int			mouseY;
unsigned int			mouseMotionX;
unsigned int			mouseMotionY;
const unsigned int		NUM_INPUT_EVENTS = 32;
SDL_Event				inputEvents[NUM_INPUT_EVENTS];

int InputManager::init() {
	mouseX = -1;
	mouseY = -1;
	mouseMotionX = 0;
	mouseMotionY = 0;

	return 0;
}

int InputManager::update() {
	// Update the key states
	for (auto keyValue : keys) oldKeys[keyValue.first] = keyValue.second;
	for (auto mouseValue : mouse) oldMouse[mouseValue.first] = mouseValue.second;
	mouseMotionX = 0;
	mouseMotionY = 0;

	SDL_PumpEvents();
	unsigned int numEvents = SDL_PeepEvents(inputEvents, NUM_INPUT_EVENTS, SDL_GETEVENT, SDL_KEYDOWN, SDL_MOUSEBUTTONUP);

	// Get the new key states
	for (unsigned int i = 0; i < numEvents; ++i) {
		SDL_Event inputEvent = inputEvents[i];
		switch (inputEvent.type) {
		case SDL_KEYDOWN:			setKey(inputEvent.key.keysym.sym, true); break;
		case SDL_KEYUP:				setKey(inputEvent.key.keysym.sym, false); break;
		case SDL_MOUSEMOTION:		setMousePosition(inputEvent.motion.x, inputEvent.motion.y, inputEvent.motion.xrel, inputEvent.motion.yrel); break;
		case SDL_MOUSEBUTTONUP:		setMouse(inputEvent.button.button, false); break;
		case SDL_MOUSEBUTTONDOWN:	setMouse(inputEvent.button.button, true); break;
		default: break;
		}
	}

	return 0;
}

void InputManager::setKey(unsigned int a_key, bool a_state) {
	keys[a_key] = a_state;
}

void InputManager::setMouse(unsigned int a_mouse, bool a_state) {
	mouse[a_mouse] = a_state;
}

void InputManager::setMousePosition(int a_x, int a_y, int a_xRelative, int a_yRelative) {
	mouseMotionX = a_xRelative;
	mouseMotionY = a_yRelative;
	mouseX = a_x;
	mouseY = a_y;
}

bool InputManager::getPressed(unsigned int a_key) const {
	bool returnVal = (keys[a_key] && !oldKeys[a_key]);
	return (keys[a_key] && !oldKeys[a_key]);
}

bool InputManager::getReleased(unsigned int a_key) const {
	return (!keys[a_key] && oldKeys[a_key]);
}

bool InputManager::getDown(unsigned int a_key) const {
	return (keys[a_key] && oldKeys[a_key]);
}

bool InputManager::getUp(unsigned int a_key) const {
	return (!keys[a_key] && !oldKeys[a_key]);
}

bool InputManager::getMousePressed(unsigned int a_mouse) const {
	return (mouse[a_mouse] && !oldMouse[a_mouse]);
}

bool InputManager::getMouseReleased(unsigned int a_mouse) const {
	return (!mouse[a_mouse] && oldMouse[a_mouse]);
}

bool InputManager::getMouseDown(unsigned int a_mouse) const {
	return (mouse[a_mouse] && oldMouse[a_mouse]);
}

bool InputManager::getMouseUp(unsigned int a_mouse) const {
	return (!mouse[a_mouse] && !oldMouse[a_mouse]);
}

int InputManager::getMouseX() const {
	return mouseX;
}

int InputManager::getMouseY() const {
	return mouseY;
}

int InputManager::getMouseMotionX() const {
	return mouseMotionX;
}

int InputManager::getMouseMotionY() const {
	return mouseMotionY;
}