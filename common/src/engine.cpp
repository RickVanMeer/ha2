#include "engine.h"

#include "render_manager.h"
#include "input_manager.h"
#include "resource_manager.h"
#include "debug.h"
#include "timing.h"

Engine::Engine(std::function<int (int, char**)> a_initFunc, std::function<int ()> a_updateFunc, std::function<int ()> a_exitFunc):
	m_initFunc(a_initFunc),
	m_updateFunc(a_updateFunc),
	m_exitFunc(a_exitFunc) {
}

Engine::~Engine() {
}

void Engine::startMainLoop(int argc, char* argv[]) {
	init(argc, argv);

	while (update() == 0);

	exit();
}

int Engine::init(int argc, char* argv[]) {
	Debug.init("", false);
	Debug.logMessage("Initializing Engine...");

	if (Renderer.init() != 0)			return 1;
	if (Input.init() != 0)				return 2;
	if (m_initFunc(argc, argv) != 0)	return 3;

	Debug.logMessage("Engine initialized.");
	return 0;
}

int Engine::update() {
	Time::update();
	if (Input.update() != 0)	return 1;
	if (m_updateFunc() != 0)	return 3;
	if (Renderer.update() != 0) return 2;

	return 0;
}

int Engine::exit() {
	Debug.logMessage("Exiting Engine...");

	if (m_exitFunc() != 0)		return 1;
	if (Renderer.exit() != 0)	return 2;
	if (Resources.exit() != 0)	return 3;

	Debug.logMessage("Engine exited.");
	Debug.exit();
	return 0;
}