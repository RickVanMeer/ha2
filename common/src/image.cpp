#include "image.h"

#include "resource_manager.h"
#include "debug.h"
#include "shader.h"

const GLfloat imageCorners[] = {
	-0.5f, -0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	-0.5f, 0.5f, 0.0f,
	0.5f, 0.5f, 0.0f
};

const unsigned short imageIndices[] = { 0, 1, 2, 3 };

Image::Image():
	m_position(0.0f),
	m_rotation(0.0f),
	m_size(0.1f),
	m_color(1.0f),
	m_texture(0),
	m_backgroundColor(0.0f),
	m_correctForAspectRatio(false),
	m_isStatic(false),
	m_vertexArrayObject(0),
	m_vertexBufferObject(0),
	m_indexBufferObject(0),
	m_shaderProgram(0) {
}

Image::~Image() {

}

unsigned int Image::getWidth() const {
	int width;
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
	glBindTexture(GL_TEXTURE_2D, 0);
	return width;
}

unsigned int Image::getHeight() const {
	int height;
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);
	glBindTexture(GL_TEXTURE_2D, 0);
	return height;
}

void Image::setTexture(unsigned int a_texture) {
	m_texture = a_texture;
}

void Image::setTexture(const char* a_filePath) {
	m_texture = Resources.loadTexture(a_filePath);
}

void Image::initBuffers() {
	// Initialize shader program
	GLuint vertShader = loadShader(GL_VERTEX_SHADER, "../../res/shaders/shader_image.vert");
	GLuint fragShader = loadShader(GL_FRAGMENT_SHADER, "../../res/shaders/shader_image.frag");
	std::vector<GLuint> shaders;
	shaders.push_back(vertShader);
	shaders.push_back(fragShader);
	m_shaderProgram = createShaderProgram(shaders);

	glGenVertexArrays(1, &m_vertexArrayObject);
	glBindVertexArray(m_vertexArrayObject);

	glGenBuffers(1, &m_vertexBufferObject);
	glGenBuffers(1, &m_indexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(imageCorners), imageCorners, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(imageIndices), imageIndices, GL_STATIC_DRAW);

	GLuint positionAttrib = glGetAttribLocation(m_shaderProgram, "in_vertex");
	glVertexAttribPointer(positionAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(positionAttrib);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(positionAttrib);
}

void Image::reset() {
	m_position = glm::vec2(0.0f);
	m_rotation = 0.0f;
	m_size = glm::vec2(0.1f);
	m_color = glm::vec4(1.0f);
	m_texture = (unsigned int)0;
	m_backgroundColor = glm::vec4(0.0f);
	m_correctForAspectRatio = false;
	m_isStatic = false;
}