#pragma once

#include "network.h"
#include "raknet_fwd.h"

class GameObject;
class Player;
class Text;
class Button;

class GameMode {
public:
	GameMode();
	~GameMode();
	
	// Methods //
	int init();
	int update();
	int exit();

	void onDisconnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);
	void onUpdateGameInfo(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);
	void onSpawnPlayer(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);
	void onSpawnGameObject(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);
	void onDespawnPlayer(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);
	void onDespawnGameObject(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);
	void onUpdatePlayers(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);
	void onUpdateGameObjects(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream);

	// Getters/Setters //
	inline bool getExitGame();

private:
	// Methods //
	void sendUpdatePlayer();
	void sendSpawnBullet();

	// Data members //
	Player*						m_players[GAME_SERVER_MAX_CLIENTS];
	std::vector<GameObject*>	m_gameObjects;
	float						m_updateTimer;
	bool						m_exitGame;

	Text*						m_textRedScore;
	Text*						m_textBlueScore;
	
	Button*						m_scoreBackground;
	Button*						m_scoreColors[GAME_SERVER_MAX_CLIENTS];
	Button*						m_scoreRows[3];
	Text*						m_textKills[GAME_SERVER_MAX_CLIENTS + 1];
	Text*						m_textDeaths[GAME_SERVER_MAX_CLIENTS + 1];
	Text*						m_textAssists[GAME_SERVER_MAX_CLIENTS + 1];
	Text*						m_textFlagsStolen[GAME_SERVER_MAX_CLIENTS + 1];
	Text*						m_textFlagsRetrieved[GAME_SERVER_MAX_CLIENTS + 1];
	Text*						m_textFlagsCaptured[GAME_SERVER_MAX_CLIENTS + 1];
	Text*						m_textAccuracy[GAME_SERVER_MAX_CLIENTS + 1];
};

bool GameMode::getExitGame() {
	return m_exitGame;
}