#pragma once

#include <functional>
#include "network.h"
#include "raknet_fwd.h"

class NetworkManager {
public:
	NetworkManager();
	~NetworkManager();

	// Methods //
	int init();
	int update();
	int exit();

	void connect(const char* a_ip, unsigned short a_port);
	void sendMessage(RakNet::BitStream* a_bitStream);

	void setNetworkCallback(NetworkMessageID a_messageID, std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback);
	void setConnectCallback(std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback);
	void setDisconnectCallback(std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback);

private:
	// Methods //
	int		startServer();
	void	stopServer();
	int		restartServer();

	int		handleMessages();
	void	closeConnections();
	void	onConnectionFailed(unsigned int a_failureType);
	void	onConnectionAccepted(RakNet::Packet* a_packet);

	// Data members //
	RakNet::RakPeerInterface*	m_peer;
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> m_networkCallbacks[ENumNetworkMessages - ID_USER_PACKET_ENUM];
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> m_connectCallback;
	std::function<void(RakNet::Packet*, RakNet::BitStream*)>		m_disconnectCallback;
};

extern NetworkManager Network;