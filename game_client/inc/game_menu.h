#pragma once

#include "raknet_fwd.h"
#include "network.h"

class Text;
class Button;

class GameMenu {
public:
	GameMenu();
	~GameMenu();

	// Methods //
	int init(bool a_startInLobby);
	int update();
	int exit();

	void onConnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);
	void onDisconnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);
	void onGameServerList(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);
	void onCreateGameServer(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);
	void onUpdateGameServerInfo(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);
	
	void onButtonServerList(int a_id);
	void onButtonCreateGameServer(int a_id);
	void onButtonJoinGameServer(int a_id);
	void onButtonStop(int a_id);
	void onButtonReady(int a_id);

private:
	// Methods //
	void switchMenu();

	// Data members //
	enum GameMenuState {
		EJoiningLobbyServer,
		EInLobbyServer,
		EJoiningGameServer,
		EInGameServer
	};
	GameMenuState	m_state;
	RakNet::RakNetGUID*	m_lobbyServerGUID;
	RakNet::RakNetGUID*	m_gameServerGUID;
	
	// In LobbyServer
	Text*			m_lobbyServerText;
	Text*			m_serverNameTexts[LOBBY_SERVER_MAX_SERVERS];
	Text*			m_serverIPTexts[LOBBY_SERVER_MAX_SERVERS];
	Text*			m_serverPlayerTexts[LOBBY_SERVER_MAX_SERVERS];
	std::string		m_serverIPs[LOBBY_SERVER_MAX_SERVERS];
	unsigned short	m_serverPorts[LOBBY_SERVER_MAX_SERVERS];
	Button*			m_serverButtons[LOBBY_SERVER_MAX_SERVERS];
	bool			m_serverClosed[LOBBY_SERVER_MAX_SERVERS];
	int				m_serverSelected;
	Button*			m_buttonCreateServer;
	Button*			m_buttonJoinServer;
	Button*			m_buttonStop;
	bool			m_stop;	

	// In GameServer
	Text*			m_gameServerText;
	Text*			m_gameServerPlayers;
	Text*			m_gameServerPlayersReady;
	Text*			m_gameServerTimer;
	Button*			m_buttonReady;
	bool			m_isReady;
};