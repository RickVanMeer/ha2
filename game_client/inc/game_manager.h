#pragma once

#include "raknet_fwd.h"

class NetworkManager;
class GameMenu;
class GameMode;

class GameManager {
public:
	GameManager();
	~GameManager();

	// Methods //
	int init(int argc, char* argv[]);
	int update();
	int exit();

	void onStartGame(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);
	void onStopGame(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream);

private:
	// Data members //
	GameMenu*	m_gameMenu;
	GameMode*	m_gameMode;
};