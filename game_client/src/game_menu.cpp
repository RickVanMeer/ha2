#include "game_menu.h"

#include <functional>

#include "RakPeerInterface.h"
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "RakNetTypes.h"
using namespace RakNet;

#include "render_manager.h"
#include "network_manager.h"
#include "input_manager.h"
#include "text.h"
#include "button.h"
#include "image.h"
#include "debug.h"
#include "camera.h"

GameMenu::GameMenu() {

}

GameMenu::~GameMenu() {

}

int GameMenu::init(bool a_startInLobby) {
	Renderer.setClearColor(glm::vec4(0.2f, 0.2f, 0.2f, 0.0f));
	Renderer.getCamera()->setPosition(glm::vec3(0.0f));

	if (a_startInLobby)	m_state = EJoiningLobbyServer;
	else m_state = EInGameServer;
	
	m_lobbyServerGUID = nullptr;
	m_gameServerGUID = nullptr;

	m_lobbyServerText = nullptr;
	for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		m_serverNameTexts[i] = nullptr;
		m_serverIPTexts[i] = nullptr;
		m_serverPlayerTexts[i] = nullptr;
		m_serverButtons[i] = nullptr;
		m_serverClosed[i] = false;
	}
	m_buttonJoinServer = nullptr;
	m_buttonCreateServer = nullptr;
	m_buttonStop = nullptr;

	m_gameServerText = nullptr;
	m_gameServerPlayers = nullptr;
	m_gameServerPlayersReady = nullptr;
	m_gameServerTimer = nullptr;
	m_buttonReady = nullptr;
	m_isReady = false;

	switchMenu();

	m_serverSelected = -1;
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> gameServerFunc;
	gameServerFunc = std::bind(&GameMenu::onGameServerList, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientGameServerlist, gameServerFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> createServerFunc;
	createServerFunc = std::bind(&GameMenu::onCreateGameServer, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientCreateGameServer, createServerFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> updateInfoFunc;
	updateInfoFunc = std::bind(&GameMenu::onUpdateGameServerInfo, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientUpdateGameServerInfo, updateInfoFunc);

	std::function<void(RakNet::Packet*, RakNet::BitStream*)> connectFunc;
	connectFunc = std::bind(&GameMenu::onConnected, this, std::placeholders::_1, std::placeholders::_2);
	Network.setConnectCallback(connectFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> disconnectFunc;
	disconnectFunc = std::bind(&GameMenu::onDisconnected, this, std::placeholders::_1, std::placeholders::_2);
	Network.setDisconnectCallback(disconnectFunc);

	if (a_startInLobby) Network.connect(LOBBY_SERVER_ADDRESS, LOBBY_SERVER_PORT);

	return 0;
}

int GameMenu::update() {
	if (Input.getReleased(SDLK_ESCAPE)) {
		if (m_state == EInGameServer) {
			m_state = EJoiningLobbyServer;
			Network.connect(LOBBY_SERVER_ADDRESS, LOBBY_SERVER_PORT);
			switchMenu();
		}
	}

	if (m_stop) return 1;

	switch (m_state) {
	case EInLobbyServer:
	case EJoiningLobbyServer:
	{
		if (!m_buttonStop) break;
		for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
			m_serverButtons[i]->update();
		}
		m_buttonCreateServer->update();
		m_buttonJoinServer->update();
		m_buttonStop->update();
	}
	break;
	case EInGameServer:
	{
		m_buttonReady->update();
	}
	break;
	}

	return 0;
}

int GameMenu::exit() {
	if (m_lobbyServerGUID) delete m_lobbyServerGUID;
	m_lobbyServerGUID = nullptr;
	if (m_gameServerGUID) delete m_gameServerGUID;
	m_gameServerGUID = nullptr;

	switch (m_state) {
	case EInGameServer:
	{
		// Delete GameServer menu
		Renderer.deleteText(m_gameServerText);
		m_gameServerText = nullptr;
		Renderer.deleteText(m_gameServerPlayers);
		m_gameServerPlayers = nullptr;
		Renderer.deleteText(m_gameServerPlayersReady);
		m_gameServerPlayers = nullptr;
		Renderer.deleteText(m_gameServerTimer);
		m_gameServerTimer = nullptr;
		if (m_buttonReady) delete m_buttonReady;
		m_buttonReady = nullptr;
	}
	break;
	case EInLobbyServer:
	{
		// Delete LobbyServer menu
		Renderer.deleteText(m_lobbyServerText);
		m_lobbyServerText = nullptr;
		for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
			Renderer.deleteText(m_serverNameTexts[i]);
			Renderer.deleteText(m_serverIPTexts[i]);
			Renderer.deleteText(m_serverPlayerTexts[i]);

			if (m_serverButtons[i]) delete m_serverButtons[i];
			m_serverButtons[i] = nullptr;
		}
		if (m_buttonJoinServer) delete m_buttonJoinServer;
		m_buttonJoinServer = nullptr;
		if (m_buttonCreateServer) delete m_buttonCreateServer;
		m_buttonCreateServer = nullptr;
		if (m_buttonStop) delete m_buttonStop;
		m_buttonStop = nullptr;
	}
	break;
	}
	
	Network.setConnectCallback(nullptr);
	Network.setDisconnectCallback(nullptr);

	Network.setNetworkCallback(EClientGameServerlist, nullptr);
	Network.setNetworkCallback(EClientCreateGameServer, nullptr);
	Network.setNetworkCallback(EClientUpdateGameServerInfo, nullptr);

	return 0;
}

void GameMenu::onConnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	switch (m_state) {
	case EJoiningLobbyServer:
	{
		Debug.logMessage("Connected to LobbyServer");
		m_state = EInLobbyServer;
		m_lobbyServerGUID = new RakNet::RakNetGUID(a_packet->guid);
	}
	break;
	case EJoiningGameServer:
	{
		Debug.logMessage("Connected to GameServer");
		m_state = EInGameServer;
		m_gameServerGUID = new RakNet::RakNetGUID(a_packet->guid);
	}
	break;
	}

	switchMenu();
}

void GameMenu::onDisconnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	switch (m_state) {
	case EInLobbyServer:
	{
		if (m_lobbyServerGUID && *m_lobbyServerGUID != a_packet->guid) return;
		m_state = EJoiningLobbyServer;
		Network.connect(LOBBY_SERVER_ADDRESS, LOBBY_SERVER_PORT);
		switchMenu();
	}
	break;
	case EInGameServer:
	{
		if (m_gameServerGUID && *m_gameServerGUID != a_packet->guid) return;
		m_state = EJoiningLobbyServer;
		Network.connect(LOBBY_SERVER_ADDRESS, LOBBY_SERVER_PORT);
		switchMenu();
	}
	break;
	}	
}

void GameMenu::onGameServerList(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	unsigned int numGameServers;
	a_bitstream->Read(numGameServers);

	// Refresh the game server list
	for (unsigned int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		if (i < numGameServers) {
			char buffer[256];
			a_bitstream->Read(buffer);
			m_serverNameTexts[i]->setText(buffer);

			unsigned short port;
			a_bitstream->Read(buffer);
			a_bitstream->Read(port);
			sprintf_s(buffer, "%s|%hu", buffer, port);
			m_serverIPTexts[i]->setText(buffer);
			m_serverIPs[i] = std::string(buffer);
			m_serverPorts[i] = port;

			unsigned int numClients;
			int maxNumClients;
			a_bitstream->Read(numClients);
			a_bitstream->Read(maxNumClients);
			sprintf_s(buffer, "%d/%d", numClients, maxNumClients);
			m_serverPlayerTexts[i]->setText(buffer);

			a_bitstream->Read(m_serverClosed[i]);
		}
		else {
			m_serverNameTexts[i]->setText("-");
			m_serverIPTexts[i]->setText("-");
			m_serverPlayerTexts[i]->setText("-/-");
			m_serverIPs[i] = std::string();
			m_serverPorts[i] = 0;
		}
	}
}

void GameMenu::onCreateGameServer(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	Debug.logMessage("Creating a GameServer.");

	CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);

	SHELLEXECUTEINFO shellExecuteInfo = { 0 };
	shellExecuteInfo.cbSize = sizeof(shellExecuteInfo);
#ifdef _DEBUG
	shellExecuteInfo.lpFile = "..\\..\\game_server\\bin\\game_serverd.exe";
#else
	shellExecuteInfo.lpFile = "..\\..\\game_server\\bin\\game_server.exe";
#endif
	shellExecuteInfo.nShow = SW_NORMAL;

	ShellExecuteEx(&shellExecuteInfo);

	CoUninitialize();
}

void GameMenu::onUpdateGameServerInfo(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	int numPlayers, numPlayersMax, numPlayersReady, timer;
	a_bitstream->Read(numPlayers);
	a_bitstream->Read(numPlayersMax);
	a_bitstream->Read(numPlayersReady);
	a_bitstream->Read(timer);

	char buffer[256];
	sprintf_s(buffer, "Players: %d/%d", numPlayers, numPlayersMax);
	m_gameServerPlayers->setText(buffer);
	sprintf_s(buffer, "Players ready: %d/%d", numPlayersReady, numPlayers);
	m_gameServerPlayersReady->setText(buffer);
	if (timer > 0) {
		sprintf_s(buffer, "%d", timer);
		m_gameServerTimer->setText(buffer);
	}
	else m_gameServerTimer->setText(" ");
}

void GameMenu::onButtonServerList(int a_id) {
	m_serverSelected = a_id;
	for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
		if (i == m_serverSelected) m_serverButtons[i]->setBackgroundColor(glm::vec4(0.5f));
		else m_serverButtons[i]->setBackgroundColor(glm::vec4(0.0f));
	}
}

void GameMenu::onButtonCreateGameServer(int a_id) {
	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)ELobbyRequestGameServer);
	Network.sendMessage(&bitStreamOut);
}

void GameMenu::onButtonJoinGameServer(int a_id) {
	if (m_serverSelected == -1) return;
	if (m_serverPorts[m_serverSelected] && !m_serverClosed[m_serverSelected]) {
		Network.connect(m_serverIPs[m_serverSelected].c_str(), m_serverPorts[m_serverSelected]);
		m_state = EJoiningGameServer;
	}
}

void GameMenu::onButtonReady(int a_id) {
	m_isReady = !m_isReady;
	if (m_isReady) {
		m_buttonReady->setBackgroundColor(glm::vec4(0.7f));
	}
	else m_buttonReady->setBackgroundColor(glm::vec4(0.4f));

	BitStream bitStreamOut;
	bitStreamOut.Write((MessageID)EServerGameClientReady);
	bitStreamOut.Write(m_isReady);
	Network.sendMessage(&bitStreamOut);
}

void GameMenu::onButtonStop(int a_id) {
	m_stop = true;
}

void GameMenu::switchMenu() {
	switch (m_state) {
	case EJoiningLobbyServer:
	case EInLobbyServer:
	{
		if (m_lobbyServerText) return;

		// Delete GameServer menu
		Renderer.deleteText(m_gameServerText);
		m_gameServerText = nullptr;
		Renderer.deleteText(m_gameServerPlayers);
		m_gameServerPlayers = nullptr;
		Renderer.deleteText(m_gameServerPlayersReady);
		m_gameServerPlayers = nullptr;
		Renderer.deleteText(m_gameServerTimer);
		m_gameServerTimer = nullptr;
		if (m_buttonReady) delete m_buttonReady;
		m_buttonReady = nullptr;

		// Create LobbyServer menu
		m_lobbyServerText = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_lobbyServerText->getImage()->m_position = glm::vec2(0.0f, 0.35f);
		m_lobbyServerText->getImage()->m_size = glm::vec2(0.2f, 0.1f);
		m_lobbyServerText->setTextAndColor("LobbyServer", 0xffffffff);

		// Create texts and buttons
		for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
			m_serverNameTexts[i] = Renderer.createText("../../res/fonts/cour.ttf", 500);
			m_serverIPTexts[i] = Renderer.createText("../../res/fonts/cour.ttf", 500);
			m_serverPlayerTexts[i] = Renderer.createText("../../res/fonts/cour.ttf", 500);

			m_serverNameTexts[i]->setTextAndColor("-", 0xffffffff);
			m_serverIPTexts[i]->setTextAndColor("-", 0xffffffff);
			m_serverPlayerTexts[i]->setTextAndColor("-/-", 0xffffffff);

			m_serverNameTexts[i]->getImage()->m_position = glm::vec2(-0.25f, 0.235f - i * 0.07f);
			m_serverNameTexts[i]->getImage()->m_size = glm::vec2(0.3f, 0.07f);
			m_serverIPTexts[i]->getImage()->m_position = glm::vec2(0.1f, 0.235f - i * 0.07f);
			m_serverIPTexts[i]->getImage()->m_size = glm::vec2(0.3f, 0.07f);
			m_serverPlayerTexts[i]->getImage()->m_position = glm::vec2(0.35f, 0.235f - i * 0.07f);
			m_serverPlayerTexts[i]->getImage()->m_size = glm::vec2(0.1f, 0.07f);

			m_serverButtons[i] = new Button(glm::vec2(0.0f, m_serverNameTexts[i]->getImage()->m_position.y), glm::vec2(0.8f, 0.07f), i, " ");
			std::function<void(int)> buttonFunc = std::bind(&GameMenu::onButtonServerList, this, std::placeholders::_1);
			m_serverButtons[i]->setReleasedCallback(buttonFunc);

			m_serverIPs[i] = std::string();
			m_serverPorts[i] = 0;
		}

		// Create join/create gameserver buttons
		m_buttonCreateServer = new Button(glm::vec2(-0.3f, -0.35f), glm::vec2(0.2f, 0.1f), 0, "Create server");
		m_buttonJoinServer = new Button(glm::vec2(0.3f, -0.35f), glm::vec2(0.2f, 0.1f), 0, "Join server");
		std::function<void(int)> createFunc = std::bind(&GameMenu::onButtonCreateGameServer, this, std::placeholders::_1);
		std::function<void(int)> joinFunc = std::bind(&GameMenu::onButtonJoinGameServer, this, std::placeholders::_1);
		m_buttonCreateServer->setReleasedCallback(createFunc);
		m_buttonJoinServer->setReleasedCallback(joinFunc);
		m_buttonCreateServer->setBackgroundColor(glm::vec4(0.4f));
		m_buttonJoinServer->setBackgroundColor(glm::vec4(0.4f));
		m_buttonCreateServer->setTextColor(glm::vec4(1.0f));
		m_buttonJoinServer->setTextColor(glm::vec4(1.0f));

		// Create stop button
		m_buttonStop = new Button(glm::vec2(0.0f, -0.35f), glm::vec2(0.1f), 0, "Stop");
		m_buttonStop->setTextColor(glm::vec4(1.0f));
		m_buttonStop->setBackgroundColor(glm::vec4(0.4f));
		std::function<void(int)> stopCallback = std::bind(&GameMenu::onButtonStop, this, std::placeholders::_1);
		m_buttonStop->setReleasedCallback(stopCallback);
		m_stop = false;
	}
	break;
	case EJoiningGameServer:
	case EInGameServer:
	{
		if (m_gameServerText) return;

		// Delete LobbyServer menu
		Renderer.deleteText(m_lobbyServerText);
		m_lobbyServerText = nullptr;
		for (int i = 0; i < LOBBY_SERVER_MAX_SERVERS; ++i) {
			Renderer.deleteText(m_serverNameTexts[i]);
			Renderer.deleteText(m_serverIPTexts[i]);
			Renderer.deleteText(m_serverPlayerTexts[i]);

			if (m_serverButtons[i]) delete m_serverButtons[i];
			m_serverButtons[i] = nullptr;
		}
		if (m_buttonJoinServer) delete m_buttonJoinServer;
		m_buttonJoinServer = nullptr;
		if (m_buttonCreateServer) delete m_buttonCreateServer;
		m_buttonCreateServer = nullptr;
		if (m_buttonStop) delete m_buttonStop;
		m_buttonStop = nullptr;

		// Create GameServer menu
		m_gameServerText = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_gameServerText->getImage()->m_position = glm::vec2(0.0f, 0.35f);
		m_gameServerText->getImage()->m_size = glm::vec2(0.2f, 0.1f);
		m_gameServerText->setTextAndColor("GameServer", 0xffffffff);

		m_gameServerPlayers = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_gameServerPlayers->getImage()->m_position = glm::vec2(0.0f, 0.15f);
		m_gameServerPlayers->getImage()->m_size = glm::vec2(0.3f, 0.1f);
		m_gameServerPlayers->setTextAndColor("Players: -/-", 0xffffffff);
		m_gameServerPlayersReady = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_gameServerPlayersReady->getImage()->m_position = glm::vec2(0.0f, -0.05f);
		m_gameServerPlayersReady->getImage()->m_size = glm::vec2(0.4f, 0.1f);
		m_gameServerPlayersReady->setTextAndColor("Players ready: -/-", 0xffffffff);

		m_gameServerTimer = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_gameServerTimer->getImage()->m_position = glm::vec2(0.0f, 2.7f);
		m_gameServerTimer->getImage()->m_size = glm::vec2(0.6f, 0.6f);
		m_gameServerTimer->setTextAndColor(" ", 0xaaaaaaff);

		m_buttonReady = new Button(glm::vec2(0.0f, -0.35f), glm::vec2(0.2f, 0.1f), 0, "Ready");
		m_buttonReady->setTextColor(glm::vec4(1.0f));
		std::function<void(int)> readyFunc = std::bind(&GameMenu::onButtonReady, this, std::placeholders::_1);
		m_buttonReady->setReleasedCallback(readyFunc);
		m_buttonReady->setBackgroundColor(glm::vec4(0.4f));
		m_isReady = false;
	}
	break;
	}
}