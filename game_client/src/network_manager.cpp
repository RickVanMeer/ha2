#include "network_manager.h"

#include "input_manager.h"
#include "debug.h"

#include "RakPeerInterface.h"
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "RakNetTypes.h"
using namespace RakNet;

NetworkManager Network = NetworkManager();

RakNet::RakNetGUID connectedGUID = UNASSIGNED_RAKNET_GUID;

NetworkManager::NetworkManager():
	m_peer(nullptr) {
}

NetworkManager::~NetworkManager() {
}

int NetworkManager::init() {
	Debug.logMessage("Initializing GameClient...");

	for (int i = ID_USER_PACKET_ENUM; i < ENumNetworkMessages - 1; ++i) {
		m_networkCallbacks[i - ID_USER_PACKET_ENUM] = nullptr;
	}
	m_connectCallback = nullptr;
	m_disconnectCallback = nullptr;

	startServer();

	Debug.logMessage("GameClient initialized!");
	return 0;
}

int NetworkManager::update() {
	if (handleMessages() != 0) return 1;

	return 0;
}

int NetworkManager::exit() {
	Debug.logMessage("Exiting GameClient...");

	stopServer();

	Debug.logMessage("GameClient Exited!");
	return 0;
}

void NetworkManager::connect(const char* a_ip, unsigned short a_port) {
	if (m_peer) {
		if (connectedGUID != UNASSIGNED_RAKNET_GUID) {
			closeConnections();
		}
		m_peer->Connect(a_ip, a_port, 0, 0);
		Debug.logMessage("Connecting...");
	}
}

void NetworkManager::sendMessage(RakNet::BitStream* a_bitStream) {
	m_peer->Send(a_bitStream, HIGH_PRIORITY, RELIABLE_ORDERED, 0, connectedGUID, false);
}

void NetworkManager::setNetworkCallback(NetworkMessageID a_messageID, std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback) {
	if (a_messageID < ID_USER_PACKET_ENUM || a_messageID >= ENumNetworkMessages) return;

	m_networkCallbacks[a_messageID - ID_USER_PACKET_ENUM] = a_callback;
}

void NetworkManager::setConnectCallback(std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback) {
	m_connectCallback = a_callback;
}

void NetworkManager::setDisconnectCallback(std::function<void(RakNet::Packet*, RakNet::BitStream*)> a_callback) {
	m_disconnectCallback = a_callback;
}

int NetworkManager::startServer() {
	if (m_peer) {
		stopServer();
	}

	m_peer = RakPeerInterface::GetInstance();
	if (!m_peer) return 1;
	SocketDescriptor socketDescriptor;
	m_peer->Startup(GAME_SERVER_MAX_CLIENTS + 1, &socketDescriptor, 1);
	m_peer->SetMaximumIncomingConnections(GAME_SERVER_MAX_CLIENTS);

	// Connect to the lobby
	m_peer->Connect(LOBBY_SERVER_ADDRESS, LOBBY_SERVER_PORT, 0, 0);

	Debug.logMessage("Started the server.");
	return 0;
}

void NetworkManager::stopServer() {
	if (!m_peer) return;
	
	closeConnections();
	m_peer->Shutdown(1000);
	RakPeerInterface::DestroyInstance(m_peer);
	m_peer = nullptr;

	Debug.logMessage("Stopped the server.");
}

int NetworkManager::restartServer() {
	if (m_peer) {
		stopServer();
	}
	if (startServer() != 0) return 2;

	return 0;
}

int NetworkManager::handleMessages() {
	if (!m_peer) return 0;

	// Handle all incoming messages
	Packet *messagePacket;
	for (messagePacket = m_peer->Receive(); messagePacket; m_peer->DeallocatePacket(messagePacket), messagePacket = m_peer->Receive()) {
		RakNet::BitStream bitStreamIn(messagePacket->data, messagePacket->length, false);
		RakNet::MessageID messageID;
		bitStreamIn.Read(messageID);

		switch (messageID) {
		case ID_CONNECTION_REQUEST_ACCEPTED:
			onConnectionAccepted(messagePacket);
			if (m_connectCallback) m_connectCallback(messagePacket, &bitStreamIn);
			break;
		case ID_INVALID_PASSWORD:
		case ID_CONNECTION_ATTEMPT_FAILED:
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			onConnectionFailed(messageID);
			break;
		case ID_DISCONNECTION_NOTIFICATION:
		case ID_CONNECTION_LOST:
			if (m_disconnectCallback) m_disconnectCallback(messagePacket, &bitStreamIn);
			break;

		// Ignore:
		case ID_NEW_INCOMING_CONNECTION:
		case EClientGameServerlist:
		case EClientCreateGameServer:
		case EClientUpdateGameServerInfo:
		case EClientStartGame:
		case EClientStopGame:
		case EClientUpdateGameInfo:
		case EClientSpawnPlayer:
		case EClientDespawnPlayer:
		case EClientUpdatePlayers:
		case EClientSpawnGameObject:
		case EClientDespawnGameObject:
		case EClientUpdateGameObjects:
			break;

		default:
			Debug.logWarning("Received message with messageID %d; unknown messageID.", messageID);
			break;
		}

		if (messageID >= ID_USER_PACKET_ENUM && messageID < ENumNetworkMessages && m_networkCallbacks[messageID - ID_USER_PACKET_ENUM]) m_networkCallbacks[messageID - ID_USER_PACKET_ENUM](messagePacket, &bitStreamIn);
	}

	return 0;
}

void NetworkManager::closeConnections() {
	// No harm in sending one wrong message
	BitStream bitStreamOutLobbyServer;
	bitStreamOutLobbyServer.Write((MessageID)ELobbyDisconnectGameClient);
	m_peer->Send(&bitStreamOutLobbyServer, HIGH_PRIORITY, RELIABLE_ORDERED, 0, connectedGUID, false);
	
	BitStream bitStreamOutGameServer;
	bitStreamOutGameServer.Write((MessageID)EServerDisconnectGameClient);
	m_peer->Send(&bitStreamOutGameServer, HIGH_PRIORITY, RELIABLE_ORDERED, 0, connectedGUID, false);
}

void NetworkManager::onConnectionFailed(unsigned int a_failureType) {
	switch (a_failureType) {
	case ID_CONNECTION_ATTEMPT_FAILED:
		Debug.logWarning("Couldn't connect to the LobbyServer: timeout");
		break;
	case ID_NO_FREE_INCOMING_CONNECTIONS:
		Debug.logWarning("Couldn't connect to the LobbyServer: full.");
		break;
	case ID_INVALID_PASSWORD:
		Debug.logWarning("Couldn't connect to the LobbyServer: wrong password.");
		break;
	default:
		Debug.logWarning("Couldn't connect to the LobbyServer");
	}
}

void NetworkManager::onConnectionAccepted(RakNet::Packet* a_packet) {
	Debug.logMessage("Connected");
	connectedGUID = a_packet->guid;

	// Send a message to confirm that this is a new gameclient
	BitStream bitStreamOutLobbyServer;
	bitStreamOutLobbyServer.Write((RakNet::MessageID) ELobbyNewGameClient);
	m_peer->Send(&bitStreamOutLobbyServer, HIGH_PRIORITY, RELIABLE_ORDERED, 0, connectedGUID, false);
	BitStream bitStreamOutGameServer;
	bitStreamOutGameServer.Write((RakNet::MessageID) EServerNewGameClient);
	m_peer->Send(&bitStreamOutGameServer, HIGH_PRIORITY, RELIABLE_ORDERED, 0, connectedGUID, false);
}