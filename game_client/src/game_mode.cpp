#include "game_mode.h"

#include "render_manager.h"
#include "network_manager.h"
#include "input_manager.h"
#include "game_object.h"
#include "player.h"
#include "image.h"
#include "timing.h"
#include "camera.h"
#include "debug.h"
#include "text.h"
#include "button.h"

#include "RakPeerInterface.h"
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "RakNetTypes.h"
using namespace RakNet;

GameMode::GameMode():
	m_gameObjects(),
	m_updateTimer(0.0f),
	m_exitGame(false),
	m_textRedScore(nullptr),
	m_textBlueScore(nullptr) {

}

GameMode::~GameMode() {

}

int GameMode::init() {
	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		m_players[i] = nullptr;
	}

	m_scoreBackground = nullptr;
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		m_scoreColors[i] = nullptr;
	}
	for (unsigned int i = 0; i < 3; ++i) {
		m_scoreRows[i] = nullptr;
	}
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS + 1; ++i) {
		m_textKills[i] = nullptr;
		m_textDeaths[i] = nullptr;
		m_textAssists[i] = nullptr;
		m_textFlagsStolen[i] = nullptr;
		m_textFlagsRetrieved[i] = nullptr;
		m_textFlagsCaptured[i] = nullptr;
		m_textAccuracy[i] = nullptr;
	}

	std::function<void(RakNet::Packet*, RakNet::BitStream*)> updateGameInfoFunc;
	updateGameInfoFunc = std::bind(&GameMode::onUpdateGameInfo, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientUpdateGameInfo, updateGameInfoFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> spawnPlayerFunc;
	spawnPlayerFunc = std::bind(&GameMode::onSpawnPlayer, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientSpawnPlayer, spawnPlayerFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> spawnGameObjectFunc;
	spawnGameObjectFunc = std::bind(&GameMode::onSpawnGameObject, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientSpawnGameObject, spawnGameObjectFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> despawnPlayerFunc;
	despawnPlayerFunc = std::bind(&GameMode::onDespawnPlayer, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientDespawnPlayer, despawnPlayerFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> despawnGameObjectFunc;
	despawnGameObjectFunc = std::bind(&GameMode::onDespawnGameObject, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientDespawnGameObject, despawnGameObjectFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> updatePlayersFunc;
	updatePlayersFunc = std::bind(&GameMode::onUpdatePlayers, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientUpdatePlayers, updatePlayersFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> updateGameObjectsFunc;
	updateGameObjectsFunc = std::bind(&GameMode::onUpdateGameObjects, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientUpdateGameObjects, updateGameObjectsFunc);

	std::function<void(RakNet::Packet*, RakNet::BitStream*)> disconnectFunc;
	disconnectFunc = std::bind(&GameMode::onDisconnected, this, std::placeholders::_1, std::placeholders::_2);
	Network.setDisconnectCallback(disconnectFunc);

	m_updateTimer = 0.0f;

	return 0;
}

int GameMode::update() {
	Renderer.setClearColor(0.5f + 0.5f * glm::vec4(glm::sin(Time::getRunningTime() / 5.0f), glm::cos(Time::getRunningTime() / 5.0f), glm::sin((Time::getRunningTime() + 1.7f) / 5.0f), 1.0f));

	if (Input.getReleased(SDLK_ESCAPE)) m_exitGame = true;

	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_players[i]) {
			m_players[i]->update();
			if (!m_players[i]->getIsGhost()) {
				glm::vec2 wantedCameraPosition = m_players[i]->getPosition();
				if (wantedCameraPosition.y < 0.0f) wantedCameraPosition.y = 0.0f;
				Renderer.getCamera()->setPosition(Renderer.getCamera()->getPosition() * 0.95f + glm::vec3(wantedCameraPosition, 0.0f) * 0.05f);

				// Shoot
				if (Input.getMouseDown(SDL_BUTTON_LEFT) && m_players[i]->getShootCooldown() <= 0.0f) {
					m_players[i]->setShootCooldown(0.25f);
					sendSpawnBullet();
				}
			}
		}
	}

	unsigned int numGameObjects = m_gameObjects.size();
	for (unsigned int i = 0; i < numGameObjects; ++i) {
		m_gameObjects[i]->update();
	}

	m_updateTimer -= (float)Time::getDeltaTime();
	if (m_updateTimer < 0.0f) {
		sendUpdatePlayer();
		m_updateTimer = 1.0f / GAME_CLIENT_TICK_RATE;
	}

	return 0;
}

int GameMode::exit() {
	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_players[i]) delete m_players[i];
		m_players[i] = nullptr;
	}

	unsigned int numGameObjects = m_gameObjects.size();
	for (unsigned int i = 0; i < numGameObjects; ++i) {
		delete m_gameObjects[i];
	}
	m_gameObjects.clear();

	Renderer.deleteText(m_textRedScore);
	m_textRedScore = nullptr;
	Renderer.deleteText(m_textBlueScore);
	m_textBlueScore = nullptr;

	if (m_scoreBackground) delete m_scoreBackground;
	m_scoreBackground = nullptr;
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_scoreColors[i]) delete m_scoreColors[i];
		m_scoreColors[i] = nullptr;
	}
	for (unsigned int i = 0; i < 3; ++i) {
		if (m_scoreRows[i]) delete m_scoreRows[i];
		m_scoreRows[i] = nullptr;
	}
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS + 1; ++i) {
		Renderer.deleteText(m_textKills[i]);
		m_textKills[i] = nullptr;
		Renderer.deleteText(m_textDeaths[i]);
		m_textDeaths[i] = nullptr;
		Renderer.deleteText(m_textAssists[i]);
		m_textAssists[i] = nullptr;
		Renderer.deleteText(m_textFlagsStolen[i]);
		m_textFlagsStolen[i] = nullptr;
		Renderer.deleteText(m_textFlagsRetrieved[i]);
		m_textFlagsRetrieved[i] = nullptr;
		Renderer.deleteText(m_textFlagsCaptured[i]);
		m_textFlagsCaptured[i] = nullptr;
		Renderer.deleteText(m_textAccuracy[i]);
		m_textAccuracy[i] = nullptr;
	}

	Network.setNetworkCallback(EClientUpdateGameInfo, nullptr);
	Network.setNetworkCallback(EClientSpawnPlayer, nullptr);
	Network.setNetworkCallback(EClientSpawnGameObject, nullptr);
	Network.setNetworkCallback(EClientDespawnPlayer, nullptr);
	Network.setNetworkCallback(EClientDespawnGameObject, nullptr);
	Network.setNetworkCallback(EClientUpdatePlayers, nullptr);
	Network.setNetworkCallback(EClientUpdateGameObjects, nullptr);

	return 0;
}

void GameMode::onDisconnected(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	m_exitGame = true;
}

void GameMode::onUpdateGameInfo(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	int redScore, blueScore;
	a_bitStream->Read(redScore);
	a_bitStream->Read(blueScore);
	
	if (!m_textRedScore) {
		m_textRedScore = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_textRedScore->setColor(0xff0000ff);
		m_textRedScore->setText("0");
		m_textRedScore->getImage()->m_position = glm::vec2(-0.85f, 0.8f);
		m_textRedScore->getImage()->m_isStatic = true;
		m_textBlueScore = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_textBlueScore->setColor(0x0000ffff);
		m_textBlueScore->setText("0");
		m_textBlueScore->getImage()->m_position = glm::vec2(0.85f, 0.8f);
		m_textBlueScore->getImage()->m_isStatic = true;
	}

	char buffer[256];
	sprintf_s(buffer, "%d", redScore);
	m_textRedScore->setText(buffer);
	sprintf_s(buffer, "%d", blueScore);
	m_textBlueScore->setText(buffer);

	bool displayScoreBoard;
	a_bitStream->Read(displayScoreBoard);
	if (!displayScoreBoard || m_scoreBackground) return;
	
	Renderer.deleteText(m_textRedScore);
	m_textRedScore = nullptr;
	Renderer.deleteText(m_textBlueScore);
	m_textBlueScore = nullptr;

	m_scoreBackground = new Button(glm::vec2(0.0f), glm::vec2(1.9f), 0, " ");
	m_scoreBackground->setBackgroundColor(glm::vec4(0.2f, 0.2f, 0.2f, 0.75f));
	m_scoreBackground->getText()->getImage()->m_isStatic = true;

	for (unsigned int i = 0; i < 3; ++i) {
		m_scoreRows[i] = new Button(glm::vec2(0.0f, 0.3f - i * 0.4f), glm::vec2(1.9f, 0.2f), 0, " ");
		m_scoreRows[i]->setBackgroundColor(glm::vec4(0.5f));
		m_scoreRows[i]->getText()->getImage()->m_isStatic = true;
	}

	m_textKills[0] = Renderer.createText("../../res/fonts/cour.ttf", 500);
	m_textKills[0]->setTextAndColor("Kills", 0xffffffff);
	m_textKills[0]->getImage()->m_position = glm::vec2(-0.7f, 0.5f);
	m_textKills[0]->getImage()->m_size = glm::vec2(0.4f, 0.1f);
	m_textKills[0]->getImage()->m_isStatic = true;
	m_textDeaths[0] = Renderer.createText("../../res/fonts/cour.ttf", 500);
	m_textDeaths[0]->setTextAndColor("Deaths", 0xffffffff);
	m_textDeaths[0]->getImage()->m_position = glm::vec2(-0.7f, 0.3f);
	m_textDeaths[0]->getImage()->m_size = glm::vec2(0.4f, 0.1f);
	m_textDeaths[0]->getImage()->m_isStatic = true;
	m_textAssists[0] = Renderer.createText("../../res/fonts/cour.ttf", 500);
	m_textAssists[0]->setTextAndColor("Assists", 0xffffffff);
	m_textAssists[0]->getImage()->m_position = glm::vec2(-0.7f, 0.1f);
	m_textAssists[0]->getImage()->m_size = glm::vec2(0.4f, 0.1f);
	m_textAssists[0]->getImage()->m_isStatic = true;
	m_textFlagsStolen[0] = Renderer.createText("../../res/fonts/cour.ttf", 500);
	m_textFlagsStolen[0]->setTextAndColor("Flags stolen", 0xffffffff);
	m_textFlagsStolen[0]->getImage()->m_position = glm::vec2(-0.7f, -0.1f);
	m_textFlagsStolen[0]->getImage()->m_size = glm::vec2(0.4f, 0.1f);
	m_textFlagsStolen[0]->getImage()->m_isStatic = true;
	m_textFlagsRetrieved[0] = Renderer.createText("../../res/fonts/cour.ttf", 500);
	m_textFlagsRetrieved[0]->setTextAndColor("Flags retrieved", 0xffffffff);
	m_textFlagsRetrieved[0]->getImage()->m_position = glm::vec2(-0.7f, -0.3f);
	m_textFlagsRetrieved[0]->getImage()->m_size = glm::vec2(0.4f, 0.1f);
	m_textFlagsRetrieved[0]->getImage()->m_isStatic = true;
	m_textFlagsCaptured[0] = Renderer.createText("../../res/fonts/cour.ttf", 500);
	m_textFlagsCaptured[0]->setTextAndColor("Flags captured", 0xffffffff);
	m_textFlagsCaptured[0]->getImage()->m_position = glm::vec2(-0.7f, -0.5f);
	m_textFlagsCaptured[0]->getImage()->m_size = glm::vec2(0.4f, 0.1f);
	m_textFlagsCaptured[0]->getImage()->m_isStatic = true;
	m_textAccuracy[0] = Renderer.createText("../../res/fonts/cour.ttf", 500);
	m_textAccuracy[0]->setTextAndColor("Accuracy", 0xffffffff);
	m_textAccuracy[0]->getImage()->m_position = glm::vec2(-0.7f, -0.7f);
	m_textAccuracy[0]->getImage()->m_size = glm::vec2(0.4f, 0.1f);
	m_textAccuracy[0]->getImage()->m_isStatic = true;

	unsigned int numPlayers;
	glm::vec4 color;
	int kills, deaths, assists, flagsStolen, flagsRetrieved, flagsCaptured;
	float accuracy;
	
	a_bitStream->Read(numPlayers);
	for (unsigned int i = 0; i < numPlayers; ++i) {
		a_bitStream->Read(color.r);
		a_bitStream->Read(color.g);
		a_bitStream->Read(color.b);
		a_bitStream->Read(color.a);
		a_bitStream->Read(kills);
		a_bitStream->Read(deaths);
		a_bitStream->Read(assists);
		a_bitStream->Read(flagsStolen);
		a_bitStream->Read(flagsRetrieved);
		a_bitStream->Read(flagsCaptured);
		a_bitStream->Read(accuracy);

		m_scoreColors[i] = new Button(glm::vec2(-0.4f + i * 0.25f, 0.7f), glm::vec2(0.15f), 0, " ");
		m_scoreColors[i]->setBackgroundColor(color);
		m_scoreColors[i]->getText()->getImage()->m_isStatic = true;
		m_scoreColors[i]->getText()->getImage()->m_correctForAspectRatio = true;

		char buffer[256];
		sprintf_s(buffer, "%d", kills);
		m_textKills[i + 1] = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_textKills[i + 1]->setTextAndColor(buffer, 0xffffffff);
		m_textKills[i + 1]->getImage()->m_position = glm::vec2(-0.4f + i * 0.25f, 0.5f);
		m_textKills[i + 1]->getImage()->m_size = glm::vec2(0.1f);
		m_textKills[i + 1]->getImage()->m_isStatic = true;

		sprintf_s(buffer, "%d", deaths);
		m_textDeaths[i + 1] = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_textDeaths[i + 1]->setTextAndColor(buffer, 0xffffffff);
		m_textDeaths[i + 1]->getImage()->m_position = glm::vec2(-0.4f + i * 0.25f, 0.3f);
		m_textDeaths[i + 1]->getImage()->m_size = glm::vec2(0.1f);
		m_textDeaths[i + 1]->getImage()->m_isStatic = true;

		sprintf_s(buffer, "%d", assists);
		m_textAssists[i + 1] = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_textAssists[i + 1]->setTextAndColor(buffer, 0xffffffff);
		m_textAssists[i + 1]->getImage()->m_position = glm::vec2(-0.4f + i * 0.25f, 0.1f);
		m_textAssists[i + 1]->getImage()->m_size = glm::vec2(0.1f);
		m_textAssists[i + 1]->getImage()->m_isStatic = true;

		sprintf_s(buffer, "%d", flagsStolen);
		m_textFlagsStolen[i + 1] = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_textFlagsStolen[i + 1]->setTextAndColor(buffer, 0xffffffff);
		m_textFlagsStolen[i + 1]->getImage()->m_position = glm::vec2(-0.4f + i * 0.25f, -0.1f);
		m_textFlagsStolen[i + 1]->getImage()->m_size = glm::vec2(0.1f);
		m_textFlagsStolen[i + 1]->getImage()->m_isStatic = true;

		sprintf_s(buffer, "%d", flagsRetrieved);
		m_textFlagsRetrieved[i + 1] = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_textFlagsRetrieved[i + 1]->setTextAndColor(buffer, 0xffffffff);
		m_textFlagsRetrieved[i + 1]->getImage()->m_position = glm::vec2(-0.4f + i * 0.25f, -0.3f);
		m_textFlagsRetrieved[i + 1]->getImage()->m_size = glm::vec2(0.1f);
		m_textFlagsRetrieved[i + 1]->getImage()->m_isStatic = true;

		sprintf_s(buffer, "%d", flagsCaptured);
		m_textFlagsCaptured[i + 1] = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_textFlagsCaptured[i + 1]->setTextAndColor(buffer, 0xffffffff);
		m_textFlagsCaptured[i + 1]->getImage()->m_position = glm::vec2(-0.4f + i * 0.25f, -0.5f);
		m_textFlagsCaptured[i + 1]->getImage()->m_size = glm::vec2(0.1f);
		m_textFlagsCaptured[i + 1]->getImage()->m_isStatic = true;

		sprintf_s(buffer, "%.2f%%", accuracy);
		m_textAccuracy[i + 1] = Renderer.createText("../../res/fonts/cour.ttf", 500);
		m_textAccuracy[i + 1]->setTextAndColor(buffer, 0xffffffff);
		m_textAccuracy[i + 1]->getImage()->m_position = glm::vec2(-0.4f + i * 0.25f, -0.7f);
		m_textAccuracy[i + 1]->getImage()->m_size = glm::vec2(0.1f);
		m_textAccuracy[i + 1]->getImage()->m_isStatic = true;
	}
}

void GameMode::onSpawnPlayer(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	for (int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_players[i]) {
			m_players[i] = new Player();
			m_players[i]->init();

			int networkID;
			bool isGhost;
			float posX, posY, colR, colG, colB, colA;
			a_bitStream->Read(networkID);
			a_bitStream->Read(isGhost);
			a_bitStream->Read(posX);
			a_bitStream->Read(posY);
			a_bitStream->Read(colR);
			a_bitStream->Read(colG);
			a_bitStream->Read(colB);
			a_bitStream->Read(colA);

			m_players[i]->setNetworkID(networkID);
			m_players[i]->setPosition(glm::vec2(posX, posY));
			m_players[i]->setIsGhost(isGhost);
			m_players[i]->getImage()->m_size = glm::vec2(0.05f);
			m_players[i]->getImage()->m_color = glm::vec4(colR, colG, colB, colA);
			return;
		}
	}
}

void GameMode::onSpawnGameObject(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	unsigned int index = m_gameObjects.size();
	m_gameObjects.push_back(new GameObject());
	m_gameObjects[index]->init();

	int networkID;
	bool isGhost, correctForAspect;
	float posX, posY, velX, velY, colR, colG, colB, colA, sizX, sizY, rot;
	a_bitStream->Read(networkID);
	a_bitStream->Read(isGhost);
	a_bitStream->Read(posX);
	a_bitStream->Read(posY);
	a_bitStream->Read(velX);
	a_bitStream->Read(velY);
	a_bitStream->Read(colR);
	a_bitStream->Read(colG);
	a_bitStream->Read(colB);
	a_bitStream->Read(colA);
	a_bitStream->Read(sizX);
	a_bitStream->Read(sizY);
	a_bitStream->Read(rot);
	a_bitStream->Read(correctForAspect);

	m_gameObjects[index]->setNetworkID(networkID);
	m_gameObjects[index]->setPosition(glm::vec2(posX, posY));
	m_gameObjects[index]->setVelocity(glm::vec2(velX, velY));
	m_gameObjects[index]->setIsGhost(isGhost);
	m_gameObjects[index]->getImage()->m_color = glm::vec4(colR, colG, colB, colA);
	m_gameObjects[index]->getImage()->m_size = glm::vec2(sizX, sizY);
	m_gameObjects[index]->getImage()->m_rotation = rot;
	m_gameObjects[index]->getImage()->m_correctForAspectRatio = correctForAspect;
}

void GameMode::onDespawnPlayer(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	int networkID;
	a_bitStream->Read(networkID);
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_players[i]) continue;
		if (m_players[i]->getNetworkID() != networkID) continue;
		
		delete m_players[i];
		m_players[i] = nullptr;
		return;
	}
}

void GameMode::onDespawnGameObject(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	int networkID;
	a_bitStream->Read(networkID);
	unsigned int numGameObjects = m_gameObjects.size();
	for (unsigned int i = 0; i < numGameObjects; ++i) {
		if (m_gameObjects[i]->getNetworkID() != networkID) continue;

		delete m_gameObjects[i];
		m_gameObjects.erase(m_gameObjects.begin() + i);
		return;
	}
}

void GameMode::onUpdatePlayers(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	unsigned int numPlayers;
	a_bitStream->Read(numPlayers);

	for (unsigned int i = 0; i < numPlayers; ++i) {
		int networkID;
		a_bitStream->Read(networkID);

		float posX, posY, velX, velY;
		a_bitStream->Read(posX);
		a_bitStream->Read(posY);
		a_bitStream->Read(velX);
		a_bitStream->Read(velY);

		for (int j = 0; j < GAME_SERVER_MAX_CLIENTS; ++j) {			
			if (!m_players[j]) continue;
			if (m_players[j]->getNetworkID() != networkID || !m_players[j]->getIsGhost()) continue;

			m_players[j]->setPosition(glm::vec2(posX, posY));
			m_players[j]->setVelocity(glm::vec2(velX, velY));
			break;
		}
	}
}

void GameMode::onUpdateGameObjects(RakNet::Packet* a_packet, RakNet::BitStream* a_bitStream) {
	unsigned int numGameObjects = m_gameObjects.size();
	unsigned int numUpdateGameObjects;
	a_bitStream->Read(numUpdateGameObjects);

	for (unsigned int i = 0; i < numUpdateGameObjects; ++i) {
		int networkID;
		a_bitStream->Read(networkID);

		float posX, posY, velX, velY, colR, colG, colB, colA, sizX, sizY, rot;
		a_bitStream->Read(posX);
		a_bitStream->Read(posY);
		a_bitStream->Read(velX);
		a_bitStream->Read(velY);
		a_bitStream->Read(colR);
		a_bitStream->Read(colG);
		a_bitStream->Read(colB);
		a_bitStream->Read(colA);
		a_bitStream->Read(sizX);
		a_bitStream->Read(sizY);
		a_bitStream->Read(rot);

		for (unsigned int j = 0; j < numGameObjects; ++j) {
			if (!m_gameObjects[j]) continue;
			if (m_gameObjects[j]->getNetworkID() != networkID || !m_gameObjects[j]->getIsGhost()) continue;

			m_gameObjects[j]->setPosition(glm::vec2(posX, posY));
			m_gameObjects[j]->setVelocity(glm::vec2(velX, velY));
			m_gameObjects[j]->getImage()->m_color = glm::vec4(colR, colG, colB, colA);
			m_gameObjects[j]->getImage()->m_size = glm::vec2(sizX, sizY);
			m_gameObjects[j]->getImage()->m_rotation = rot;
			break;
		}
	}
}

void GameMode::sendUpdatePlayer() {
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (!m_players[i]) continue;
		if (m_players[i]->getIsGhost()) continue;

		BitStream bitStreamOut;
		bitStreamOut.Write((MessageID)EServerUpdatePlayer);
		bitStreamOut.Write(m_players[i]->getNetworkID());
		bitStreamOut.Write(m_players[i]->getPosition().x + m_players[i]->getVelocity().x * 1.0f / float(GAME_CLIENT_TICK_RATE));
		bitStreamOut.Write(m_players[i]->getPosition().y + m_players[i]->getVelocity().y * 1.0f / float(GAME_CLIENT_TICK_RATE));
		bitStreamOut.Write(m_players[i]->getVelocity().x);
		bitStreamOut.Write(m_players[i]->getVelocity().y);
		Network.sendMessage(&bitStreamOut);
		break;
	}
}

void GameMode::sendSpawnBullet() {
	for (unsigned int i = 0; i < GAME_SERVER_MAX_CLIENTS; ++i) {
		if (m_players[i] && !m_players[i]->getIsGhost()) {
			float rotation = 0.0f;
			glm::vec2 velocity = glm::vec2(0.025f, 0.0f);

			glm::vec2 mousePos = glm::vec2(Input.getMouseX(), Input.getMouseY());
			mousePos /= Renderer.getScreenSize();
			mousePos *= 2.0f;
			mousePos -= glm::vec2(1.0f);
			mousePos.y *= -1.0f;
			glm::vec2 playerPos = glm::vec2((Renderer.getCamera()->getProjectionMatrix() * Renderer.getCamera()->getViewMatrix()) * glm::vec4(m_players[i]->getPosition(), 0.0f, 1.0f));

			velocity = glm::normalize(mousePos - playerPos) * 0.01f;
			rotation = glm::atan(velocity.y / velocity.x);

			BitStream bitStreamOut;
			bitStreamOut.Write((MessageID)EServerSpawnBullet);
			bitStreamOut.Write(rotation);
			bitStreamOut.Write(velocity.x);
			bitStreamOut.Write(velocity.y);
			Network.sendMessage(&bitStreamOut);
		}
	}
}