#include "engine.h"
#include "game_manager.h"

#include <functional>
using std::function;
using std::bind;

int main(int argc, char* argv[]) {
	GameManager game;
	function<int(int, char**)> initFunc = bind(&GameManager::init, &game, std::placeholders::_1, std::placeholders::_2);
	function<int()> updateFunc = bind(&GameManager::update, &game);
	function<int()> exitFunc = bind(&GameManager::exit, &game);

	Engine engine(initFunc, updateFunc, exitFunc);
	engine.startMainLoop(argc, argv);

	return 0;
}