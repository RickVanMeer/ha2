#include "game_manager.h"

#include "input_manager.h"
#include "network_manager.h"
#include "render_manager.h"
#include "game_menu.h"
#include "game_mode.h"
#include "timing.h"
#include "debug.h"

GameManager::GameManager():
	m_gameMenu(nullptr),
	m_gameMode(nullptr) {

}

GameManager::~GameManager() {

}

int GameManager::init(int argc, char* argv[]) {
	Network.init();

	m_gameMenu = new GameMenu();
	m_gameMenu->init(true);
	//m_gameMode = new GameMode();
	//m_gameMode->init();

	std::function<void(RakNet::Packet*, RakNet::BitStream*)> startFunc;
	startFunc = std::bind(&GameManager::onStartGame, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientStartGame, startFunc);
	std::function<void(RakNet::Packet*, RakNet::BitStream*)> stopFunc;
	stopFunc = std::bind(&GameManager::onStopGame, this, std::placeholders::_1, std::placeholders::_2);
	Network.setNetworkCallback(EClientStopGame, stopFunc);

	return 0;
}

int GameManager::update() {
	Network.update();
	if (m_gameMenu) {
		if (m_gameMenu->update() != 0) return 1;
	}
	if (m_gameMode) {
		if (m_gameMode->update() != 0) return 1;

		if (m_gameMode->getExitGame()) {
			m_gameMode->exit();
			delete m_gameMode;
			m_gameMode = nullptr;

			m_gameMenu = new GameMenu();
			m_gameMenu->init(true);
		}
	}

	return 0;
}

int GameManager::exit() {
	Network.exit();

	if (m_gameMenu) {
		m_gameMenu->exit();
		delete m_gameMenu;
		m_gameMenu = nullptr;
	}
	if (m_gameMode) {
		m_gameMode->exit();
		delete m_gameMode;
		m_gameMode = nullptr;
	}

	return 0;
}

void GameManager::onStartGame(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	if (m_gameMenu) {
		m_gameMenu->exit();
		delete m_gameMenu;
		m_gameMenu = nullptr;

		m_gameMode = new GameMode();
		m_gameMode->init();
	}
}

void GameManager::onStopGame(RakNet::Packet* a_packet, RakNet::BitStream* a_bitstream) {
	if (m_gameMode) {
		m_gameMode->exit();
		delete m_gameMode;
		m_gameMode = nullptr;
	}

	m_gameMenu = new GameMenu();
	m_gameMenu->init(false);
}