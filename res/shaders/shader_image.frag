#version 330 core

in vec4 frag_color;
in vec2 frag_textureCoords;

out vec4 out_color;

uniform sampler2D	texture0;
uniform vec4		backgroundColor;

void main() {
	out_color = texture(texture0, frag_textureCoords) * frag_color;
	if (out_color.a < 1.0f) {
		out_color = vec4((1.0f-out_color.a) * backgroundColor.rgb, 0.0f);
		out_color.a = backgroundColor.a;
	}
}