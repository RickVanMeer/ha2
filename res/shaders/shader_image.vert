#version 330 core

in vec3 in_vertex;

out vec4 frag_color;
out vec2 frag_textureCoords;

uniform mat4	modelViewProjectionMatrix;
uniform vec3	cameraRight;
uniform vec3	cameraUp;
uniform vec2	position;
uniform vec2	size;
uniform vec4	color;
uniform float	rotation;
uniform bool	correctAspect;
uniform bool	isStatic;

void main() {
	// First scale, then rotate, then billboard, then apply MVP
	vec3 vertexW = in_vertex * vec3(size, 1.0f);
	float cosAngle = cos(rotation);
	float sinAngle = sin(rotation);
	float rotatedX = cosAngle * vertexW.x - sinAngle * vertexW.y;
	float rotatedY = sinAngle * vertexW.x + cosAngle * vertexW.y;
	vertexW = vec3(position, 0.0f) + cameraRight * rotatedX * ((correctAspect) ? 720.0f/1080.0f : 1.0f) + cameraUp * rotatedY;
	if (!isStatic) gl_Position = modelViewProjectionMatrix * vec4(vertexW, 1.0f);
	else gl_Position = vec4(vertexW, 1.0f);
	
	frag_color = color;
	frag_textureCoords = vec2(in_vertex.x + 0.5f, 1.0f - in_vertex.y + 0.5f);
}